﻿using DspDataModel;
using DspDataModel.Hardware;

namespace HardwareLibrary.Receivers
{
    public class Receiver : IReceiver
    {
        private readonly ConverterWrapper _converterWrapper;

        public IAntennaCommutator AntennaCommutator { get; }
        public IConverter Converter { get; }

        public Receiver(string comPortName, int baudRate, FtmRole role)
        {
            _converterWrapper = new ConverterWrapper(comPortName, baudRate, role);

            var antennaSetup = role == FtmRole.Master
                ? Config.Instance.AntennaSettings.MasterAntennaNumbers
                : Config.Instance.AntennaSettings.SlaveAntennaNumbers;
            AntennaCommutator = new AntennaCommutator(_converterWrapper, antennaSetup);
            Converter = new Converter(_converterWrapper);
        }

        public bool Initialize() => _converterWrapper.Initialize();
    }
}
