﻿using DspDataModel;
using DspDataModel.Hardware;

namespace HardwareLibrary.Receivers
{
    /*
     * One would assume that for relay commands a pause would be necessary.
     * But it turns out that this is not true and relay switches almost instantly.
     * That means that no pause is needed.
     */
    public class Relay : IRelay
    {
        private readonly ConverterLib.Relay _relay;

        public RelayWorkingRange WorkingRange { get; private set; } = RelayWorkingRange.Unknown;
        public string ComPortName { get; }
        public int BaudRate { get; }

        public Relay(string comPortName, int baudRate)
        {
            _relay = new ConverterLib.Relay();
            ComPortName = comPortName;
            BaudRate = baudRate;
        }

        public bool Initialize() => _relay.OpenPort(ComPortName, BaudRate);

        public bool SetRangeFrom0To1Ghz()
        {
            return SetRange(RelayWorkingRange.From100MhzTo1Ghz);
        }

        public bool SetRangeFrom1To18Ghz()
        {
            return SetRange(RelayWorkingRange.From1To18Ghz);
        }

        private bool SetRange(RelayWorkingRange range) 
        {
            if (WorkingRange == range)
                return true;
            var result = range == RelayWorkingRange.From100MhzTo1Ghz 
                ? _relay.SetRangeFrom0To1GHz() 
                : _relay.SetRangeFrom1To18GHz();
            if (result)
                WorkingRange = range;
            return result;
        }

        public bool TestRadiopath() => _relay.EnableControlSignal();
    }
}
