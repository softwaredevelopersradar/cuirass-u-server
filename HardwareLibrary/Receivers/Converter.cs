﻿using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;

namespace HardwareLibrary.Receivers
{
    public class Converter : IConverter
    {
        private readonly ConverterWrapper _converterWrapper;

        public string ComPort => _converterWrapper.ComPortName;

        public FtmRole Role => _converterWrapper.Role;

        public int CurrentBandNumber { get; private set; } = -1;
        public ConverterWrapper Yo => _converterWrapper;

        public Converter(ConverterWrapper converterWrapper) 
        {
            _converterWrapper = converterWrapper;
        }

        public bool SetBand(int bandNumber)
        {
            var band = new Band(bandNumber);
            return SetBand(band);
        }

        public bool SetBand(Band band)
        {
            if (CurrentBandNumber == band.Number)
                return true;
            var result = _converterWrapper.SetConverterFrequency(band.ConverterFrequencyMhz);
            if (result)
                CurrentBandNumber = band.Number;
            return result;
        }

        public async System.Threading.Tasks.Task<bool> SetBandAsync(Band band)
        {
            if (CurrentBandNumber == band.Number)
                return true;
            var result = await _converterWrapper.SetConverterFrequencyAsync(band.ConverterFrequencyMhz);
            if (result)
                CurrentBandNumber = band.Number;
            return result;
        }
    }
}
