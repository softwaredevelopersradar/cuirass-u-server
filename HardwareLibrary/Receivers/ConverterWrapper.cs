﻿using System;
using DspDataModel;
using Settings;
using ConverterLib;
using System.Threading;
using System.Threading.Tasks;

namespace HardwareLibrary.Receivers
{
    public class ConverterWrapper
    {
        private readonly ConverterLib.Converter _converter;
        public FtmRole Role { get; }
        public string ComPortName { get; }
        public int BaudRate { get; }

        public ConverterWrapper(string comPortName, int baudRate, FtmRole role)
        {
            var mode = role == FtmRole.Master
                ? Mode.Master
                : Mode.Slave;
            _converter = new ConverterLib.Converter(mode);
            ComPortName = comPortName;
            BaudRate = baudRate;
            Role = role;
        }

        public void SetCommutatorAntenna(int antennaNumber, int attenuator)
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
                return;
            if (antennaNumber < Constants.FirstAntennaNumber || antennaNumber > Constants.LastAntennaNumber)
            {
                MessageLogger.Error($"Antenna number {antennaNumber} does not exist");
                return;
            }
            var attenuatorLoss = attenuator;
            if (attenuator < Constants.MinAttenuatorValue || attenuator > Constants.MaxAttenuatorValue)
                attenuatorLoss = 0;
            _converter.SetCommutatorAntenna((byte)antennaNumber, (byte)attenuatorLoss);
        }

        public async Task SetCommutatorAntennaAsync(int antennaNumber, int attenuator)
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                await Task.Delay(Config.Instance.DirectionFindingSettings.CommutatorPauseTimeMs);
                return;//todo : remove me, when converter simulator added to simulator 
            }
            if (antennaNumber < Constants.FirstAntennaNumber || antennaNumber > Constants.LastAntennaNumber)
            {
                MessageLogger.Error($"Antenna number {antennaNumber} does not exist");
                return;
            }
            var attenuatorLoss = attenuator;
            if (attenuator < Constants.MinAttenuatorValue || attenuator > Constants.MaxAttenuatorValue)
                attenuatorLoss = 0;
            _converter.SetCommutatorAntenna((byte)antennaNumber, (byte)attenuatorLoss);
            await Task.Delay(Config.Instance.DirectionFindingSettings.CommutatorPauseTimeMs).ConfigureAwait(false);
        }

        public bool SetConverterFrequency(int frequencyMhz)
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
                return true;
            var result = _converter.SetConverterFrequency((short)frequencyMhz);
            if (!result)
                MessageLogger.Log($"{Role} Converter frequency failed to set to {frequencyMhz} MHz", ConsoleColor.Magenta);
            return result;
        }

        public async Task<bool> SetConverterFrequencyAsync(int frequencyMhz)
        {
            var config = Config.Instance.DirectionFindingSettings;
            var pauseTime = BandConstants.IsFrequencyLiesInLowerBand(frequencyMhz)
                ? config.LowerConverterPauseTimeMs : config.HigherConverterPauseTimeMs;
            if (Config.Instance.HardwareSettings.IsSimulation) 
            {
                await Task.Delay(pauseTime);
                return true;
            }
            var result = _converter.SetConverterFrequency((short)frequencyMhz);
            if (!result)
                MessageLogger.Log($"{Role} Converter frequency failed to set to {frequencyMhz} MHz", ConsoleColor.Magenta);
            await Task.Delay(pauseTime).ConfigureAwait(false);
            //Thread.Sleep(pauseTime);
            return result;
        }

        public bool Initialize() => _converter.OpenPort(ComPortName, BaudRate);

        /// <summary>
        /// Since we don't have a confirmation, we wait some time to be sure that settings have been applied
        /// </summary>
        private void Pause(int pauseTimeMs = 0)
        {
            Thread.Sleep(pauseTimeMs);
        }
    }
}
