﻿using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using Settings;
using System.Threading.Tasks;

namespace HardwareLibrary.Receivers
{
    public class ReceiverManager : IReceiverManager
    {
        public IReceiver MasterReceiver { get; }
        public IReceiver SlaveReceiver { get; }

        public IRelay Relay { get; }

        public ReceiverManager() : this(Config.Instance.HardwareSettings)
        { }

        public ReceiverManager(HardwareConfig config)
        {
            MasterReceiver = new Receiver(config.MasterReceiver.Address, config.MasterReceiver.BaudRate, FtmRole.Master);
            SlaveReceiver = new Receiver(config.SlaveReceiver.Address, config.SlaveReceiver.BaudRate, FtmRole.Slave);
            Relay = new Relay(config.Relay.Address, config.Relay.BaudRate);
        }

        public bool Initialize() =>
            MasterReceiver.Initialize()
            && SlaveReceiver.Initialize()
            && Relay.Initialize();

        public bool SetConverterBand(Band band)
        {
            SetRelay(band);
            return MasterReceiver.Converter.SetBand(band)
                && SlaveReceiver.Converter.SetBand(band);
        }

        public async Task<bool> SetConverterBandAsync(Band band)
        {
            SetRelay(band);
            if (MasterReceiver.Converter.CurrentBandNumber == band.Number
                && SlaveReceiver.Converter.CurrentBandNumber == band.Number)
            {
                return true; // avoid a pause, when no changes will be made
            }
            var result = MasterReceiver.Converter.SetBand(band)
                && SlaveReceiver.Converter.SetBand(band);
            var config = Config.Instance.DirectionFindingSettings;
            var pauseTime = BandConstants.IsFrequencyLiesInLowerBand(band.ConverterFrequencyMhz)
                 ? config.LowerConverterPauseTimeMs : config.HigherConverterPauseTimeMs;
            await Task.Delay(pauseTime);
            return result;
        }

        public void SetCommutatorAntennas(int masterAntennaIndex, int slaveAntennaIndex, AntennaSwitchSource antennaSwitchSource)
        {
            if (MasterReceiver.AntennaCommutator.WorkingAntennaIndex == masterAntennaIndex
                && SlaveReceiver.AntennaCommutator.WorkingAntennaIndex == slaveAntennaIndex)
            {                
                return; // avoid a pause, when no changes will be made
            }
            MasterReceiver.AntennaCommutator.SetWorkingAntenna(masterAntennaIndex, antennaSwitchSource);
            SlaveReceiver.AntennaCommutator.SetWorkingAntenna(slaveAntennaIndex, antennaSwitchSource);
            System.Threading.Thread.Sleep(Config.Instance.DirectionFindingSettings.CommutatorPauseTimeMs);
        }

        private bool SetRelay(Band band) 
        {
            if (band.Number <= Constants.BandThresholdNumber)
                return Relay.SetRangeFrom0To1Ghz();
            else
                return Relay.SetRangeFrom1To18Ghz();
        }
    }
}
