﻿using System;
using DspDataModel.Hardware;

namespace HardwareLibrary.Receivers
{
    public class AntennaCommutator : IAntennaCommutator
    {
        private readonly ConverterWrapper _converterWrapper;
        private readonly int[] _antennaSetup;

        public int WorkingAntennaIndex { get; private set; }

        public int DefaultAttenuator { get; set; } = 0;

        public event EventHandler<string> OnError;
        public event EventHandler<AntennaSwitchSource> WorkingAntennasChanged;

        public AntennaCommutator(ConverterWrapper wrapper, int[] antennaSetup)
        {
            _converterWrapper = wrapper;
            _antennaSetup = antennaSetup;
        }

        public void SetWorkingAntenna(int antennaId, AntennaSwitchSource antennaSwitchSource)
        {
            if (WorkingAntennaIndex == antennaId)
                return;
            var antennaNumber = _antennaSetup[antennaId];
            _converterWrapper.SetCommutatorAntenna(antennaNumber, DefaultAttenuator);
            WorkingAntennaIndex = antennaId;
            WorkingAntennasChanged?.Invoke(this, antennaSwitchSource);
        }

        public async System.Threading.Tasks.Task SetWorkingAntennaAsync(int antennaId, AntennaSwitchSource antennaSwitchSource)
        {
            if (WorkingAntennaIndex == antennaId)
                return;
            var antennaNumber = _antennaSetup[antennaId];
            await _converterWrapper.SetCommutatorAntennaAsync(antennaNumber, DefaultAttenuator);
            WorkingAntennaIndex = antennaId;
            WorkingAntennasChanged?.Invoke(this, antennaSwitchSource);
        }
    }
}