namespace FtmDeviceMessages
{
	extern enum DspDataModel.Hardware.CommandCodes
	extern enum DspDataModel.Hardware.FtmDeviceOutputMode
	extern enum DspDataModel.Hardware.SecondsMark
	extern enum DspDataModel.Hardware.WindowFunctionType
	extern enum DspDataModel.Hardware.NarrowFilterWidth
	extern enum DspDataModel.Hardware.DacVideoMode
	extern enum DspDataModel.Hardware.DacAudioMode
	extern enum DspDataModel.Hardware.Gps2DIndicationModes

	struct DefaultMessage
	{
		DspDataModel.Hardware.CommandCodes Code
	}

	struct SecondsMarkMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		DspDataModel.Hardware.SecondsMark Mark
	}

	struct SecondsCounterMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte Counter
	}

	struct ThresholdMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte Threshold
	}

	struct SetOutputModeMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		DspDataModel.Hardware.FtmDeviceOutputMode Mode
	}

	struct FftDelayMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		short Delay
	}

	struct WindowTypeMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		DspDataModel.Hardware.WindowFunctionType Type
	}

	struct SetIpMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte First
		byte Second
		byte Third
		byte Fourth
	}

	struct UpperOrPeakThresholdMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		short Threshold
	}

	struct NarrowFrequencyKhzMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte Low
		byte Middle
		byte High
	}

	struct NarrowBandwidthMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		DspDataModel.Hardware.NarrowFilterWidth Width
	}

	struct ForbiddenRangeMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		short StartMhz
		short EndMhz
	}

	struct ImpulseDurationMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte MinLowByte
		byte MinMiddleByte
		byte MinHighByte
		byte MaxLowByte
		byte MaxMiddleByte
		byte MaxHighByte
		bool IsActive
	}

	struct CalibrationFrequencyKhzMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte Low
		byte Middle
		byte High
	}

	struct CalibrationImpulseDurationMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte Low
		byte Middle
		byte High
	}

	struct CalibrationPeriodMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte Low
		byte Middle
		byte High
	}

	struct DacVideoModeMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		DspDataModel.Hardware.DacVideoMode Mode
	}

	struct DacAudioModeMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		DspDataModel.Hardware.DacVideoMode Mode
	}

	struct GpsStateMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte NumberOfSatellites
		DspDataModel.Hardware.Gps2DIndicationModes Gps2DMode
		byte Seconds
		byte Minutes
		byte Hours
	}

	struct VolumeMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		short Volume
	}

	struct IdMessage
	{
		DspDataModel.Hardware.CommandCodes Code
		byte Id
	}

	struct CalculatorResponseMessage
	{
		short FrequencyAndAmplitude
		byte BandwidthMhz
		byte ImpulseDurationLow
		byte ImpulseDurationMiddle
		byte ImpulseDurationHigh
		int ImpulseRepeatCount
		int ImpulseAppearanceStamp
		byte Minutes
		byte Hours
	}
}