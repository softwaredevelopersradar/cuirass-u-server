﻿using System;
using System.Linq;
using System.Net.Sockets;
using DspDataModel.Hardware;
using FtmDeviceMessages;
using DspDataModel;
using Settings;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace HardwareLibrary.Ftm
{
    public class FtmDevice : IFtmDevice
    {
        public UdpClient Client { get; }
    
        public UdpClient DataClient { get; }

        public bool IsConnected { get; private set; }

        public FtmDeviceOutputMode OutputMode { get; private set; }

        public IFtmDeviceSettings Settings { get; }

        public INarrowSettings NarrowSettings { get; }

        public IWideSettings WideSettings { get; }

        public int WidePicThreshold { get; private set; }

        public int WideAttackThreshold { get; private set; }

        public int NarrowPicThreshold { get; private set; }

        public int NarrowAttackThreshold { get; private set; }

        public FtmDevice(int port, int dataPort)
        {
            Client = new UdpClient(port);
            DataClient = new UdpClient(dataPort);
            Settings = new FtmDeviceSettings(Client);
            NarrowSettings = new NarrowSettings(Client);
            WideSettings = new WideSettings(Client);

            Client.Client.ReceiveTimeout = Config.Instance.HardwareSettings.DeviceResponseTimeoutMs;
            DataClient.Client.ReceiveTimeout = Config.Instance.HardwareSettings.DeviceResponseTimeoutMs;
        }

        public bool Connect(string deviceIpAddress, int devicePort)
        {
            Client.Connect(deviceIpAddress, devicePort);
            DataClient.Connect(deviceIpAddress, devicePort);
            if (!SearchDevice()) //ping connection
            {
                IsConnected = false;
                MessageLogger.Error($"Error during Ftm device connection. No reply from {deviceIpAddress}:{devicePort}.");
                return false;
            }
            IsConnected = true;
            return true;
        }

        public void Disconnect()
        {
            IsConnected = false;
        }

        public void GetGpsState()
        {
        }

        public bool Initialize()
        {
            SetOutputMode(FtmDeviceOutputMode.NoOutput);
            var response = Client.SendReceive(CommandCodes.Initialization);
            if(!Settings.Initialize(response))
                return false;
            if(!WideSettings.Initialize(response))
                return false;
            if (!NarrowSettings.Initialize(response))
                return false;
            return true;
        }

        /// <summary>
        /// Performs manual strobe to clear previous scan if needed, since ftm won't do it by itself
        /// </summary>
        private void ManualStrobe()
        {
            if (Settings.StrobeMode == StrobeMode.Auto)
                return;
            var request = new byte[] { (byte)CommandCodes.ManualStrobe , 0};
            Client.SendReceive(request);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] WideReadFft()
        {
            ManualStrobe();
            var data = Client.SendReceive(CommandCodes.WideReadFft);
            var output = new byte[Constants.WideBandFftSampleCount];
            //we omit first byte of command code
            //aswell as all bytes after we got Constants.WideBandFftSampleCount 
            //since they don't carry any information
            Buffer.BlockCopy(data, 1, output, 0, Constants.WideBandFftSampleCount);
            return output;
        }

        public byte[] NarrowReadFft()
        {
            ManualStrobe();
            var data = Client.SendReceive(CommandCodes.NarrowReadFft);
            var output = new byte[Constants.NarrowFftSampleCount];
            //we omit first byte of command code
            Buffer.BlockCopy(data, 1, output, 0, Constants.WideBandFftSampleCount);
            return output;
        }

        public bool SearchDevice()
        {
            var response = Client.SendReceive<DefaultMessage>(CommandCodes.SearchDevice);
            if (CommandCodes.SearchDevice == response.Code)
                return true;
            return false;
        }

        public void SetIpAddress()
        {
            throw new NotImplementedException();
        }

        public void SetOutputMode(FtmDeviceOutputMode mode)
        {
            var message = new SetOutputModeMessage(CommandCodes.SetOutputMode, mode).GetBytes();
            var response = Client.SendReceive<DefaultMessage>(message);
            OutputMode = mode;
        }

        public void StartNoiseCalibration()
        {
            var message = new byte[] {(byte)CommandCodes.StartNoiseCalibration, 1};
            Client.SendReceive(message);

            WideSettings.GetThreshold();
            WideSettings.SetThreshold(WideSettings.Threshold);
            WideSettings.GetAttackThreshold();
            WideSettings.SetAttackThreshold(WideSettings.AttackThreshold);

            NarrowSettings.GetThreshold();
            NarrowSettings.SetThreshold(NarrowSettings.Threshold);
            NarrowSettings.GetAttackThreshold();
            NarrowSettings.SetAttackThreshold(NarrowSettings.AttackThreshold);
        }

        //todo : REMOVE!
        private bool _isSimulation;
        public void SetSimulationStatus(bool isSimulation) 
        { 
            _isSimulation = isSimulation; 
        }

        public int[] VideoSpectrumRead(FtmRole isMaster = FtmRole.Master)
        {
            if (_isSimulation)
            {
                //todo : remove!!!!+!+!
                return isMaster == FtmRole.Master
                    ? KillmeSimulation.SimulateVideoSignal(7, 50).Select(v => (int)v).ToArray()
                    : KillmeSimulation.SimulateVideoSignal(7, 60).Select(v => (int)v).ToArray();
            }

            var result = new byte[16 * 1024];

            for (byte i = 0; i < 16; i++)
            {
                var subScan = Client.SendReceive(new byte[] { 0xae, i });
                Array.Copy(subScan, 2, result, 1024 * i, subScan.Length - 2);
            }

            var output = new int[result.Length / 2];

            for (int i = 0; i < result.Length; i += 2)
            {
                var dataSample = ((((ushort)result[i + 1]) << 8) | (ushort)result[i]);
                var dataVideo = 0x3fff & dataSample;
                var bitStrob = 0x4000 & dataSample;
                output[i / 2] = dataVideo;
            }
            return output;
        }

        public (int[], int[]) RfSignalSpectrumRead(FtmRole isMaster = FtmRole.Master)
        {
            if (_isSimulation)
            {
                var delta = isMaster == FtmRole.Master? 1 : 2;
                var raw = KillmeSimulation.SimulateRfSignalsWithoutWindow(delta).Select(r => (int)r).ToArray();
                var windowed = KillmeSimulation.SimulateRfSignalsWithWindow(delta).Select(r => (int)r).ToArray();
                return (raw, windowed);
            }

            var rawSignals = new int[1024];
            var windowedSignals = new int[1024];

            //TODO : CLEAN
            var result1 = Client.SendReceive(new byte[] { 0x8c, 0 });
            var result2 = Client.SendReceive(new byte[] { 0x8c, 1 });
            var result3 = Client.SendReceive(new byte[] { 0x8c, 2 });
            var result4 = Client.SendReceive(new byte[] { 0x8c, 3 });
            var with1 = new short[512];
            var with2 = new short[512];
            for (int i = 0; i < 1024; i += 2)
            {
                with1[i / 2] = Utilities.TwoBytesToShort(result3[i + 2], result3[i + 3]);
            }
            for (int i = 0; i < 1024; i += 2)
            {
                with2[i / 2] = Utilities.TwoBytesToShort(result4[i + 2], result4[i + 3]);
            }
            for (int i = 0; i < 512; i++)
            {
                windowedSignals[i] = with1[i];
            }
            for (int i = 0; i < 512; i++)
            {
                windowedSignals[i + 512] = with2[i];
            }

            var without1 = new short[512];
            var without2 = new short[512];

            for (int i = 0; i < 1024; i += 2)
            {
                without1[i / 2] = Utilities.TwoBytesToShort(result1[i + 2], result1[i + 3]);
            }
            for (int i = 0; i < 1024; i += 2)
            {
                without2[i / 2] = Utilities.TwoBytesToShort(result2[i + 2], result2[i + 3]);
            }

            for (int i = 0; i < 512; i++)
            {
                rawSignals[i] = without1[i];
            }
            for (int i = 0; i < 512; i++)
            {
                rawSignals[i + 512] = without2[i];
            }
            return (rawSignals, windowedSignals);
        }

        public IEnumerable<CalculatorSignal> GetCalculatorSignals(FtmDeviceOutputMode outputMode)
        {
            var output = new List<CalculatorSignal>(64); //1024 - is data size (1026 - 2), 16 per signal
            SetOutputMode(outputMode);
            var calculatorData = DataClient.Receive();
            SetOutputMode(FtmDeviceOutputMode.NoOutput);
            for (int i = 2; i < calculatorData.Length; i += 16)
            {
                output.Add(CalculatorHelper.TryParseToCalculatorSignal(calculatorData, i));
            }
            return output;
        }
    }
}
