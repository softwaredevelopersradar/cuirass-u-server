﻿using DspDataModel;
using DspDataModel.Hardware;

namespace HardwareLibrary.Ftm
{
    public class FtmDeviceManager : IFtmDeviceManager
    {
        public IFtmDevice Ftm2M { get; }
        public IFtmDevice Ftm2S { get; }
        
        //todo : change to filter manager or even direction finding config
        public double Threshold { get; set; } = ((byte)80).ToVoltAmplitude();
        public int NarrowFrequencyKhz { get; private set; } = 1_400_000;
        public NarrowFilterWidth NarrowBandWidthKhz { get; private set; } = NarrowFilterWidth.TenMHz;

        public VideoWorkingBand WorkingBand { get; private set; } = VideoWorkingBand.Off;

        public double SlaveAntennaCoefficient { get; set; } = 1;
        public double MasterAntennaCoefficient { get; set; } = 1;

        public FtmDeviceManager()
        {
            var appConfig = Config.Instance.AppSettings;

            var masterDataPort = Config.Instance.HardwareSettings.Ftm2M.DataPort;
            var slaveDataPort = Config.Instance.HardwareSettings.Ftm2S.DataPort;

            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                masterDataPort = Config.Instance.SimulatorSettings.SimulatorDataPort;
                slaveDataPort = Config.Instance.SimulatorSettings.SimulatorDataPort + 1;
            }
            var masterDevice = new FtmDevice(appConfig.ServerPort, masterDataPort);
            var slaveDevice = new FtmDevice(appConfig.ServerPort + 1, slaveDataPort);
            //todo : REMOVE!
            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                masterDevice.SetSimulationStatus(true);
                slaveDevice.SetSimulationStatus(true);
            }
            Ftm2M = masterDevice;
            Ftm2S = slaveDevice;
        }

        public bool Connect()
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                var simulatorAddress = Config.Instance.SimulatorSettings.SimulatorAddress;
                var simulatorPort = Config.Instance.SimulatorSettings.SimulatorPort;

                if (!Ftm2M.Connect(simulatorAddress, simulatorPort))
                    return false;
                if (!Ftm2S.Connect(simulatorAddress, simulatorPort + 1))
                    return false;
                MessageLogger.Warning("Ftm devices connected");
                return true;
            }

            var hardwareConfig = Config.Instance.HardwareSettings;
            if (!Ftm2M.Connect(hardwareConfig.Ftm2M.Address, hardwareConfig.Ftm2M.Port))
                return false;
            if (!Ftm2S.Connect(hardwareConfig.Ftm2S.Address, hardwareConfig.Ftm2S.Port))
                return false;
            MessageLogger.Warning("Ftm devices connected");
            return true;
        }

        public void Disconnect()
        {
            Ftm2M.Disconnect();
            Ftm2S.Disconnect();
        }

        public bool Initialize()
        {
            if (!Ftm2M.Initialize())
                return false;
            if (!Ftm2S.Initialize())
                return false;
            MessageLogger.Warning("Ftm devices initialized");
            return true;
        }

        public bool PingDevices() => Ftm2M.SearchDevice() && Ftm2S.SearchDevice();

        public void SetOutputMode(FtmDeviceOutputMode mode)
        {
            Ftm2M.SetOutputMode(mode);
            Ftm2S.SetOutputMode(mode);
        }

        public bool StartNoiseCalibration()
        {
            Ftm2M.StartNoiseCalibration();
            Ftm2S.StartNoiseCalibration();
            return true;
        }

        public void SetForbiddenRanges(int index, ForbiddenRange range)
        {
            Ftm2M.WideSettings.SetForbiddenRanges(index, range);
            Ftm2S.WideSettings.SetForbiddenRanges(index, range);
        }

        public void NarrowSetFrequency(int frequencyKhz)
        {
            NarrowFrequencyKhz = frequencyKhz;
            Ftm2M.NarrowSettings.SetFrequencyKhz(frequencyKhz);
            Ftm2S.NarrowSettings.SetFrequencyKhz(frequencyKhz);
        }

        public void SetFilterWidth(NarrowFilterWidth filterWidth)
        {
            NarrowBandWidthKhz = filterWidth;
            Ftm2M.NarrowSettings.SetFilterWidth(filterWidth);
            Ftm2S.NarrowSettings.SetFilterWidth(filterWidth);
        }

        public void WideGetFft(out byte[] masterWideFft, out byte[] slaveWideFft)
        {
            masterWideFft = Ftm2M.WideReadFft();
            slaveWideFft = Ftm2S.WideReadFft();
        }

        public void NarrowGetFft(out byte[] masterNbFft, out byte[] slaveNbFft)
        {
            masterNbFft = Ftm2M.NarrowReadFft();
            slaveNbFft = Ftm2S.NarrowReadFft();
        }

        public void SetVideoWorkingBand(VideoWorkingBand workingBand)
        {
            //todo : refactor
            Ftm2M.Client.SendReceive(new byte[] { (byte)CommandCodes.SetDacVideoMode, (byte)(workingBand + 1) });
            Ftm2S.Client.SendReceive(new byte[] { (byte)CommandCodes.SetDacVideoMode, (byte)(workingBand + 1) });
            WorkingBand = workingBand;
        }

        public IBandSettings GetBandSettings(FtmRole role, FtmMode mode)
        {
            var device = role == FtmRole.Master
                   ? Ftm2M
                   : Ftm2S;
            return mode == FtmMode.Wideband
                ? device.WideSettings as IBandSettings
                : device.NarrowSettings;
        }

        public bool SetStrobeMode(StrobeMode mode)
        {
            return Ftm2M.Settings.SetStrobeMode(mode) && Ftm2S.Settings.SetStrobeMode(mode);
        }

        public bool SetWindowType(WindowFunctionType type)
        {
            return Ftm2M.Settings.SetWindowFunctionType(type) && Ftm2S.Settings.SetWindowFunctionType(type);
        }

        bool IFtmDeviceManager.SetSecondsMark(SecondsMark mark)
        {
            return Ftm2M.Settings.SetSecondsMark(mark) && Ftm2S.Settings.SetSecondsMark(mark);
        }
    }
}
