﻿using System;
using System.Net.Sockets;
using DspDataModel.Hardware;
using FtmDeviceMessages;
using Settings;
using DspDataModel;

namespace HardwareLibrary.Ftm
{
    public class NarrowSettings : INarrowSettings
    {
        public int FrequencyKhz { get; private set; }

        public NarrowFilterWidth BandFilterWidth { get; private set; }

        public UdpClient Client { get; private set; }

        public byte Threshold { get; private set; }

        public short FftDelay { get; private set; }

        public short AttackThreshold { get; private set; }

        public short PicThreshold { get; private set; }

        public NarrowSettings(UdpClient client)
        {
            Client = client;
        }

        public void GetFftDelay()
        {
            var response = Client.SendReceive<FftDelayMessage>(CommandCodes.NarrowGetFftDelay);
            FftDelay = response.Delay;
        }

        public void GetFilterWidth()
        {
            var response = Client.SendReceive<NarrowBandwidthMessage>(CommandCodes.NarrowGetBandwidth);
            BandFilterWidth = response.Width;
        }

        public void GetFrequencyKhz()
        {
            var response = Client.SendReceive<NarrowFrequencyKhzMessage>(CommandCodes.NarrowGetFrequency);
            FrequencyKhz = Utilities.ThreeBytesToInt(response.Low, response.Middle, response.High);
        }

        public void GetPicThreshold()
        {
            var response = Client.SendReceive<UpperOrPeakThresholdMessage>(CommandCodes.NarrowGetPeakThreshold);
            PicThreshold = response.Threshold;
        }

        public void GetThreshold()
        {
            var response = Client.SendReceive<ThresholdMessage>(CommandCodes.NarrowGetThreshold);
            Threshold = response.Threshold;
        }

        public void GetAttackThreshold()
        {
            var response = Client.SendReceive<UpperOrPeakThresholdMessage>(CommandCodes.NarrowGetUpperThreshold);
            AttackThreshold = response.Threshold;
        }

        public bool Initialize(byte[] data)
        {
            try
            {
                Threshold = data[3];
                FftDelay = Utilities.TwoBytesToShort(data, 6);
                AttackThreshold = Utilities.TwoBytesToShort(data, 13);
                PicThreshold = Utilities.TwoBytesToShort(data, 15);
                FrequencyKhz = Utilities.ThreeBytesToInt(data, 18);
                BandFilterWidth = (NarrowFilterWidth) data[21];
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SetFftDelay(short fftDelay)
        {
            var message = new FftDelayMessage(CommandCodes.NarrowSetFftDelay, fftDelay);
            var response = Client.SendReceive<DefaultMessage>(message.GetBytes());
            if (response.Code == CommandCodes.NarrowSetFftDelay)
            {
                FftDelay = fftDelay;
                return true;
            }
            return false;
        }

        public void SetFilterWidth(NarrowFilterWidth filterWidth)
        {
            var message = new NarrowBandwidthMessage(CommandCodes.NarrowSetBandwidth, filterWidth);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            //after setting narrow bandwidth you need to update narrow band threshold since they are related to each other
            GetAttackThreshold();
            SetAttackThreshold(AttackThreshold);
            GetPicThreshold();
            SetAttackThreshold(PicThreshold);
            GetThreshold();
            SetThreshold(Threshold);
        }

        public void SetFrequencyKhz(int frequencyKhz)
        {
            var bytes = Utilities.IntToThreeBytes(frequencyKhz);
            var message = new NarrowFrequencyKhzMessage(
                CommandCodes.NarrowSetFrequency, 
                bytes[0], bytes[1], bytes[2]);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            FrequencyKhz = frequencyKhz;
        }

        public void SetPicThreshold(short peakThreshold)
        {
            if (peakThreshold < 0)
                return;
            var message = new UpperOrPeakThresholdMessage(CommandCodes.NarrowSetPeakThreshold, peakThreshold);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            PicThreshold = peakThreshold;
        }

        public void SetThreshold(byte threshold)
        {
            var message = new ThresholdMessage(CommandCodes.NarrowSetThreshold, threshold);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            Threshold = threshold;
        }

        public void SetAttackThreshold(short upperThreshold)
        {
            if (upperThreshold < 0)
                return;
            var message = new UpperOrPeakThresholdMessage(CommandCodes.NarrowSetUpperThreshold ,upperThreshold);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            AttackThreshold = upperThreshold;
        }
    }
}
