﻿using System;
using System.Net.Sockets;
using DspDataModel;
using DspDataModel.Hardware;
using FtmDeviceMessages;
using Settings;

namespace HardwareLibrary.Ftm
{
    //todo : remove this, since no-one really cares about all the settings?
    public class FtmDeviceSettings : IFtmDeviceSettings
    {
        public UdpClient Client { get; private set; }

        public SecondsMark SecondsMark { get; private set; }

        public WindowFunctionType WindowType { get; private set; }

        public StrobeMode StrobeMode { get; private set; }

        public byte PermissionByte { get; private set; }

        public DacVideoMode DacVideoMode { get; private set; }

        public DacVideoMode DacAudioMode { get; private set; }

        public short Volume { get; private set; }

        public FtmDeviceSettings(UdpClient client)
        {
            Client = client;
        }

        public void GetDacAudioMode()
        {}

        public void GetDacVideoMode()
        {}

        public void GetStrobeMode()
        {
            var response = Client.SendReceive(CommandCodes.GetStrobeMode);
            StrobeMode = (StrobeMode)response[1];
        }

        public void GetPermissionByte()
        {}

        public void GetSecondsMark()
        {}

        public void GetVolume()
        {}

        public void GetWindowFunctionType()
        {}

        public bool Initialize(byte[] data)
        {
            try
            {
                SecondsMark = (SecondsMark) data[1];
                WindowType = (WindowFunctionType) data[8];
                StrobeMode = (StrobeMode) data[17];
                PermissionByte = data[76];
                DacVideoMode = (DacVideoMode) data[77];
                DacAudioMode = (DacVideoMode) data[78];
                Volume = Utilities.TwoBytesToShort(data, 79);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void SetDacAudioMode()
        {}

        public void SetDacVideoMode()
        {}

        public bool SetStrobeMode(StrobeMode mode)
        {
            var response = Client.SendReceive(new byte[] { (byte)CommandCodes.SetStrobeMode, (byte)mode});
            if (response.Length == 0)
            {
                return false;
            }
            StrobeMode = mode;
            return true;
        }

        public void SetPermissionByte()
        {}

        public bool SetSecondsMark(SecondsMark mark)
        {
            var message = new SecondsMarkMessage(CommandCodes.SetSecondsMark, mark).GetBytes();
            var response = Client.SendReceive<DefaultMessage>(message);
            if (response.GetBytes().Length == 0)
                return false;
            SecondsMark = mark;
            return true;
        }

        public void SetVolume()
        {}

        public bool SetWindowFunctionType(WindowFunctionType type)
        {
            var message = new WindowTypeMessage(CommandCodes.SetWindowType, type).GetBytes();
            var response = Client.SendReceive<DefaultMessage>(message);
            if (response.GetBytes().Length == 0)
                return false;
            WindowType = type;
            return true;
        }
    }
}
