﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using DspDataModel;
using DspDataModel.Hardware;
using FtmDeviceMessages;
using Settings;

namespace HardwareLibrary.Ftm
{
    public class WideSettings : IWideSettings
    {
        public UdpClient Client { get; private set; }   

        public byte Threshold { get; private set; }

        public short FftDelay { get; private set; }

        public short AttackThreshold { get; private set; }

        public short PicThreshold { get; private set; }

        public IList<ForbiddenRange> ForbiddenRanges { get; private set; }

        public SelectionSetting ImpulseDurationSettings { get; private set; }

        public SelectionSetting ImpulseRepeatSettings { get; private set; }

        public WideSettings(UdpClient client)
        {
            ForbiddenRanges = new List<ForbiddenRange>(Constants.ForbiddenRangesCount);
            Client = client;
        }

        public void GetFftDelay()
        {
            var response = Client.SendReceive<FftDelayMessage>(CommandCodes.WideGetFftDelay);
            FftDelay = response.Delay;
        }

        public void GetForbiddenRanges()
        {
            for (int i = 0; i < ForbiddenRanges.Count; i++)
            {
                var response = Client.SendReceive<ForbiddenRangeMessage>(CommandCodes.WideGetForbiddenRange + i);
                ForbiddenRanges[i] = new ForbiddenRange(response.StartMhz, response.EndMhz);
            }
        }

        public void GetForbiddenRanges(int index)
        {
            var response = Client.SendReceive<ForbiddenRangeMessage>(CommandCodes.WideGetForbiddenRange + index);
            ForbiddenRanges[index] = new ForbiddenRange(response.StartMhz, response.EndMhz);
        }

        public void GetImpulseDurationSettings()
        {
            var response = Client.SendReceive<ImpulseDurationMessage>(CommandCodes.WideGetImpulseDuration);
            var minImpulses = Utilities.ThreeBytesToInt(response.MinLowByte, response.MinMiddleByte, response.MinHighByte);
            var maxImpulses = Utilities.ThreeBytesToInt(response.MaxLowByte, response.MaxMiddleByte, response.MaxHighByte);
            ImpulseDurationSettings = new SelectionSetting(minImpulses, maxImpulses, response.IsActive);
        }

        public void GetImpulseRepeatSettings()
        {
            throw new System.NotImplementedException();
        }

        public void GetPicThreshold()
        {
            var response = Client.SendReceive<UpperOrPeakThresholdMessage>(CommandCodes.WideGetPeakThreshold);
            PicThreshold = response.Threshold;
        }

        public void GetThreshold()
        {
            var response = Client.SendReceive<ThresholdMessage>(CommandCodes.WideGetThreshold);
            Threshold = response.Threshold;
        }

        public void GetAttackThreshold()
        {
            var response = Client.SendReceive<UpperOrPeakThresholdMessage>(CommandCodes.WideGetUpperThreshold);
            AttackThreshold = response.Threshold;
        }

        public bool Initialize(byte[] data)
        {
            try
            {
                Threshold = data[2];
                FftDelay = Utilities.TwoBytesToShort(data, 4);
                AttackThreshold = Utilities.TwoBytesToShort(data, 9);
                PicThreshold = Utilities.TwoBytesToShort(data, 11);

                for (int i = 0; i < Constants.ForbiddenRangesCount; i++)
                {
                    var start = Utilities.TwoBytesToShort(data, 22 + i * 4);
                    var end = Utilities.TwoBytesToShort(data, 24 + i * 4);
                    ForbiddenRanges.Add(new ForbiddenRange(start, end));
                }

                ImpulseDurationSettings = new SelectionSetting(
                    Utilities.ThreeBytesToInt(data,62),
                    Utilities.ThreeBytesToInt(data,65),
                    data[68] == 1);

                ImpulseRepeatSettings = new SelectionSetting(
                    Utilities.ThreeBytesToInt(data, 69),
                    Utilities.ThreeBytesToInt(data, 72),
                    data[75] == 0x80);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SetFftDelay(short fftDelay)
        {
            var message = new FftDelayMessage(CommandCodes.WideSetFftDelay, fftDelay);
            var response = Client.SendReceive<DefaultMessage>(message.GetBytes());
            if (response.Code == CommandCodes.WideSetFftDelay)
            {
                FftDelay = fftDelay;
                return true;
            }
            return false;
        }

        public void SetForbiddenRanges(int index, ForbiddenRange range)
        {
            try
            {
                var message = new ForbiddenRangeMessage(CommandCodes.WideSetForbiddenRange + index, (short)range.StartMhz, (short)range.GetEndMhz());
                Client.SendReceive<DefaultMessage>(message.GetBytes());
                ForbiddenRanges[index] = range;
            }
            catch(OverflowException e)
            {
                MessageLogger.Error($"Overflow exception during int to short convertion, {e}");
            }
        }

        public void SetForbiddenRanges(IDictionary<int,ForbiddenRange> ranges)
        {
            foreach (var key in ranges.Keys)
            {
                SetForbiddenRanges(key, ranges[key]);
            }
        }

        public void SetImpulseDurationSettings(SelectionSetting setting)
        {
            var minImpulses = Utilities.IntToThreeBytes(setting.MinImpulses);
            var maxImpulses = Utilities.IntToThreeBytes(setting.MaxImpluses);
            var message = new ImpulseDurationMessage(
                CommandCodes.WideSetImpulseDuration, 
                minImpulses[0], minImpulses[1], minImpulses[2],
                maxImpulses[0], maxImpulses[1], maxImpulses[2],
                setting.IsActive);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            ImpulseDurationSettings = setting;
        }

        public void SetImpulseRepeatSettings(SelectionSetting setting)
        {}

        public void SetPicThreshold(short peakThreshold)
        {
            if (peakThreshold < 0)
                return;
            var message = new UpperOrPeakThresholdMessage(CommandCodes.WideSetPeakThreshold, peakThreshold);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            PicThreshold = peakThreshold;
        }

        public void SetThreshold(byte threshold)
        {
            var message = new ThresholdMessage(CommandCodes.WideSetThreshold, threshold);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            Threshold = threshold;
        }

        public void SetAttackThreshold(short upperThreshold)
        {
            if (upperThreshold < 0)
                return;
            var message = new UpperOrPeakThresholdMessage(CommandCodes.WideSetUpperThreshold, upperThreshold);
            Client.SendReceive<DefaultMessage>(message.GetBytes());
            AttackThreshold = upperThreshold;
        }
    }
}
