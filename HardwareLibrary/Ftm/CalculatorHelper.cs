﻿using System;
using Settings;
using FtmDeviceMessages;
using DspDataModel.Data;
using DspDataModel.Hardware;
using System.Linq;
using System.Collections.Generic;

namespace HardwareLibrary.Ftm
{
    public static class CalculatorHelper
    {
        public static CalculatorSignal TryParseToCalculatorSignal(this byte[] data, int offset = 0) 
        {
            try
            {
                var calculatorResponse = CalculatorResponseMessage.Parse(data, offset);

                var bitFrequencyAndAmplitude = Convert.ToString(calculatorResponse.FrequencyAndAmplitude, 2);
                while (bitFrequencyAndAmplitude.Length < 16)
                {
                    bitFrequencyAndAmplitude = "0" + bitFrequencyAndAmplitude; //we need to add zeroes that were removed as non-informative, because bits bits bits ._.
                }

                var bitFrequency = bitFrequencyAndAmplitude.Remove(9);
                var bitAmplitude = bitFrequencyAndAmplitude.Remove(0, 9);

                var frequencyMhz = Convert.ToInt32(bitFrequency, 2) + 1100;
                var amplitude = Convert.ToInt32(bitAmplitude, 2);

                var bitAppearance = Convert.ToString(calculatorResponse.ImpulseAppearanceStamp, 2);
                while (bitAppearance.Length < 32)
                {
                    bitAppearance = "0" + bitAppearance; //we need to add zeroes that were removed as non-informative, because bits bits bits ._.
                }

                var bitSeconds = bitAppearance.Remove(6);
                var bitTicks = bitAppearance.Remove(0, 6);

                var seconds = Convert.ToInt32(bitSeconds, 2);
                var ticks = Convert.ToInt32(bitTicks, 2);

                var durationNs = Utilities.ThreeBytesToInt(calculatorResponse.ImpulseDurationLow, calculatorResponse.ImpulseDurationMiddle,
                               calculatorResponse.ImpulseDurationHigh) * 5;
                var repeatPeriodNs = calculatorResponse.ImpulseRepeatCount * 5;
                //Constant 5 is given in protocol, you can look at it in Documents/Protocol FTM V21.doc

                return new CalculatorSignal(
                   frequencyMhz: frequencyMhz,
                   amplitude: amplitude,
                   bandwidthMhz: calculatorResponse.BandwidthMhz,
                   durationNs: durationNs,
                   repeatPeriodNs: repeatPeriodNs,
                   hours: calculatorResponse.Hours,
                   minutes: calculatorResponse.Minutes,
                   seconds: seconds,
                   ticks: ticks); 
            }
            catch
            {
                return new CalculatorSignal();
            }
        }

        /// <summary>
        /// Searches for the best fit by frequency from calculator signals and updates our signal bandwidth, duration and repeat period respectively
        /// </summary>
        public static void TryUpdateCalculatorValues(this ISignal signal, float ftmSignalFrequencyKhz, IEnumerable<CalculatorSignal> calculatorSignals) 
        {
            var calculatorSignal = calculatorSignals.FirstOrDefault(s => Math.Abs(s.FrequencyMhz * 1000 - ftmSignalFrequencyKhz) <= 3_500);
            if (ValidateCalculatorSignal(calculatorSignal))
                signal.UpdateCalculatorValues(calculatorSignal.BandwidthMhz * 1000, calculatorSignal.DurationNs, calculatorSignal.RepeatPeriodNs);
            else
                signal.Note = "Failed to measure";//todo : add language support
        }

        private static bool ValidateCalculatorSignal(CalculatorSignal calculatorSignal)
        {
            return calculatorSignal.FrequencyMhz != 0 && calculatorSignal.Amplitude != 0;
        }
    }
}
