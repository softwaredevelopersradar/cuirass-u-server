﻿using System;

namespace DspDataModel
{
    public interface IMessageLogger
    {
        ConsoleColor ErrorColor { get; set; }
        bool IsFtmLogEnabled { get; set; }
        bool IsServerLogEnabled { get; set; }
        bool IsTraceLogEnabled { get; set; }
        ConsoleColor MessageColor { get; set; }
        ConsoleColor WarningColor { get; set; }

        void Error(Exception e);
        void Error(Exception e, string message);
        void Error(string message);
        void FtmLog(string message);
        void Log(string message);
        void Log(string message, ConsoleColor color);
        void ServerLog(string message);
        void Trace(string message, ConsoleColor color = ConsoleColor.Gray);
        void Warning(string message);
    }
}