﻿using System;

namespace DspDataModel
{
    public class NullLogger : IMessageLogger
    {
        public ConsoleColor ErrorColor { get; set; }
        public bool IsFtmLogEnabled { get; set; }
        public bool IsServerLogEnabled { get; set; }
        public bool IsTraceLogEnabled { get; set; }
        public ConsoleColor MessageColor { get; set; }
        public ConsoleColor WarningColor { get; set; }

        public void Error(Exception e)
        { }

        public void Error(Exception e, string message)
        { }

        public void Error(string message)
        { }

        public void FtmLog(string message)
        { }

        public void Log(string message)
        { }

        public void Log(string message, ConsoleColor color)
        { }

        public void ServerLog(string message)
        { }

        public void Trace(string message, ConsoleColor color = ConsoleColor.Gray)
        { }

        public void Warning(string message)
        { }
    }
}