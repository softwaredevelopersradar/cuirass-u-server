﻿using System;
using System.Linq;
using NLog;

namespace DspDataModel
{
    public class NLogLogger : IMessageLogger
    {
        public ConsoleColor ErrorColor { get; set; } = ConsoleColor.Red;
        public ConsoleColor WarningColor { get; set; } = ConsoleColor.Yellow;
        public ConsoleColor MessageColor { get; set; } = ConsoleColor.Gray;

        private Logger FpgaLogger { get; }

        private Logger ServerLogger { get; }

        private Logger TraceLogger { get; }

        public bool IsFtmLogEnabled { get; set; }
        public bool IsServerLogEnabled { get; set; }
        public bool IsTraceLogEnabled { get; set; }

        private Logger ErrorLogger { get; }

        public NLogLogger()
        {
            try
            {
                FpgaLogger = LogManager.GetLogger("Fpga");
                ErrorLogger = LogManager.GetLogger("Error");
                ServerLogger = LogManager.GetLogger("Server");
                TraceLogger = LogManager.GetLogger("Trace");

                var logConfig = LogManager.Configuration;
                if (logConfig == null)
                {
                    return;
                }
                if (logConfig.Variables.ContainsKey(nameof(IsFtmLogEnabled)))
                {
                    IsFtmLogEnabled = logConfig.Variables[nameof(IsFtmLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsServerLogEnabled)))
                {
                    IsServerLogEnabled = logConfig.Variables[nameof(IsServerLogEnabled)].Text == "true";
                }
                if (logConfig.Variables.ContainsKey(nameof(IsTraceLogEnabled)))
                {
                    IsTraceLogEnabled = logConfig.Variables[nameof(IsTraceLogEnabled)].Text == "true";
                }
            }
            // something get wrong - nlog config file doesn't exist or nlog assembly is not loaded
            catch
            {
                // ignored
                // in this case all logging is switched off
            }
        }

        public void Log(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
        }

        public void FtmLog(string message)
        {
            if (IsFtmLogEnabled)
            {
                Log(FpgaLogger, message);
            }
        }

        public void ServerLog(string message)
        {
            if (IsServerLogEnabled)
            {
                Log(ServerLogger, message);
            }
        }

        /// <summary>
        /// Traces inner app methods
        /// </summary>
        public void Trace(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            if (IsTraceLogEnabled)
            {
                Log(TraceLogger, message, color);
            }
        }

        private void Log(ILogger logger, string seed, byte[] data)
        {
            var message = data.Aggregate(seed, (s, b) => s + "\t" + b.ToString("X"));
            Log(logger, message);
        }

        private void Log(ILogger logger, string message, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            logger.Trace(message);
        }

        public void Log(string message)
        {
            Log(message, MessageColor);
        }

        public void Warning(string message)
        {
            Log(message, WarningColor);
        }

        public void Error(string message)
        {
            Log(message, ErrorColor);
            ErrorLogger.Error(message);
        }

        public void Error(Exception e)
        {
            Error(e, e.Message + "\n" + e.StackTrace);
        }

        public void Error(Exception e, string message)
        {
            Log($"{message}: {e.StackTrace}", ErrorColor);
            ErrorLogger.Error(e);
        }
    }
}