﻿using System.Collections.Generic;
using DspDataModel.Data;

namespace DspDataModel.Storages
{
    public interface ISignalStorage
    {
        IReadOnlyList<ISignal> Signals { get; }
        void Put(IReadOnlyList<ISignal> signals);
        ISignal GetSignal(int signalId);
        bool Remove(int signalId);
        void Clear();
    } 
}
