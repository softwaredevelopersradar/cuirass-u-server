﻿namespace DspDataModel.Storages
{
    public interface IScanStorage<T>
    {
        T MasterStorage { get; }
        T SlaveStorage { get; }

        void Put(T scan, FtmRole role);
        void Clear();
        T GetStorage(FtmRole role);
    }
}
