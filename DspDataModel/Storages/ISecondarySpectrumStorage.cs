﻿using DspDataModel.Data;

namespace DspDataModel.Storages
{
    public interface ISecondarySpectrumStorage
    {
        IScanStorage<FtmScan> NarrowSpectrumStorage { get; }
        IScanStorage<VideoScan> VideoSpectrumStorage { get; }
        IScanStorage<RfSignalScan> RfSpectrumStorage { get; }

        void PutNarrow(FtmScan scan, FtmRole role);
        void PutVideo(VideoScan scan, FtmRole role);
        void PutRf(RfSignalScan scan, FtmRole role);
    }
}
