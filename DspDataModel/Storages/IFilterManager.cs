﻿using DspDataModel.Data;
using DspDataModel.Hardware;
using System;
using System.Collections.Generic;

namespace DspDataModel.Storages
{
    public interface IFilterManager
    {
        IReadOnlyList<FrequencyRange> IntelligenceRanges { get; }
        IReadOnlyList<FrequencyRange> ForbiddenRanges { get; }
        IReadOnlyList<DirectionSector> IntelligenceSectors { get; }
        AntennaSetting AntennaSettings { get; set; }
        FrequencyRange WideMeasureRange { get; set; }
        NarrowVideoClarificationSetting NarrowVideoSettings { get; set; }
        bool IsWaterfallEnabled { get; set; }
        bool IsNarrowMeasuringEnabled { get; set; }

        event EventHandler<IReadOnlyList<FrequencyRange>> IntelligenceRangesUpdatedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesUpdatedEvent;

        IEnumerable<(int MasterIndex, int SlaveIndex)> GetRdfSettings();

        bool SetIntelligenceRanges(IEnumerable<FrequencyRange> intelligenceRanges);
        bool SetForbiddenRanges(IEnumerable<FrequencyRange> forbiddenRanges);
        bool SetIntelligenceSectors(IEnumerable<DirectionSector> intelligenceSectors);
    }
}
