﻿namespace DspDataModel.Storages
{
    public interface IMultiScanStorage<T>
    {
        int NumberOfScans { get; }

        void Put(T scan, int scanIndex, FtmRole role);
        T GetScan(int scanIndex, FtmRole role);

        void Clear();
    }
}
