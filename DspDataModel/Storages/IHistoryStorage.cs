﻿namespace DspDataModel.Storages
{
    public interface IHistoryStorage
    {
        void Start();
        void Stop();

        void AddScan(byte[] scan);
        byte[] GetHistory();
        void Clear();

        int GetNumberOfPointsPerScan();
    }
}
