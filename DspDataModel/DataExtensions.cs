﻿using System;
using DspDataModel.Data;
using DspDataModel.Hardware;

namespace DspDataModel
{
    public static class DataExtensions
    {
        //todo: 512 for narrow?
        public static double ToVoltAmplitude(this byte byteAmplitude) =>
            Math.Pow(10, (byteAmplitude - 30 - 40 * Math.Log(466, 10)) / 20);

        public static byte ToByteAmplitude(this double voltAmplitude) =>
            (byte) (20 * Math.Log(voltAmplitude, 10) + 30 + 40 * Math.Log(466, 10));

        public static float NarrowFilterWidthToMhz(this NarrowFilterWidth filterWidth)
        {
            switch (filterWidth)
            {
                case NarrowFilterWidth.OneMHz: return 1;
                case NarrowFilterWidth.TwoMHz: return 2;
                case NarrowFilterWidth.FiveMHz: return 5;
                case NarrowFilterWidth.TenMHz: return 10;
                case NarrowFilterWidth.TwentyMHz: return 20;
                case NarrowFilterWidth.FiftyMHz: return 50;
                default: return 0;
            }
        }

        public static bool AreSameSignals(this ISignal self, ISignal signal)
        {
            var config = Config.Instance.DirectionFindingSettings;
            return Math.Abs(self.FrequencyKhz - signal.FrequencyKhz) < config.SignalMergeGapMKz &&
                   GetAngle(self.Direction, signal.Direction) < config.SignalMergeDirectionDeviation;
        }

        /// <summary>
        /// Returns angle between two directions
        /// </summary>
        public static double GetAngle(double direction1, double direction2) 
        {
            var a1 = Math.Abs(direction1 - direction2);
            var a2 = 360 - a1;
            return Math.Min(a1, a2);
        }
    }
}
