﻿namespace DspDataModel.Data
{
    public interface ISignal
    {
        int Id { get; }
        float FrequencyKhz { get; }
        byte Amplitude { get; }
        double AmplitudeVolt { get; }
        float Direction { get; }
        float BandwidthKhz { get; }
        float StandardDeviation { get; }
        float DurationMcs { get; }
        float RepeatPeriodMcs { get; }
        int MasterAntennaIndex { get; }
        int SlaveAntennaIndex { get; }
        string Note { get; set; }

        void Update(ISignal newSignal);
        void UpdateCalculatorValues(int bandWidthKhz, int durationNs, int repeatPeriodNs);
    }
}
