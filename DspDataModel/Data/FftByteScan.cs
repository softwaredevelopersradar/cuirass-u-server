﻿using System.Linq;
using System.Collections.Generic;

namespace DspDataModel.Data
{
    //todo : remove me?
    public struct FftByteScan
    {
        /// <summary>
        /// Amplitudes as bytes, raw
        /// </summary>
        public byte[] Amplitudes { get; }

        public FftByteScan(IEnumerable<byte> amplitudes)
        {
            Amplitudes = amplitudes.ToArray();//creates a copy
        }
    }
}
