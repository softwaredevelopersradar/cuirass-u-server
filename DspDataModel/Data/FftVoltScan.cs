﻿using System.Linq;
using System.Collections.Generic;

namespace DspDataModel.Data
{
    public struct FftVoltScan
    {
        /// <summary>
        /// Amplitudes as volts
        /// </summary>
        public double[] Amplitudes { get; }

        public FftVoltScan(IEnumerable<byte> amplitudes)
        {
            Amplitudes = amplitudes.Select(e => e.ToVoltAmplitude()).ToArray();
        }

        public FftVoltScan(double[] amplitudes) 
        {
            Amplitudes = amplitudes;
        }
    }
}
