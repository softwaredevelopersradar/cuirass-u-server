﻿namespace DspDataModel.Data
{
    public struct VideoScan
    {
        public int[] Scan { get; private set; }

        public VideoScan(int[] scan) 
        {
            Scan = scan;
        }
    }
}
