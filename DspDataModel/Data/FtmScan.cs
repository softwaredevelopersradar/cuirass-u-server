﻿using System;
using System.Threading;

namespace DspDataModel.Data
{
    public struct FtmScan
    {
        private static int _indexFactory;

        public int ScanIndex { get; }

        public int BandNumber { get; }

        //todo : rename to raw scan?
        public byte[] Scan { get; }

        public DateTime CreationTime { get; }

        public FtmScan(byte[] scan, int bandNumber) 
        {
            ScanIndex = Interlocked.Increment(ref _indexFactory);
            Scan = scan;
            BandNumber = bandNumber;
            CreationTime = DateTime.UtcNow;
        }
    }
}
