﻿using Settings;
using System;

namespace DspDataModel.Data
{
    public struct Band : IEquatable<Band>
    {
        public int Number { get; }
        public int FrequencyFromMhz { get; }
        public int FrequencyToMhz { get; }
        public int ConverterFrequencyMhz { get; }
        public bool Inversed { get; }
        public int ScanSampleCount { get; }
        public bool IsLowerBand => Number <= Constants.BandThresholdNumber;

        public Band(int bandNumber)
        {
            Number = bandNumber;
            Inversed = BandConstants.IsBandInversed(bandNumber);

            var range = BandConstants.GetBandRangeSettings(bandNumber);
            var index = bandNumber - range.StartBandNumber;
            var halfRange = range.BandWidthMhz / 2;

            ConverterFrequencyMhz = range.ConverterFrequencyMhz + range.BandWidthMhz * index;
            if (bandNumber != range.StartBandNumber)
            {
                FrequencyFromMhz = range.ConverterFrequencyMhz + range.BandWidthMhz * index - halfRange;
                FrequencyToMhz = range.ConverterFrequencyMhz + range.BandWidthMhz * index + halfRange;

                ScanSampleCount = Constants.WideBandFftSampleCount;
                if (IsLowerBand)
                    ScanSampleCount = Constants.LowerBandWideFftSampleCount;
            }
            else
            {
                FrequencyFromMhz = range.FirstBandFrequencyRangeStartMhz;
                FrequencyToMhz = range.FirstBandFrequencyRangeEndMhz;

                ScanSampleCount = range.FirstBandNumberOfSamples;
            }
        }

        public bool Equals(Band other) => other.Number == Number;
        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            return other is Band band && Equals(this, band);
        }
    }
}
