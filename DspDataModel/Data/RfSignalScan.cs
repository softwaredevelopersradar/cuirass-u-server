﻿namespace DspDataModel.Data
{
    public struct RfSignalScan
    {
        public int[] RawScan { get; private set; }
        public int[] WindowedScan { get; private set; }

        public RfSignalScan(int[] rawScan, int[] windowedScan) 
        {
            RawScan = rawScan;
            WindowedScan = windowedScan;
        }
    }
}
