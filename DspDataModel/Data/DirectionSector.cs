﻿namespace DspDataModel.Data
{
    public struct DirectionSector
    {
        public int SectorStart { get; }
        public int SectorEnd { get; }

        public DirectionSector(int startAngle, int endAngle) 
        {
            if (!IsAngleValid(startAngle) || !IsAngleValid(endAngle))
            {
                startAngle = -1;
                endAngle = -1;
            }

            SectorStart = startAngle;
            SectorEnd = endAngle;
        }

        private static bool IsAngleValid(int angle) 
        {
            return angle >= 0 && angle < 360;
        }

        public bool Contains(float angle) 
        {
            return angle >= SectorStart && angle <= SectorEnd;
        }
    }
}
