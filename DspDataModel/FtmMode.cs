﻿namespace DspDataModel
{
    public enum FtmMode : byte
    {
        Wideband = 0,
        Narrowband = 1
    }
}
