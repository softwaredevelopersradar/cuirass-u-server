﻿using System;
using System.Collections.Generic;
using DspDataModel.Data;
using KirasaUModelsDBLib;

namespace DspDataModel.Database
{
    public interface IDatabaseController
    {
        bool IsConnected { get; }

        event EventHandler OnConnectToDatabase;
        event EventHandler OnDisconnectFromDatabase;
        event EventHandler<string> OnDatabaseError;

        event EventHandler<GlobalProperties> OnGlobalPropertiesUpdated;
        event EventHandler<IEnumerable<FrequencyRange>> OnForbiddenFrequenciesUpdated;
        event EventHandler<IEnumerable<FrequencyRange>> OnReconFrequenciesUpdated;
        event EventHandler<IEnumerable<DirectionSector>> OnSectorsUpdated;

        void Connect();
        void Disconnect();

        void AddFrsSignals(IEnumerable<ISignal> signals);

        void ClearFrsSignalsTable();
    }
}
