﻿using DspDataModel.Data;
using System.Linq;
using System.Collections.Generic;

namespace DspDataModel.DataProcessor
{
    public struct IndexMostEntriesCounter
    {
        public int Index { get; }
        private readonly Dictionary<double, int> LevelsEntries;

        public IndexMostEntriesCounter(int index, IReadOnlyList<FftVoltScan> scans)
        {
            Index = index;
            LevelsEntries = new Dictionary<double, int>(scans.Count);
            GetEntries(scans);
        }

        public double GetMostEntriedAmplitude()
        {
            var maxValue = LevelsEntries.Values.Max();
            return LevelsEntries.First(k => k.Value == maxValue).Key;
        }

        private void GetEntries(IReadOnlyList<FftVoltScan> scans)
        {
            for (int i = 0; i < scans.Count; i++)
            {
                var entryAmplitude = scans[i].Amplitudes[Index];
                var entryKeyExists = LevelsEntries.ContainsKey(entryAmplitude);
                if (entryKeyExists)
                    LevelsEntries[entryAmplitude] += 1;
                else
                    LevelsEntries.Add(entryAmplitude, 1);
            }
        }
    }
}
