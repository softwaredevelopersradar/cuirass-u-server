﻿using DspDataModel.Data;
using System;
using System.Linq;
using System.Collections.Generic;

namespace DspDataModel.DataProcessor
{
    //todo : move to data processor library
    public class PossibleSignal
    {
        public IReadOnlyList<IndexAmplitudes> Indices { get; }
        public IndexAmplitudes PeakIndex { get; private set; }
        public double Direction { get; private set; }
        public double StandardDeviation { get; private set; }
        
        public int MasterAntennaIndex { get; }
        public int SlaveAntennaIndex { get; }

        public PossibleSignal(IEnumerable<int> indices,
            IReadOnlyList<FftVoltScan> masterScans,
            IReadOnlyList<FftVoltScan> slaveScans,
            float startFrequencyKhz, float stepKhz,
            int masterAntennaIndex, int slaveAntennaIndex) 
        {
            var masterIndices = GetScansMostEntries(indices, masterScans);
            var slaveIndices = GetScansMostEntries(indices, slaveScans);
            Indices = GetIndices(masterIndices, slaveIndices, startFrequencyKhz, stepKhz);
            PeakIndex = GetPeakIndex();

            MasterAntennaIndex = masterAntennaIndex;
            SlaveAntennaIndex = slaveAntennaIndex;

            column = new double[masterScans.Count];
            var startFrequencyMhz = startFrequencyKhz / 1000;
            CalculateDirectionAndDeviation(indices, masterScans, slaveScans, startFrequencyMhz);
        }

        private void CalculateDirectionAndDeviation(IEnumerable<int> indices, IReadOnlyList<FftVoltScan> master, IReadOnlyList<FftVoltScan> slave, float frequencyMhz)
        {
            var directions = new List<double>(indices.Count());
            foreach (var index in indices)
            {
                var columnDirection = CalculateColumnDirection(index, master, slave, frequencyMhz);
                if (double.IsNaN(columnDirection))
                    continue;
                directions.Add(columnDirection);
            }

            if (directions.Count == 0)
            {
                Direction = float.NaN;
                StandardDeviation = float.NaN;
            }

            var averageDirection = directions.Average();
            Direction = averageDirection;
            StandardDeviation = CalculateStandardDeviation(directions, averageDirection);
        }
        private readonly double[] column;
        private double CalculateColumnDirection(int index, IReadOnlyList<FftVoltScan> master, IReadOnlyList<FftVoltScan> slave, float frequencyMhz) 
        {
            for (int i = 0; i < column.Length; i++)
            {
                column[i] = CalculateDirection(frequencyMhz, master[i].Amplitudes[index], slave[i].Amplitudes[index]);
            }

            var mean = column.Average();
            var std = CalculateStandardDeviation(column, mean);

            var clarifiedColumn = new List<double>(column.Length);

            for (int i = 0; i < column.Length; i++)
            {
                if (Math.Abs(mean - column[i]) <=  std * 1.5) 
                {
                    clarifiedColumn.Add(column[i]);
                }
            }

            if (clarifiedColumn.Count == 0)
                return double.NaN;
            return clarifiedColumn.Average();
        }

        private static double CalculateDirection(float frequencyMhz, double masterAmplitude, double slaveAmplitude)
        {
            var range = Config.Instance.CoefficientSettings.Records.FirstOrDefault(r =>
                r.StartFrequencyMhz <= frequencyMhz && r.EndFrequencyMhz > frequencyMhz);
            var coefficient = range.Coefficient != 0 ? range.Coefficient : 1;
            return (-1 * masterAmplitude + slaveAmplitude) / (masterAmplitude + slaveAmplitude) * coefficient;
        }

        private IndexMostEntriesCounter[] GetScansMostEntries(IEnumerable<int> indices, IReadOnlyList<FftVoltScan> scans) 
        {
            var indexMostEntriesCounter = new IndexMostEntriesCounter[indices.Count()];
            for (int i = 0; i < indices.Count(); i++)
            {
                indexMostEntriesCounter[i] = new IndexMostEntriesCounter(indices.ElementAt(i), scans);
            }
            return indexMostEntriesCounter;
        }

        private IReadOnlyList<IndexAmplitudes> GetIndices(IndexMostEntriesCounter[] masterIndices, IndexMostEntriesCounter[] slaveIndices,
            float startFrequencyKhz, float stepKhz) 
        {
            var list = new List<IndexAmplitudes>(masterIndices.Count());
            for (int i = 0; i < masterIndices.Length; i++)
            {
                var index = new IndexAmplitudes(masterIndices[i], slaveIndices[i], startFrequencyKhz, stepKhz);
                list.Add(index);
            }
            return list;
        }

        IList<double> stdList;

        private double CalculateStandardDeviation(IList<double> directions , double averageDirection) 
        {
            if (directions.Count == 1)
                return 0;
            stdList = new List<double>(directions.Count);
            for (int i = 0; i < directions.Count; i++)
            {
                stdList.Add((directions[i] - averageDirection) * (directions[i] - averageDirection) /
                    (directions.Count - 1));
                // (x - x.average)^2 / (k - 1)
            }
            return Math.Sqrt(stdList.Sum());
        }

        private IndexAmplitudes GetPeakIndex() 
        {
            var masterPeak = GetPeakIndex(FtmRole.Master);
            var slavePeak = GetPeakIndex(FtmRole.Slave);
            if (masterPeak.Index != slavePeak.Index)
                MessageLogger.Warning($"Peak index finding mistake : {masterPeak.Index} != {slavePeak.Index}");
            return masterPeak;
        }

        private IndexAmplitudes GetPeakIndex(FtmRole role)
        {
            IndexAmplitudes peakIndex = Indices.First();
            var localMaxDistance = Config.Instance.DirectionFindingSettings.LocalPeaksFindingDistance;
            for (int i = 1; i < Indices.Count; i++)
            {
                if (IsLocalMax(i, role, localMaxDistance))
                    peakIndex = Indices[i];
            }

            return peakIndex;
        }

        private bool IsLocalMax(int possiblePeakIndex, FtmRole role, int distance)
        {
            for (int i = Math.Max(0, possiblePeakIndex - distance); i < Math.Min(Indices.Count, possiblePeakIndex + distance + 1); i++)
            {
                var possiblePeakAmplitude = role == FtmRole.Master 
                    ? Indices[possiblePeakIndex].MasterAmplitudeVolts 
                    : Indices[possiblePeakIndex].SlaveAmplitudeVolts;

                var indexAmplitude = role == FtmRole.Master
                    ? Indices[i].MasterAmplitudeVolts
                    : Indices[i].SlaveAmplitudeVolts;

                if (indexAmplitude > possiblePeakAmplitude)
                    return false;
            }
            return true;
        }
    }
}
