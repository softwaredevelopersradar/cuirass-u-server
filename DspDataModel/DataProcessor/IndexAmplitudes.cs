﻿using System;

namespace DspDataModel.DataProcessor
{
    public struct IndexAmplitudes
    {
        public int Index { get; }
        public double MasterAmplitudeVolts { get; }
        public double SlaveAmplitudeVolts { get; }
        public float FrequencyKhz { get; }

        public IndexAmplitudes(IndexMostEntriesCounter masterIndex, IndexMostEntriesCounter slaveIndex, float startFrequencyKhz, float stepKhz)
        {
            if (masterIndex.Index != slaveIndex.Index)
                throw new ArgumentException($"Indices do not match! Master is {masterIndex.Index}, slave is {slaveIndex.Index}");
            Index = masterIndex.Index;
            MasterAmplitudeVolts = masterIndex.GetMostEntriedAmplitude();
            SlaveAmplitudeVolts = slaveIndex.GetMostEntriedAmplitude();
            FrequencyKhz = startFrequencyKhz + Index * stepKhz;
        }
    }
}
