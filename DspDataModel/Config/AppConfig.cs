﻿namespace DspDataModel
{
    public class AppConfig
    {
        [IpString]
        public string ServerHost { get; set; }

        [Port]
        public int ServerPort { get; set; }
    }
}
