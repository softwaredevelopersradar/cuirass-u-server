﻿using System.ComponentModel.DataAnnotations;
using YamlDotNet.Serialization;

namespace DspDataModel
{
    public class DirectionFindingConfig
    {
        /// <summary>
        /// This property just adds the following number to the output of direction finding algorythm
        /// </summary>
        [YamlIgnore]
        [Range(0, 359)]
        public float DirectionCorrection { get; set; }

        [YamlIgnore]
        [Range(10, 1_000)]
        public int NumberOfRdfScans { get; set; } = 30;

        [YamlIgnore]
        [Range(1, 1_000)]
        public int LowerConverterPauseTimeMs { get; set; } = 10;

        [YamlIgnore]
        [Range(100, 1_000)]
        public int HigherConverterPauseTimeMs { get; set; } = 150;

        [YamlIgnore]
        [Range(1, 200)]
        public int PreConverterPauseTimeMs { get; set; } = 50;

        [YamlIgnore]
        public RdfPairsMode PairsMode { get; set; } = RdfPairsMode.FullPairs;

        [Range(1, 1_000)]
        public int CommutatorPauseTimeMs { get; set; }

        [Range(1, 50)]
        public int LocalPeaksFindingDistance { get; set; }

        [Range(500, 10_000)]
        public int SignalMergeGapMKz { get; set; }

        [Range(0, 20)]
        public int SignalMergeDirectionDeviation { get; set; }

        [YamlIgnore]
        [Range(1, 10)]
        public int IqAggregationDurationSec { get; set; } = 1;

        [YamlIgnore]
        public string IqFilePath{ get; set; }
    }
}
