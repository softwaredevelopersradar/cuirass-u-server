﻿namespace DspDataModel
{
    public class CoefficientsConfig
    {
        public FrequencyCoefficientRange[] Records { get; set; }
    }

    public struct FrequencyCoefficientRange
    {
        public int StartFrequencyMhz { get; set; }
        public int EndFrequencyMhz { get; set; }
        public float Coefficient { get; set; }
    }
}
