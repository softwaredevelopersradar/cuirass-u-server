﻿using System.ComponentModel.DataAnnotations;

namespace DspDataModel
{
    public class HardwareConfig
    {
        public bool IsSimulation { get; set; }
        
        [Range(100, 10000)]
        public int DeviceResponseTimeoutMs { get; set; }

        [Range(0, 10)]
        public int DeviceMessageResendCount { get; set; }

        public FtmDeviceConfig Ftm2M { get; set; }

        public FtmDeviceConfig Ftm2S { get; set; }

        public ComDeviceConfig MasterReceiver { get; set; }
        public ComDeviceConfig SlaveReceiver { get; set; }
        public ComDeviceConfig Relay { get; set; }
    }

    public class FtmDeviceConfig
    {
        [IpString]
        public string Address { get; set; }

        [Port]
        public int Port { get; set; }

        [Port]
        public int DataPort { get; set; }

    }

    public class ComDeviceConfig
    {
        [ComString]
        public string Address { get; set; }

        public int BaudRate { get; set; }
    }
}
