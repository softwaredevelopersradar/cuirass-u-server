﻿namespace DspDataModel
{
    public class SimulatorConfig
    {
        [IpString]
        public string SimulatorAddress { get; set; }

        [Port]
        public int SimulatorPort { get; set; }

        [Port]
        public int SimulatorDataPort { get; set; }
    }
}
