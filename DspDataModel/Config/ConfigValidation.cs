﻿using System.ComponentModel.DataAnnotations;
using System.Net;

namespace DspDataModel
{
    internal class IpStringAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is string ip)
            {
                return IPAddress.TryParse(ip, out _);
            }
            return false;
        }
    }

    internal class ComStringAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is string com)
            {
                if (!com.StartsWith("COM"))
                {
                    return false;
                }
                var comNumberString = com.Substring(3);
                if (int.TryParse(comNumberString, out var comNumber))
                {
                    return comNumber > 0;
                }
            }
            return false;
        }
    }

    internal class IpOrComStringAttribute : ValidationAttribute
    {
        private readonly IpStringAttribute _ipStringAttribute;
        private readonly ComStringAttribute _comStringAttribute;

        public IpOrComStringAttribute()
        {
            _ipStringAttribute = new IpStringAttribute();
            _comStringAttribute = new ComStringAttribute();
        }

        public override bool IsValid(object value)
        {
            return _ipStringAttribute.IsValid(value) || _comStringAttribute.IsValid(value);
        }
    }

    internal class PortAttribute : ValidationAttribute
    {
        private readonly RangeAttribute _rangeAttribute;

        public PortAttribute()
        {
            const int maxPortNumber = 65536;
            _rangeAttribute = new RangeAttribute(1, maxPortNumber);
        }

        public override bool IsValid(object value) => _rangeAttribute.IsValid(value);

        public override string FormatErrorMessage(string name) => _rangeAttribute.FormatErrorMessage(name);
    }
}
