﻿using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Core;
using YamlDotNet.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DspDataModel
{
    public class Config
    {
        private static Config _instance;

        public const string ConfigPath = "config.yaml";

        private static readonly object LockObject;

        public AppConfig AppSettings { get; private set; }
        public HardwareConfig HardwareSettings { get; private set; }
        public SimulatorConfig SimulatorSettings { get; private set; }
        public DirectionFindingConfig DirectionFindingSettings { get; private set; }
        public AntennaSetupConfig AntennaSettings { get; private set; }
        public CoefficientsConfig CoefficientSettings { get; private set; }

        public static Config Instance
        {
            get
            {
                if (_instance == null)
                {
                    try
                    {
                        LoadInner(ConfigPath);
                    }
                    catch (YamlException e)
                    {
                        Exception messageException = e;
                        if (e.InnerException != null)
                        {
                            messageException = e.InnerException;
                        }

                        MessageLogger.Error(messageException);
                        throw;
                    }
                    catch (Exception e)
                    {
                        MessageLogger.Error(e);
                        throw;
                    }
                }
                return _instance;
            }
        }

        static Config()
        {
            LockObject = new object();
        }

        public static void Save(string filename = ConfigPath)
        {
            try
            {
                lock (LockObject)
                {
                    var serializer = new SerializerBuilder()
                        .Build();
                    using (var fs = File.Open(filename, FileMode.Create))
                    using (var sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        serializer.Serialize(sw, _instance);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex, "Can't save config");
            }
        }

        /// <returns> returns true if load is successful </returns>
        public static bool TryLoad(string filename, out Config config)
        {
            try
            {
                LoadInner(filename);
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex, $"Can't load config: {ex.Message}");
                config = null;
                return false;
            }
            config = _instance;
            return true;
        }

        private static void LoadInner(string filename)
        {
            lock (LockObject)
            {
                var deserializer = new DeserializerBuilder().Build();
                _instance = deserializer.Deserialize<Config>(File.ReadAllText(filename));
            }

            var errors = _instance.Validate();
            if (errors.Count > 0)
            {
                var message = errors.Aggregate("Config is invalid!", (s, e) => s + Environment.NewLine + e.ErrorMessage);
                throw new Exception(message);
            }
        }

        public List<ValidationResult> Validate()
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(_instance);

            Validator.TryValidateObject(_instance, context, results, true);
            ValidateObjectProperties(_instance);

            return results;

            void ValidateObjectProperties(object currentObject)
            {
                var objectProperties = currentObject.GetType().GetProperties();
                foreach (var property in objectProperties)
                {
                    var member = property.GetValue(currentObject);
                    if (member == null || member is Array || member is Config || member is string)
                    {
                        continue;
                    }
                    var memberContext = new ValidationContext(member);
                    Validator.TryValidateObject(member, memberContext, results, true);

                    ValidateObjectProperties(member);
                }
            }
        }
    }
}
