﻿namespace DspDataModel
{
    public class AntennaSetupConfig
    {
        public int[] MasterAntennaNumbers;
        public int[] SlaveAntennaNumbers;
    }
}
