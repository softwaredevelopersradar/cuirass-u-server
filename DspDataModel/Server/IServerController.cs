﻿using DspDataModel.Database;
using DspDataModel.Hardware;
using DspDataModel.Tasks;

namespace DspDataModel.Server
{
    public interface IServerController
    {
        IDatabaseController DatabaseController { get; }
        ITaskManager TaskManager { get; }
        IFtmDeviceManager DeviceManager { get; }
    }
}
