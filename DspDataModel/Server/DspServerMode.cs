﻿namespace DspDataModel.Server
{
    public enum DspServerMode
    {
        Stop,
        RadioIntelligence,
        RadioIntelligenceWithNarrow,
        NoiseCalibration,
        GeneratorCalibration
    }
}
