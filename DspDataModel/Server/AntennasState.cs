﻿using System.Linq;
using System.Collections.Generic;
using DspDataModel.Hardware;

namespace DspDataModel.Server
{
    public struct AntennasState
    {
        public AntennaSwitchSource SwitchSource { get; }
        public IReadOnlyList<int> WorkingAntennas { get; }

        public AntennasState(AntennaSwitchSource switchSource, IEnumerable<int> workingAntennas)
        {
            SwitchSource = switchSource;
            WorkingAntennas = workingAntennas.ToList();
        }
    }
}
