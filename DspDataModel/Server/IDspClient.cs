﻿using System;

namespace DspDataModel.Server
{
    public interface IDspClient
    {
        bool IsConnected { get; }

        event EventHandler OnConnect;
        event EventHandler OnDisconnect;
        event EventHandler<DspServerMode> ModeReceived;
        event EventHandler<AntennasState> AntennasStateReceived;

        bool Connect();
        bool Connect(string host, int port);
        void Disconnect();

        bool SetMode(DspServerMode mode);
        DspServerMode GetMode();
        byte[] GetSpectrum(FtmRole role);

        byte[] GetNarrowSpectrum(FtmRole role);
        int[] GetVideoSpectrum(FtmRole role);
        (int[], int[]) GetRfSignalSpectrum(FtmRole role);
    }
}
