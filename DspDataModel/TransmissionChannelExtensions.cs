﻿using System;
using System.Net;
using System.Net.Sockets;
using DspDataModel.Hardware;
using llcss;

namespace DspDataModel
{
    public static class TransmissionChannelExtensions
    {
        public static T SendReceive<T>(this UdpClient client, byte[] message) where T : IBinarySerializable, new()
        {
            try
            {
                var result = new T();
                var response = SendReceive(client, message);
                result.Decode(response, 0);
                return result;
            }
            catch (Exception e)
            {
                MessageLogger.Error($"Decode exception : {e}");
                return new T();
            }
        }

        public static T SendReceive<T>(this UdpClient client, CommandCodes code) where T : IBinarySerializable, new()
        {
            var message = new[] { (byte)code };
            return SendReceive<T>(client, message);
        }

        public static byte[] SendReceive(this UdpClient client, CommandCodes code)
        {
            var message = new[] { (byte)code };
            return SendReceive(client, message);
        }

        /// <summary>
        /// Send byte[] message to an already set up host and receive an answer to the message
        /// </summary>
        public static byte[] SendReceive(this UdpClient client, byte[] message)
        {
            lock (client)
            {
                var commandResendTries = Config.Instance.HardwareSettings.DeviceMessageResendCount;
                while (commandResendTries != 0)
                {
                    try
                    {
                        client.Send(message, message.Length);
                        var response = client.Receive();
                        //didn't receive response at all
                        if (response.Length == 0)
                            throw new SocketException();
                        //received another command response
                        if (message[0] != response[0])
                            throw new SocketException();

                        return response;
                    }
                    catch (SocketException)
                    {
                        commandResendTries--;
                        MessageLogger.Error($"Sendreceive socket exception, input : {BitConverter.ToString(message)}, tries left : {commandResendTries}");
                    }
                }

                throw new Exception($"Device {client.Client.RemoteEndPoint} is not responding");
            }
        }
        
        /// <summary>
        /// Receive udp package from already set up remote end point
        /// </summary>
        public static byte[] Receive(this UdpClient client)
        {//todo : clean up
            try
            {
                var clientRemotePoint = client.Client.RemoteEndPoint.ToString().Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                var ipEndPoint = new IPEndPoint(IPAddress.Parse(clientRemotePoint[0]), Int32.Parse(clientRemotePoint[1]));
                return client.Receive(ref ipEndPoint);
            }
            catch
            {
                return new byte[] { };
            }
        }
    }
}
