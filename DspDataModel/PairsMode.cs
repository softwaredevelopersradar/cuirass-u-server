﻿namespace DspDataModel
{
	public enum RdfPairsMode
	{
		FullPairs = 0,
		FastPairs = 1
	}
}
