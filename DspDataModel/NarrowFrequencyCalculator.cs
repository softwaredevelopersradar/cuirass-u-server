﻿using DspDataModel.Data;

namespace DspDataModel
{
    /// <summary>
    /// This class helps to serialize real frequencies to ftm narrow band range (1100-1600)
    /// </summary>
    public static class NarrowFrequencyCalculator
    {
        /// <summary>
        /// Band number is important for boundary cases
        /// </summary>
        /// <returns>Returns frequency in Khz </returns>
        public static float GetFtmNarrowFrequency(float realFrequencyKhz, Band band) 
        {
            if (band.IsLowerBand)
            {
                return (1_231 + band.FrequencyToMhz) * 1_000 - realFrequencyKhz;
            }
            if (band.Inversed)
            {
                return (1_100 + band.FrequencyToMhz) * 1_000 - realFrequencyKhz;
            }
            return realFrequencyKhz + (1_100 - band.FrequencyFromMhz) * 1_000;
        }

        public static float GetFtmNarrowFrequency(float realFrequencyKhz, int bandNumber) 
        {
            var band = new Band(bandNumber);
            return GetFtmNarrowFrequency(realFrequencyKhz, band);
        }
    }
}
