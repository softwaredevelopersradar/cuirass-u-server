﻿namespace DspDataModel.Hardware
{
    public interface IRelay
    {
        RelayWorkingRange WorkingRange { get; }

        bool Initialize();

        bool SetRangeFrom0To1Ghz();
        bool SetRangeFrom1To18Ghz();
        bool TestRadiopath();//todo : or idk ask what is this one
    }
}
