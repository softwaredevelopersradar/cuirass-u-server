﻿using System.Collections.Generic;

namespace DspDataModel.Hardware
{
    public interface IWideSettings : IBandSettings
    {
        IList<ForbiddenRange> ForbiddenRanges { get; }
        SelectionSetting ImpulseDurationSettings { get; }
        SelectionSetting ImpulseRepeatSettings { get; }

        void GetForbiddenRanges(int index);
        void SetForbiddenRanges(int index, ForbiddenRange range);
        void GetImpulseDurationSettings();
        void SetImpulseDurationSettings(SelectionSetting setting);
        void GetImpulseRepeatSettings();
        void SetImpulseRepeatSettings(SelectionSetting setting);
    }
}
