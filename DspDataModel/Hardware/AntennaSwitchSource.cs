﻿namespace DspDataModel.Hardware
{
    public enum AntennaSwitchSource : byte
    {
        RdfSpectrumTask = 0,
        VideoSpectrumTask = 1,
        NarrowSpectrumTask = 2,
        /// <summary>
        /// This source is used, when antenna switch won't be shown to the user
        /// </summary>
        None = 255
    }
}
