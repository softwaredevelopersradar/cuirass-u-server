﻿using DspDataModel.Data;

namespace DspDataModel.Hardware
{
    public interface IConverter
    {
        string ComPort { get; }
        FtmRole Role { get; }
        int CurrentBandNumber { get; }

        bool SetBand(int bandNumber);

        bool SetBand(Band band);
        System.Threading.Tasks.Task<bool> SetBandAsync(Band band);
    }
}
