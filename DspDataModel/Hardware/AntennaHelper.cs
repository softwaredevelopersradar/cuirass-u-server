﻿using System.Linq;
using System.Collections.Generic;
using DspDataModel.Data;

namespace DspDataModel.Hardware
{
    public static class AntennaHelper
    {
        /// <summary>
        /// Creates antenna index pairs based on the master and slave antenna indices
        /// that are currently enabled
        /// </summary>
        public static IEnumerable<(int MasterIndex, int SlaveIndex)> CreatePairs(IEnumerable<int> masterIndices, IEnumerable<int> slaveIndices)
        {
            var output = new List<(int, int)>();
            var masterLength = masterIndices.Count();
            var slaveLength = slaveIndices.Count();
            for (int i = 0; i < masterLength; i++)
            {
                for (int j = 0; j < slaveLength; j++)
                {
                    var delta = masterIndices.ElementAt(i) - slaveIndices.ElementAt(j);
                    if (delta == 1 || delta == 0) //is pair
                    {
                        var pair = (masterIndices.ElementAt(i), slaveIndices.ElementAt(j));
                        output.Add(pair);
                    }
                }
            }

            //boundary case
            var boundaryDelta = masterIndices.First() - slaveIndices.Last();
            if (boundaryDelta == -3)
            {
                var pair = (masterIndices.First(), slaveIndices.Last());
                output.Add(pair);
            }
            return output;
        }

        public static IEnumerable<(int MasterIndex, int SlaveIndex)> CreateFastPairs(IEnumerable<int> masterIndices, IEnumerable<int> slaveIndices)
        {
            var allPairs = CreatePairs(masterIndices, slaveIndices);
            var fastPairs = allPairs.Where(v => v.MasterIndex == v.SlaveIndex);
            return fastPairs;
        }

        public static int MasterIndexToNumber(int masterAntennaIndex) 
        {
            return (masterAntennaIndex + 1) * 2 - 1;
        }

        public static int MasterNumberToIndex(int masterAntennaNumber) 
        {
            return (masterAntennaNumber - 1) / 2;
        }

        public static int SlaveIndexToNumber(int slaveAntennaIndex)
        {
            return (slaveAntennaIndex + 1) * 2;
        }

        public static int SlaveNumberToIndex(int slaveAntennaNumber)
        {
            return slaveAntennaNumber / 2 - 1;
        }

        public static (int MasterNumber, int SlaveNumber) ConvertIndexPairToNumberPair(int masterIndex, int slaveIndex)
        {
            return (MasterIndexToNumber(masterIndex), SlaveIndexToNumber(slaveIndex));
        }

        public static (int MasterIndex, int SlaveIndex) ConvertNumberPairToIndexPair(int masterNumber, int slaveNumber)
        {
            return (MasterNumberToIndex(masterNumber), SlaveNumberToIndex(slaveNumber));
        }

        public static (IEnumerable<int> MasterIndices, IEnumerable<int> SlaveIndices) ConvertNumbersToIndices(IEnumerable<int> numbers) 
        {
            var masterIndices = new List<int>();
            var slaveIndices = new List<int>();
            foreach (var antennaNumber in numbers)
            {
                if (antennaNumber % 2 != 0)
                {
                    masterIndices.Add(MasterNumberToIndex(antennaNumber));
                }
                else
                {
                    slaveIndices.Add(SlaveNumberToIndex(antennaNumber));
                }
            }
            return (masterIndices, slaveIndices);
        }
        
        /// <summary>
        /// Adds necessary values to direction based on the antenna pair and direction correction value
        /// </summary>
        public static float ClarifyDirection(float direction, int masterAntennaIndex, int slaveAntennaIndex) 
        {
            if (float.IsNaN(direction))
                return float.NaN;
            var delta = masterAntennaIndex - slaveAntennaIndex;
            /*
             * Delta = 0 means that in pair, master antenna is to the left from the slave one
             * So for correct value calculation we multiply direction by -1
             */
            if (delta == 0)
            {
                //we change the sign for correct value
                direction = -direction;                
            }
            //boundary case
            if (delta == -3) //todo : to constants
            {
                return direction + 315 + 22.5f;
            }

            var addedValue = 22.5f + 45 * (masterAntennaIndex + slaveAntennaIndex);
            var realDirection = direction + addedValue;

            if (realDirection < 0 || realDirection > 360)
                throw new System.Exception("something wrong with direction calculations or you");

            var directionWithCorrection = realDirection + Config.Instance.DirectionFindingSettings.DirectionCorrection;

            return ValidateDirection(directionWithCorrection);
        }

        public static float ClarifyDirection(ISignal signal) 
        {
            return ClarifyDirection(signal.Direction, signal.MasterAntennaIndex, signal.SlaveAntennaIndex);
        }

        //todo : remove
        private static float ValidateDirection(float direction)
        {
            if (float.IsNaN(direction))
                return direction;
            if (direction >= 360)
                direction -= 360;
            if (direction < 0)
                direction += 360;
            return direction;
        }
    }
}
