﻿namespace DspDataModel.Hardware
{
    public struct NarrowVideoClarificationSetting
    {
        public int FrequencyKhz { get; }
        public int FtmFrequencyKhz { get; }
        public int BandNumber { get; }

        public bool IsDefault => FtmFrequencyKhz == 0
                && BandNumber == 0;

        public NarrowVideoClarificationSetting(int frequencyKhz, int ftmFrequencyKhz, int bandNumber)
        {
            FrequencyKhz = frequencyKhz;
            FtmFrequencyKhz = ftmFrequencyKhz;
            BandNumber = bandNumber;
        }
    }
}
