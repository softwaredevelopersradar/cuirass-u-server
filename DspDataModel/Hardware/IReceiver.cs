﻿namespace DspDataModel.Hardware
{
    public interface IReceiver
    {
        IAntennaCommutator AntennaCommutator { get; }
        IConverter Converter { get; }
        bool Initialize();
    }
}
