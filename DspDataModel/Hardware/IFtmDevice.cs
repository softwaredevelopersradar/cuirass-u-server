﻿using System.Net.Sockets;
using System.Collections.Generic;

namespace DspDataModel.Hardware
{
    public interface IFtmDevice
    {
        UdpClient Client { get; }
        UdpClient DataClient { get; }
        bool IsConnected { get; }
        FtmDeviceOutputMode OutputMode { get; }
        IFtmDeviceSettings Settings { get; }
        INarrowSettings NarrowSettings { get; }
        IWideSettings WideSettings { get; }

        bool Connect(string deviceIpAddress, int devicePort);
        void Disconnect();

        bool SearchDevice();
        byte[] WideReadFft();
        byte[] NarrowReadFft();
        void SetOutputMode(FtmDeviceOutputMode mode);
        bool Initialize();
        void StartNoiseCalibration();
        void SetIpAddress();
        void GetGpsState();

        int[] VideoSpectrumRead(FtmRole isMaster = FtmRole.Master);
        (int[], int[]) RfSignalSpectrumRead(FtmRole isMaster = FtmRole.Master);

        IEnumerable<CalculatorSignal> GetCalculatorSignals(FtmDeviceOutputMode outputMode);
    }
}
