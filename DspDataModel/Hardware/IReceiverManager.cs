﻿using DspDataModel.Data;
using System.Threading.Tasks;

namespace DspDataModel.Hardware
{
    public interface IReceiverManager
    {
        IReceiver MasterReceiver { get; }
        IReceiver SlaveReceiver { get; }
        IRelay Relay { get; }

        bool Initialize();

        bool SetConverterBand(Band band);
        Task<bool> SetConverterBandAsync(Band band);
        void SetCommutatorAntennas(int masterAntennaIndex, int slaveAntennaIndex, AntennaSwitchSource antennaSwitchSource);
    }
}
