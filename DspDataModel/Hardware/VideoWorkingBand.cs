﻿namespace DspDataModel.Hardware
{
    public enum VideoWorkingBand : byte
    {
        Wide = 0,
        Narrow = 1,
        Off = 255
    }
}
