﻿using System.Net.Sockets;

namespace DspDataModel.Hardware
{
    public interface IFtmDeviceSettings
    {
        UdpClient Client { get; }
        SecondsMark SecondsMark { get; }
        WindowFunctionType WindowType { get; }
        StrobeMode StrobeMode { get; }
        byte PermissionByte { get; }
        DacVideoMode DacVideoMode { get; }
        DacVideoMode DacAudioMode { get; }
        short Volume { get; }

        bool Initialize(byte[] data);
        void GetSecondsMark();
        bool SetSecondsMark(SecondsMark mark);
        void GetWindowFunctionType();
        bool SetWindowFunctionType(WindowFunctionType type);
        void GetStrobeMode();
        bool SetStrobeMode(StrobeMode mode);
        void GetPermissionByte();
        void SetPermissionByte();
        void GetDacVideoMode();
        void SetDacVideoMode();
        void GetDacAudioMode();
        void SetDacAudioMode();
        void GetVolume();
        void SetVolume();
    }
}
