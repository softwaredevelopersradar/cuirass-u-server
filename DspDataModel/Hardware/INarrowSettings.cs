﻿namespace DspDataModel.Hardware
{
    public interface INarrowSettings : IBandSettings
    {
        int FrequencyKhz { get; }
        NarrowFilterWidth BandFilterWidth { get; }

        void GetFrequencyKhz();
        void SetFrequencyKhz(int frequencyKhz);
        void GetFilterWidth();
        void SetFilterWidth(NarrowFilterWidth filterWidth);
    }
}
