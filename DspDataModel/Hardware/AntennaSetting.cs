﻿using System.Linq;
using System.Collections.Generic;

namespace DspDataModel.Hardware
{
    public struct AntennaSetting
    {
        private readonly IEnumerable<(int MasterIndex, int SlaveIndex)> _antennaPairs;
        private readonly IEnumerable<(int MasterIndex, int SlaveIndex)> _fastAntennaPairs;
        public IEnumerable<(int MasterIndex, int SlaveIndex)> Pairs => _antennaPairs.ToList();
        public IEnumerable<(int MasterIndex, int SlaveIndex)> FastRdfPairs => _fastAntennaPairs.ToList();

        public AntennaSetting(IEnumerable<int> masterAntennaIndices, IEnumerable<int> slaveAntennaIndices) 
        {
            _antennaPairs = AntennaHelper.CreatePairs(masterAntennaIndices, slaveAntennaIndices);
            _fastAntennaPairs = AntennaHelper.CreateFastPairs(masterAntennaIndices, slaveAntennaIndices);
        }
    }
}
