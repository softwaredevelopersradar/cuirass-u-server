﻿using System.Net.Sockets;

namespace DspDataModel.Hardware
{
    public interface IBandSettings
    {
        UdpClient Client { get; }
        byte Threshold { get; }
        short FftDelay { get; }
        /// <summary>
        /// Comparator detector upper threshold
        /// </summary>
        short AttackThreshold { get; }
        /// <summary>
        /// Comparator detector peak threshold
        /// </summary>
        short PicThreshold { get; }

        bool Initialize(byte[] data);
        void GetThreshold();
        void SetThreshold(byte threshold);
        void GetFftDelay();
        bool SetFftDelay(short fftDelay);
        void GetAttackThreshold();
        void SetAttackThreshold(short upperThreshold);
        void GetPicThreshold();
        void SetPicThreshold(short peakThreshold);
    }
}
