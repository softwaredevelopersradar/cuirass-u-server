﻿using System;
using System.Collections;
using Settings;

namespace DspDataModel.Hardware
{
    public enum FtmDeviceOutputMode
    {
        NoOutput = 0,
        WideCalculator = 1,
        NarrowCalculator = 2,
        WideAndNarrow = 3,
        IqComponents = 4,
        WideAndIq = 5,
        NarrowAndIq = 6,
        WideAndNarrowAndIq = 7
    }

    public enum SecondsMark
    {
        InternalMark = 0,
        ExternalMark = 1
    }

    public enum StrobeMode : byte
    {
        Manual = 0,
        Auto = 1
    }

    public enum NarrowFilterWidth
    {
        OneMHz = 0,
        TwoMHz = 1,
        FiveMHz = 2,
        TenMHz = 3,
        TwentyMHz = 4,
        FiftyMHz = 5
    }

    public enum WindowFunctionType
    {
        ChebyshevWindow = 0,
        RectangleWindow = 1
    }

    public enum DacVideoMode
    {
        Disabled = 0,
        WideDetector = 1,
        NarrowDetector = 2
    }

    public enum Gps2DIndicationModes
    {
        Pass = 0,
        _2D = 1,
        OffAnt = 2
    }
    
    public struct ForbiddenRange
    {
        public readonly int StartMhz;
        public readonly int EndMhz;
        public readonly bool IsUsed;
        
        public ForbiddenRange(int startMhz, int endMhz) : this()
        {
            //checks
            if (startMhz > endMhz)
            {
                var bucket = startMhz;
                startMhz = endMhz;
                endMhz = bucket;
            }
            startMhz = Math.Max(startMhz, Constants.MinFrequencyMhz);
            endMhz = Math.Min(endMhz, Constants.MaxFrequencyMhz);

            //TODO : test
            StartMhz = startMhz;
            var bits = new BitArray(new int[] { endMhz });
            IsUsed = bits[bits.Length - 1];
            bits[bits.Length - 1] = false;
            var asd = new int[1];
            bits.CopyTo(asd, 0);

            EndMhz = endMhz;
        }

        public ForbiddenRange(int startMhz, int endMhz, bool isUsed) : this()
        {
            //checks
            if (startMhz > endMhz)
            {
                var bucket = startMhz;
                startMhz = endMhz;
                endMhz = bucket;
            }
            startMhz = Math.Max(startMhz, Constants.MinFrequencyMhz);
            endMhz = Math.Min(endMhz, Constants.MaxFrequencyMhz);

            IsUsed = isUsed;

            //TODO : test
            StartMhz = startMhz;
            var bits = new BitArray(new int[] { endMhz });
            IsUsed = bits[bits.Length - 1];
            bits[bits.Length - 1] = false;
            var asd = new int[1];
            bits.CopyTo(asd, 0);

            EndMhz = endMhz;
        }

        public int GetEndMhz()
        {
            var bits = new BitArray(new int[] { EndMhz });
            bits[bits.Length - 1] = IsUsed;
            var asd = new int[1];
            bits.CopyTo(asd, 0);
            return asd[0];
        }
    }

    public struct SelectionSetting
    {
        public readonly int MinImpulses;
        public readonly int MaxImpluses;
        public readonly bool IsActive;

        public SelectionSetting(int minImpulses, int maxImpulses, bool isActive)
        {
            MinImpulses = minImpulses;
            MaxImpluses = maxImpulses;
            IsActive = isActive;
        }
    }
}
