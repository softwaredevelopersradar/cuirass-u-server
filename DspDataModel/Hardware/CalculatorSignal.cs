﻿namespace DspDataModel.Hardware
{
    public struct CalculatorSignal
    {
        public int FrequencyMhz { get; private set; }
        public int Amplitude { get; private set; }
        public byte BandwidthMhz { get; private set; }
        public int DurationNs { get; private set; }
        public int RepeatPeriodNs { get; private set; }
        public int Hours { get; private set; }
        public int Minutes { get; private set; }
        public int Seconds { get; private set; }
        public int Ticks { get; private set; }

        public CalculatorSignal(int frequencyMhz, int amplitude, byte bandwidthMhz, int durationNs, 
            int repeatPeriodNs, int hours, int minutes, int seconds, int ticks)
        {
            FrequencyMhz = frequencyMhz;
            Amplitude = amplitude;
            BandwidthMhz = bandwidthMhz;
            DurationNs = durationNs;
            RepeatPeriodNs = repeatPeriodNs;
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
            Ticks = ticks;
        }
    }
}
