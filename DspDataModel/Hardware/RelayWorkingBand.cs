﻿namespace DspDataModel.Hardware
{
    public enum RelayWorkingRange : byte
    {
        From100MhzTo1Ghz,
        From1To18Ghz,
        Unknown = 255
    }
}
