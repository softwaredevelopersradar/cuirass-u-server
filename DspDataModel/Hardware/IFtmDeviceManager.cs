﻿namespace DspDataModel.Hardware
{
    public interface IFtmDeviceManager
    {
        IFtmDevice Ftm2M { get; }
        IFtmDevice Ftm2S { get; }
        double Threshold { get; set; }
        int NarrowFrequencyKhz { get; }
        NarrowFilterWidth NarrowBandWidthKhz { get; }
        VideoWorkingBand WorkingBand { get; }

        double SlaveAntennaCoefficient { get; set; }
        double MasterAntennaCoefficient { get; set; }

        bool Connect();
        void Disconnect();
        bool Initialize();

        bool PingDevices();
        void SetOutputMode(FtmDeviceOutputMode mode);
        bool StartNoiseCalibration();
        void SetForbiddenRanges(int index, ForbiddenRange range);
        void SetFilterWidth(NarrowFilterWidth filterWidth);
        void NarrowSetFrequency(int frequencyKhz);

        //todo : add async implementation
        void WideGetFft(out byte[] masterWbFft, out byte[] slaveWbFft);
        void NarrowGetFft(out byte[] masterNbFft, out byte[] slaveNbFft);

        void SetVideoWorkingBand(VideoWorkingBand workingBand);

        IBandSettings GetBandSettings(FtmRole role, FtmMode mode);

        bool SetStrobeMode(StrobeMode mode);
        bool SetWindowType(WindowFunctionType type);
        bool SetSecondsMark(SecondsMark mark);
    }
}
