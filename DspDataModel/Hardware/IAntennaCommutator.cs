﻿using System;
using System.Threading.Tasks;

namespace DspDataModel.Hardware
{
    public interface IAntennaCommutator
    {
        /// <summary>
        /// Currently set for work antenna from enabled range
        /// </summary>
        int WorkingAntennaIndex { get; }
        int DefaultAttenuator { get; set; }

        event EventHandler<string> OnError;
        event EventHandler<AntennaSwitchSource> WorkingAntennasChanged;

        void SetWorkingAntenna(int antennaId, AntennaSwitchSource antennaSwitchSource);
        Task SetWorkingAntennaAsync(int antennaId, AntennaSwitchSource antennaSwitchSource);
    }
}
