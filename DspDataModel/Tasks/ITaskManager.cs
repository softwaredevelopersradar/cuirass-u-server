﻿using System.Collections.Generic;
using DspDataModel.Data;
using DspDataModel.Database;
using DspDataModel.Hardware;
using DspDataModel.Storages;

namespace DspDataModel.Tasks
{
    public interface ITaskManager
    {
        IModeController CurrentMode { get; }
        IFtmDeviceManager DeviceManager { get; }
        IReceiverManager ReceiverManager { get; }
        ISignalStorage SignalStorage { get; }
        IFilterManager FilterManager { get; }
        IDatabaseController DatabaseController { get; }
        IMultiScanStorage<FtmScan> WideBandSpectrumStorage { get; }
        ISecondarySpectrumStorage SecondarySpectrumStorage { get; }
        IReadOnlyList<IFtmTask> Tasks { get; }
        IHistoryStorage HistoryStorage { get; }

        bool Initialize();
        void SetMode(IModeController modeController);
        void AddTask(IFtmTask task);
        void AddTasks(IEnumerable<IFtmTask> tasks);
        void RemoveTask(IFtmTask task);
        void ClearTasks();
    }
}
