﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace DspDataModel.Tasks
{
    public interface IModeController
    {
        ITaskManager TaskManager { get; }

        Task Initialize();
        IEnumerable<IFtmTask> CreateTasks();

        void CancelTasks();
    }
}
