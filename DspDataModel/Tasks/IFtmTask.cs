﻿using DspDataModel.Data;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DspDataModel.Tasks
{
    public interface IFtmTask
    {
        bool IsEndless { get; }
        int Priority { get; }
        bool IsReady { get; }
        object Result { get; }
        IReadOnlyList<Band> Objectives { get; }
        bool AutoUpdateObjectives { get; }

        event EventHandler TaskEndEvent;
        Task ProcessTask();
        void Cancel();
        void UpdateObjectives(IEnumerable<Band> newObjectives);
    }
}
