﻿namespace DspDataModel
{
    public enum FtmRole : byte
    {
        Master = 0,
        Slave = 1
    }
}
