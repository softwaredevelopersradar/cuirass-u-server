﻿using DspDataModel;
using DspDataModel.Server;
using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DspServerCuirassApp
{
    class Program
    {
        private static IServerController _server;
        private static Process _simulatorProcess;

        static void Main(string[] args)
        {
            if (Instance.HasRunningCopy)
            {
                MessageLogger.Log("Another copy of the application is already running", ConsoleColor.Red);
                Console.ReadLine();
                return;
            }
            var logger = new NLogLogger();
            MessageLogger.SetLogger(logger);
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
            StartSimulatorIfNecessary();
            SetConsoleCloseHandler();

            _server = new ServerControllerLibrary.ServerController();
            Application.Run();
        }

        private static void SetConsoleCloseHandler()
        {
            _handler = ConsoleEventCallback;
            SetConsoleCtrlHandler(_handler, true);
        }

        static bool ConsoleEventCallback(int eventType)
        {
            // Console window closing
            if (eventType == 2)
            {
                _server.TaskManager.ClearTasks();
                _server.TaskManager.DatabaseController.Disconnect();
                Thread.Sleep(1000);
                StopSimulatorIfNecessary();
                Config.Save();
            }
            return false;
        }

        private static void StartSimulatorIfNecessary()
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            var simTask = Task.Run(() =>
            {
                if (Config.Instance.HardwareSettings.IsSimulation)
                {
                    if (Process.GetProcessesByName("Simulator").Any())
                    {
                        return;
                    }
                    _simulatorProcess = new Process
                    {
                        StartInfo = { FileName = "Simulator.exe" }
                    };
                    _simulatorProcess.Start();
                }
            });
            simTask.Wait();
            Thread.Sleep(2000);//time for simulator to start
        }

        private static void StopSimulatorIfNecessary()
        {
            if (Config.Instance.HardwareSettings.IsSimulation == false)
            {
                return;
            }

            if (Process.GetProcessesByName("Simulator").Any() == false)
            {
                return;
            }
            _simulatorProcess.CloseMainWindow();
            _simulatorProcess.Close();
        }

        private static ConsoleEventDelegate _handler;

        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);
    }
}
