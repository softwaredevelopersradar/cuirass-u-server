﻿using System;
using System.Linq;
using DspDataModel.DataProcessor;
using DspDataModel.Hardware;
using NUnit.Framework;

namespace Tests
{
    public class DirectionCalculationTests
    {
        //todo: add direction finding and calculation tests

        [Test]
        public void DirectionClarificationTest()
        {
            var masterIndices = new[] { 0, 1, 2, 3 };
            var slaveIndices = new[] { 0, 1, 2, 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);
            var directions = Enumerable.Range(-22, 45).ToArray();
            var realDirections = Enumerable.Range(-22, 45).Select(v => v + 22.5f).ToArray();

            foreach (var (MasterIndex, SlaveIndex) in pairs)
            {
                realDirections = realDirections.Reverse().ToArray();
                for (int i = 0; i < directions.Length; i++)
                {
                    var realDirection = AntennaHelper.ClarifyDirection(directions[i], MasterIndex, SlaveIndex);
                    Assert.IsTrue(realDirection == realDirections[i]);
                }
                realDirections = realDirections.Select(v => v + 45).ToArray();
            }
        }

        [Test]
        public void DirectionCalculationTest() 
        {
            var numberOfTests = 100;
            var god = new Random(4242);
            for (int i = 0; i < numberOfTests; i++)
            {
                var m = god.Next(60,100);
                var s = god.Next(60, 100);
                var direction = CalculateDirection(m, s);
                var inversedDirection = -1 * CalculateDirection(s, m);
                Assert.AreEqual(direction, inversedDirection);
            }
        }

        private double CalculateDirection(float masterAmplitude, float slaveAmplitude )
        {
            return (masterAmplitude - slaveAmplitude) / (masterAmplitude + slaveAmplitude);
        }
    }
}
