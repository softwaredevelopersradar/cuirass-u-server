﻿using DspDataModel.Data;
using NUnit.Framework;
using Settings;

namespace Tests
{
    /// <summary>
    /// Tests creation of band by number (since it's quite a complicated task)
    /// In every tests first, last and random band number from range are tested
    /// </summary>
    public class BandCreationTests
    {
        [Test]
        public void CreationBandsFromRange100_133Test() 
        {
            var band = new Band(0);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 88);
            Assert.AreEqual(band.FrequencyFromMhz, 100);
            Assert.AreEqual(band.FrequencyToMhz, 103);

            band = new Band(1);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 118);
            Assert.AreEqual(band.FrequencyFromMhz, 103);
            Assert.AreEqual(band.FrequencyToMhz, 133);
        }

        public void CreationBandsFromRange133_248Test()
        {
            var band = new Band(2);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 143);
            Assert.AreEqual(band.FrequencyFromMhz, 133);
            Assert.AreEqual(band.FrequencyToMhz, 158);

            band = new Band(3);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 173);
            Assert.AreEqual(band.FrequencyFromMhz, 158);
            Assert.AreEqual(band.FrequencyToMhz, 188);

            band = new Band(5);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 233);
            Assert.AreEqual(band.FrequencyFromMhz, 218);
            Assert.AreEqual(band.FrequencyToMhz, 248);
        }

        [Test]
        public void CreationBandsFromRange248_293Test()
        {
            var band = new Band(6);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 249);
            Assert.AreEqual(band.FrequencyFromMhz, 248);
            Assert.AreEqual(band.FrequencyToMhz, 263);

            band = new Band(7);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 279);
            Assert.AreEqual(band.FrequencyFromMhz, 264);
            Assert.AreEqual(band.FrequencyToMhz, 294);
        }

        [Test]
        public void CreationBandsFromRange293_496Test()
        {
            var band = new Band(8);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 301);
            Assert.AreEqual(band.FrequencyFromMhz, 293);
            Assert.AreEqual(band.FrequencyToMhz, 316);

            band = new Band(11);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 391);
            Assert.AreEqual(band.FrequencyFromMhz, 376);
            Assert.AreEqual(band.FrequencyToMhz, 406);

            band = new Band(14);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 481);
            Assert.AreEqual(band.FrequencyFromMhz, 466);
            Assert.AreEqual(band.FrequencyToMhz, 496);
        }

        [Test]
        public void CreationBandsFromRange449_880Test()
        {
            var band = new Band(15);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 505);
            Assert.AreEqual(band.FrequencyFromMhz, 496);
            Assert.AreEqual(band.FrequencyToMhz, 520);

            band = new Band(20);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 655);
            Assert.AreEqual(band.FrequencyFromMhz, 640);
            Assert.AreEqual(band.FrequencyToMhz, 670);

            band = new Band(27);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 865);
            Assert.AreEqual(band.FrequencyFromMhz, 850);
            Assert.AreEqual(band.FrequencyToMhz, 880);
        }

        [Test]
        public void CreationBandsFromRange900_1000Test()
        {
            var band = new Band(28);
            Assert.AreEqual(band.Inversed, false);
            Assert.AreEqual(band.ConverterFrequencyMhz, 1000);
            Assert.AreEqual(band.FrequencyFromMhz, 900);
            Assert.AreEqual(band.FrequencyToMhz, 1000);
        }

        [Test]
        public void CreationBandsFromRange1000_18000Test()
        {
            var band = new Band(29);
            Assert.AreEqual(band.Inversed, false);
            Assert.AreEqual(band.ConverterFrequencyMhz, 1250);
            Assert.AreEqual(band.FrequencyFromMhz, 1000);
            Assert.AreEqual(band.FrequencyToMhz, 1500);

            band = new Band(32);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 2750);
            Assert.AreEqual(band.FrequencyFromMhz, 2500);
            Assert.AreEqual(band.FrequencyToMhz, 3000);

            band = new Band(34);
            Assert.AreEqual(band.Inversed, false);
            Assert.AreEqual(band.ConverterFrequencyMhz, 3750);
            Assert.AreEqual(band.FrequencyFromMhz, 3500);
            Assert.AreEqual(band.FrequencyToMhz, 4000);

            band = new Band(37);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 5250);
            Assert.AreEqual(band.FrequencyFromMhz, 5000);
            Assert.AreEqual(band.FrequencyToMhz, 5500);

            band = new Band(41);
            Assert.AreEqual(band.Inversed, false);
            Assert.AreEqual(band.ConverterFrequencyMhz, 7250);
            Assert.AreEqual(band.FrequencyFromMhz, 7000);
            Assert.AreEqual(band.FrequencyToMhz, 7500);

            band = new Band(45);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 9250);
            Assert.AreEqual(band.FrequencyFromMhz, 9000);
            Assert.AreEqual(band.FrequencyToMhz, 9500);

            band = new Band(50);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 11750);
            Assert.AreEqual(band.FrequencyFromMhz, 11500);
            Assert.AreEqual(band.FrequencyToMhz, 12000);

            band = new Band(57);
            Assert.AreEqual(band.Inversed, true);
            Assert.AreEqual(band.ConverterFrequencyMhz, 15250);
            Assert.AreEqual(band.FrequencyFromMhz, 15000);
            Assert.AreEqual(band.FrequencyToMhz, 15500);

            band = new Band(62);
            Assert.AreEqual(band.Inversed, false);
            Assert.AreEqual(band.ConverterFrequencyMhz, 17750);
            Assert.AreEqual(band.FrequencyFromMhz, 17500);
            Assert.AreEqual(band.FrequencyToMhz, 18000);
        }

        [Test]
        public void BandSamplesCountTest() 
        {
            var numberOfSamples = 2;
            for (int i = 0; i < 63; i++)
            {
                var band = new Band(i);
                numberOfSamples += band.ScanSampleCount;
            }
            Assert.AreEqual(numberOfSamples, Constants.NumberOfSamplesFrom100MhzTo18Ghz);
        }
    }
}
