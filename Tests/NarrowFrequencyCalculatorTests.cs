﻿using DspDataModel;
using DspDataModel.Data;
using Settings;
using NUnit.Framework;

namespace Tests
{
    public class NarrowFrequencyCalculatorTests
    {
        [Test]
        public void TestFirstBandsFirstFrequenciesFrom100MhzTo900Mhz() 
        {
            var frequencyKhz = 100_000;
            var converterdFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(frequencyKhz, 0);
            Assert.IsTrue(converterdFrequencyKhz == 1234_000);

            frequencyKhz = 133_000;
            converterdFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(frequencyKhz, 2);
            Assert.IsTrue(converterdFrequencyKhz == 1256_000);

            frequencyKhz = 248_000;
            converterdFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(frequencyKhz, 6);
            Assert.IsTrue(converterdFrequencyKhz == 1246_000);

            frequencyKhz = 293_000;
            converterdFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(frequencyKhz, 8);
            Assert.IsTrue(converterdFrequencyKhz == 1254_000);

            frequencyKhz = 496_000;
            converterdFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(frequencyKhz, 15);
            Assert.IsTrue(converterdFrequencyKhz == 1255_000);
        }

        [Test]
        public void TestFrequencyConversionsFrom100MhzTo900Mhz() 
        {
            for (int i = 0; i <= Constants.BandThresholdNumber; i++)
            {
                var band = new Band(i);
                var bandStartFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(band.FrequencyFromMhz * 1000, band);
                //checking all mhz frequencies in a band
                for (int j = band.FrequencyFromMhz; j <= band.FrequencyToMhz; j++)
                {
                    var bandFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(j * 1_000, band);
                    Assert.AreEqual(bandStartFrequencyKhz,bandFrequencyKhz);
                    Assert.IsTrue(bandStartFrequencyKhz == bandFrequencyKhz);
                    bandStartFrequencyKhz -= 1_000;
                }
            }
        }

        [Test]
        public void TestFrequencyConversionsFrom900MhzTo18Ghz() 
        {
            for (int i = Constants.BandThresholdNumber + 1; i < Constants.NumberOfBands; i++)
            {
                var band = new Band(i);
                var bandStartFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(band.FrequencyFromMhz * 1000, band);
                
                //checking all mhz frequencies in a band
                for (int j = band.FrequencyFromMhz; j <= band.FrequencyToMhz; j++)
                {
                    var bandFrequencyKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(j * 1_000, band);
                    Assert.IsTrue(bandStartFrequencyKhz == bandFrequencyKhz);

                    if (!band.Inversed)
                    {
                        bandStartFrequencyKhz += 1_000;
                    }
                    else 
                    {
                        bandStartFrequencyKhz -= 1_000;
                    }
                }
            }
        }
    }
}
