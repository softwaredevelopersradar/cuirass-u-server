using DspDataModel.Data;
using NUnit.Framework;
using Settings;

namespace Tests
{
    public class UtilityTests
    {
        [Test]
        public void TestGetSamplesCount()
        {
            var frequency = Constants.FirstBandMinKhz;

            Assert.AreEqual(Utilities.GetSamplesCount(frequency, frequency + Constants.SamplesGapKhz), 1);
            Assert.AreEqual(Utilities.GetSamplesCount(frequency, frequency + 100 * Constants.SamplesGapKhz), 100);
            Assert.AreEqual(Utilities.GetSamplesCount(
                    Utilities.GetBandMinFrequencyKhz(0),
                    Utilities.GetBandMaxFrequencyKhz(0)), Constants.BandSampleCount - 1);
        }

        [Test]
        public void TestAreRangesIntersected()
        {
            var r1 = new FrequencyRange(1000, 1100);
            var r2 = new FrequencyRange(1200, 1300);
            Assert.IsFalse(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsFalse(FrequencyRange.AreIntersected(r2, r1));

            r2 = new FrequencyRange(1050, 1060);
            Assert.IsTrue(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsTrue(FrequencyRange.AreIntersected(r2, r1));

            r2 = new FrequencyRange(1100, 1200);
            Assert.IsTrue(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsTrue(FrequencyRange.AreIntersected(r2, r1));

            r2 = new FrequencyRange(1050, 1250);
            Assert.IsTrue(FrequencyRange.AreIntersected(r1, r2));
            Assert.IsTrue(FrequencyRange.AreIntersected(r2, r1));
        }

        [Test]
        public void TestRangesIntersection()
        {
            var r1 = new FrequencyRange(1000, 1100);
            var r2 = new FrequencyRange(1200, 1300);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), null);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), null);

            r2 = new FrequencyRange(1050, 1060);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), r2);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), r2);

            r2 = new FrequencyRange(1100, 1200);
            var result = new FrequencyRange(1100, 1100);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), result);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), result);

            r2 = new FrequencyRange(1050, 1150);
            result = new FrequencyRange(1050, 1100);
            Assert.AreEqual(FrequencyRange.Intersection(r1, r2), result);
            Assert.AreEqual(FrequencyRange.Intersection(r2, r1), result);
        }

        [Test]
        public void TestRangesIntersectionFactor()
        {
            var r1 = new FrequencyRange(1000, 1100);
            var r2 = new FrequencyRange(1200, 1300);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 0, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 0, 1e-3);

            r2 = new FrequencyRange(1050, 1060);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 1, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 1, 1e-3);

            r2 = new FrequencyRange(1100, 1200);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 0, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 0, 1e-3);

            r2 = new FrequencyRange(1050, 1150);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r1, r2), 0.5, 1e-3);
            Assert.AreEqual(FrequencyRange.IntersectionFactor(r2, r1), 0.5, 1e-3);
        }

        [Test]
        public void TestGetBandNumber() 
        {
            //first band range
            Assert.AreEqual(0, Utilities.GetBandNumber(100));
            Assert.AreEqual(0, Utilities.GetBandNumber(103));
            Assert.AreEqual(1, Utilities.GetBandNumber(104));
            Assert.AreEqual(1, Utilities.GetBandNumber(133));

            //second band range
            Assert.AreEqual(2, Utilities.GetBandNumber(134));
            Assert.AreEqual(3, Utilities.GetBandNumber(173));
            Assert.AreEqual(4, Utilities.GetBandNumber(203));
            Assert.AreEqual(5, Utilities.GetBandNumber(233));

            //third band range
            Assert.AreEqual(6, Utilities.GetBandNumber(249));
            Assert.AreEqual(7, Utilities.GetBandNumber(278));

            //fourth band range
            Assert.AreEqual(8, Utilities.GetBandNumber(301));
            Assert.AreEqual(9, Utilities.GetBandNumber(331));
            Assert.AreEqual(14, Utilities.GetBandNumber(481));

            //fourth band range
            Assert.AreEqual(15, Utilities.GetBandNumber(505));
            Assert.AreEqual(19, Utilities.GetBandNumber(625));
            Assert.AreEqual(23, Utilities.GetBandNumber(745));
            Assert.AreEqual(27, Utilities.GetBandNumber(865));

            //fifth and six band range
            Assert.AreEqual(28, Utilities.GetBandNumber(970));

            Assert.AreEqual(29, Utilities.GetBandNumber(1125));
            Assert.AreEqual(40, Utilities.GetBandNumber(6999));
            Assert.AreEqual(50, Utilities.GetBandNumber(11777));
            Assert.AreEqual(62, Utilities.GetBandNumber(17700));
        }
    }
}