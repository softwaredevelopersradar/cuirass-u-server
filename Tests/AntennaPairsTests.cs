﻿using NUnit.Framework;
using DspDataModel.Hardware;
using System.Linq;

namespace Tests
{
    public class AntennaPairsTests
    {
        [Test]
        public void CreationOfAllPairs() 
        {
            var masterIndices = new[] { 0, 1, 2, 3 };
            var slaveIndices = new[] { 0, 1, 2, 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);
            Assert.IsTrue(pairs.Count() == 8);
            Assert.IsTrue(pairs.Contains((0, 0)));
            Assert.IsTrue(pairs.Contains((1, 0)));
            Assert.IsTrue(pairs.Contains((1, 1)));
            Assert.IsTrue(pairs.Contains((2, 1)));
            Assert.IsTrue(pairs.Contains((2, 2)));
            Assert.IsTrue(pairs.Contains((3, 2)));
            Assert.IsTrue(pairs.Contains((3, 3)));
            Assert.IsTrue(pairs.Contains((0, 3)));
        }

        [Test]
        public void CreationOfPairsWithout1MasterAntenna()
        {
            var masterIndices = new[] { 1, 2, 3 };
            var slaveIndices = new[] { 0, 1, 2, 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 6);
            Assert.IsTrue(pairs.Contains((1, 0)));
            Assert.IsTrue(pairs.Contains((1, 1)));
            Assert.IsTrue(pairs.Contains((2, 1)));
            Assert.IsTrue(pairs.Contains((2, 2)));
            Assert.IsTrue(pairs.Contains((3, 2)));
            Assert.IsTrue(pairs.Contains((3, 3)));
        }

        [Test]
        public void CreationOfPairsWithout2MasterAntenna()
        {
            var masterIndices = new[] { 1, 3 };
            var slaveIndices = new[] { 0, 1, 2, 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 4);
            Assert.IsTrue(pairs.Contains((1, 0)));
            Assert.IsTrue(pairs.Contains((1, 1)));
            Assert.IsTrue(pairs.Contains((3, 2)));
            Assert.IsTrue(pairs.Contains((3, 3)));
        }

        [Test]
        public void CreationOfPairsWithout1Master1SlaveAntenna()
        {
            var masterIndices = new[] { 1, 2, 3 };
            var slaveIndices = new[] { 1, 2, 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 5);
            Assert.IsTrue(pairs.Contains((1, 1)));
            Assert.IsTrue(pairs.Contains((2, 1)));
            Assert.IsTrue(pairs.Contains((2, 2)));
            Assert.IsTrue(pairs.Contains((3, 2)));
            Assert.IsTrue(pairs.Contains((3, 3)));
        }

        [Test]
        public void CreationOfPairsWithout3MasterAntenna()
        {
            var masterIndices = new[] { 1};
            var slaveIndices = new[] { 0, 1, 2, 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 2);
            Assert.IsTrue(pairs.Contains((1, 0)));
            Assert.IsTrue(pairs.Contains((1, 1)));
        }

        [Test]
        public void CreationOfPairsWithout1Master2SlaveAntenna()
        {
            var masterIndices = new[] { 1, 2, 3 };
            var slaveIndices = new[] { 1, 2 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 4);
            Assert.IsTrue(pairs.Contains((1, 1)));
            Assert.IsTrue(pairs.Contains((2, 1)));
            Assert.IsTrue(pairs.Contains((2, 2)));
            Assert.IsTrue(pairs.Contains((3, 2)));
        }

        [Test]
        public void CreationOfPairsWithout4Antennas()
        {
            var masterIndices = new[] { 1, 2, 3 };
            var slaveIndices = new[] { 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 1);
            Assert.IsTrue(pairs.Contains((3, 3)));
        }

        [Test]
        public void CreationOfPairsWith2NearbyAntennas()
        {
            var masterIndices = new[] { 1 };
            var slaveIndices = new[] { 1 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 1);
            Assert.IsTrue(pairs.Contains((1, 1)));
        }

        [Test]
        public void CreationOfPairsWith2SeparateAntennas()
        {
            var masterIndices = new[] { 1 };
            var slaveIndices = new[] { 3 };
            var pairs = AntennaHelper.CreatePairs(masterIndices, slaveIndices);

            Assert.IsTrue(pairs.Count() == 0);
        }

        [Test]
        public void AntennaIndices1_1ToAntennaNumbers() 
        {
            var (masterNumber, slaveNumber) = AntennaHelper.ConvertIndexPairToNumberPair(1, 1);
            Assert.IsTrue(masterNumber == 3);
            Assert.IsTrue(slaveNumber == 4);
        }

        [Test]
        public void AntennaIndices2_2ToAntennaNumbers()
        {
            var (masterNumber, slaveNumber) = AntennaHelper.ConvertIndexPairToNumberPair(2, 2);
            Assert.IsTrue(masterNumber == 5);
            Assert.IsTrue(slaveNumber == 6);
        }

        [Test]
        public void AntennaNumbers3_4ToAntennaIndices()
        {
            var (masterNumber, slaveNumber) = AntennaHelper.ConvertNumberPairToIndexPair(3, 4);
            Assert.IsTrue(masterNumber == 1);
            Assert.IsTrue(slaveNumber == 1);
        }

        [Test]
        public void AntennaNumbers1_2ToAntennaIndices()
        {
            var (masterNumber, slaveNumber) = AntennaHelper.ConvertNumberPairToIndexPair(1, 2);
            Assert.IsTrue(masterNumber == 0);
            Assert.IsTrue(slaveNumber == 0);
        }
    }
}
