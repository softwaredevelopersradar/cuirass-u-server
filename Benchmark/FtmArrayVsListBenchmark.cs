﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet;
using Benchmark;
using BenchmarkDotNet.Attributes;

namespace Benchmark
{
    public class FtmArrayVsListBenchmark
    {
        [Params(10, 30, 50, 100, 1000)]
        public int ScansCount { get; set; }

        private readonly Random god = new Random(4242);

        [Benchmark]
        public void ListImplementation1()
        {
            var output = new List<FftVoltScan>(ScansCount);
            for (int i = 0; i < ScansCount; i++)
            {
                var byteScan = getFtmData();
                output.Add(new FftVoltScan(byteScan));
            }
        }
        [Benchmark]
        public void ListImplementation2()
        {
            var output = new List<FftVoltScan>(ScansCount);
            for (int i = 0; i < ScansCount; i++)
            {
                var byteScan = getFtmData();
                output.Add(new FftVoltScan(byteScan, true));
            }
        }
        [Benchmark]
        public void ArrayImplementation1()
        {
            var output = new FftVoltScan[ScansCount];
            for (int i = 0; i < ScansCount; i++)
            {
                var byteScan = getFtmData();
                output[i] = new FftVoltScan(byteScan);
            }
        }
        [Benchmark]
        public void ArrayImplementation2()
        {
            var output = new FftVoltScan[ScansCount];
            for (int i = 0; i < ScansCount; i++)
            {
                var byteScan = getFtmData();
                output[i] = new FftVoltScan(byteScan, true);
            }
        }

        private byte[] getFtmData()
        {
            var output = new byte[512];
            god.NextBytes(output);
            System.Threading.Thread.Sleep(1);
            return output;
        }
    }
    public struct FftVoltScan
    {
        /// <summary>
        /// Amplitudes as volts
        /// </summary>
        public double[] Amplitudes { get; }

        public FftVoltScan(IEnumerable<byte> amplitudes)
        {
            Amplitudes = amplitudes.Select(e => e.ToVoltAmplitude()).ToArray();
        }
        public FftVoltScan(IEnumerable<byte> amplitudes, bool flag)
        {
            Amplitudes = new double[amplitudes.Count()];
            var it = amplitudes.GetEnumerator();
            it.MoveNext();
            for (int i = 0; i < Amplitudes.Length; i++, it.MoveNext())
            {
                Amplitudes[i] = it.Current.ToVoltAmplitude();
            }
            //Amplitudes = amplitudes.Select(e => e.ToVoltAmplitude()).ToArray();
        }

        public FftVoltScan(double[] amplitudes)
        {
            Amplitudes = amplitudes;
        }
    }
    public static class DataExtensions
    {
        //todo: 512 for narrow?
        public static double ToVoltAmplitude(this byte byteAmplitude) =>
            Math.Pow(10, (byteAmplitude - 30 - 40 * Math.Log(466, 10)) / 20);
    }
}
