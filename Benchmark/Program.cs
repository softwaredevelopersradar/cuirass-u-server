﻿using BenchmarkDotNet.Running;
using System;

namespace Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            //var z = new FtmArrayVsListBenchmark();
            //z.ScansCount = 10;
            //z.ArrayImplementation2();
            var summary = BenchmarkRunner.Run<FtmArrayVsListBenchmark>();
            Console.ReadLine();
        }
    }
}
