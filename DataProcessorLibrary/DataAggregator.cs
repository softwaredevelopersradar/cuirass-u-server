﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel.Data;

namespace DataProcessorLibrary
{
    public static class DataAggregator
    {
        public static IEnumerable<FftVoltScan> GetFftDataVolts(Func<byte[]> getFftResult, int scansCount)
        {
            for (int i = 0; i < scansCount; i++)
                yield return new FftVoltScan(getFftResult());
        }

        public static (List<FftVoltScan>, List<FftVoltScan>) GetFftDataVolts(Func<byte[]> getMasterFftResult, Func<byte[]> getSlaveFftResult, int scansCount)
        {
            return (GetFftDataVolts(getMasterFftResult, scansCount).ToList(), GetFftDataVolts(getSlaveFftResult, scansCount).ToList());
        }
    }
}
