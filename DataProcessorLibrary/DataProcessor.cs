﻿using System;
using System.Linq;
using System.Collections.Generic;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DataProcessorLibrary.Data;
using DspDataModel;
using DspDataModel.Hardware;

namespace DataProcessorLibrary
{
    public static class DataProcessor
    {
        /// <summary>
        /// Returns standard deviation for the whole data set
        /// </summary>
        public static double GetStandardDeviation(List<FftVoltScan> dataSet)
        {
            var averages = GetAverages(dataSet);
            FormDeviationData(dataSet, averages);
            var deviations = averages;//rename for clarity
            for (int i = 0; i < deviations.Length; i++)
            {
                deviations[i] = Math.Sqrt(dataSet[i].Amplitudes.Sum());
            }
            return deviations.Average();
        }

        /// <summary>
        /// Returns the array of average values of each array in the data set
        /// </summary>
        public static double[] GetAverages(List<FftVoltScan> dataSet)
        {
            var averages = new double[dataSet.Count];
            for (int i = 0; i < averages.Length; i++)
            {
                averages[i] = dataSet[i].Amplitudes.Average();
            }
            return averages;
        }

        /// <summary>
        /// Transforms input data set the following way : (x - x.average)^2 / (k - 1)
        /// </summary>
        private static void FormDeviationData(List<FftVoltScan> dataSet, IReadOnlyList<double> averages)
        {
            for (int i = 0; i < dataSet.Count; i++)
            {
                for (int j = 0; j < dataSet[i].Amplitudes.Length; j++)
                {
                    dataSet[i].Amplitudes[j] = 
                        (dataSet[i].Amplitudes[j] - averages[i]) * (dataSet[i].Amplitudes[j] - averages[i]) / 
                        (dataSet[i].Amplitudes.Length - 1); 
                    // (x - x.average)^2 / (k - 1)
                }
            }
        }
        public static IEnumerable<ISignal> GetSignalsFromScans(List<FftVoltScan> mData, List<FftVoltScan> sData,
           float startFrequencyKhz, float stepKhz, double threshold, int masterAntennaIndex, int slaveAntennaIndex)
        {
            (var mPeaks, var sPeaks) = FindSignalIndicesWithRank(mData, sData, threshold);
            var intersectedPeaks = mPeaks.Intersect(sPeaks);
            var signalPeaks = DividePeaksToSignalPeaks(intersectedPeaks.ToList());

            var possibleSignals = new List<PossibleSignal>();
            foreach (var signal in signalPeaks)
            {
                possibleSignals.Add(new PossibleSignal(signal, mData, sData, startFrequencyKhz, stepKhz,
                    masterAntennaIndex, slaveAntennaIndex));
            }
            var results = possibleSignals.Select(ConvertPossibleSignalToSignal).ToList();
            results.RemoveAll(s => s == null);
            return results;
        }

        public static (IReadOnlyList<int>, IReadOnlyList<int>) FindSignalIndicesWithRank(
            List<FftVoltScan> masterDataSet, 
            List<FftVoltScan> slaveDataSet, 
            double threshold)
        {
            return (FindSignalIndicesWithRank(masterDataSet, threshold), FindSignalIndicesWithRank(slaveDataSet, threshold));
        }

        /// <summary>
        /// Returns list of local peaks that surpassed rank threshold (via cumulative sum) in a given data set
        /// </summary>
        public static IReadOnlyList<int> FindSignalIndicesWithRank(List<FftVoltScan> dataSet, double threshold)
        {
            var scansCount = dataSet.Count;
            var sampleCount = dataSet[0].Amplitudes.Length;
            var cumulativeSum = new Dictionary<int,double>();
            
            for(int i = 0 ; i < scansCount; i++)
                for(int j = 0 ; j < sampleCount;j++)
                    if (dataSet[i].Amplitudes[j] > threshold)
                    {
                        if (cumulativeSum.ContainsKey(j))
                            cumulativeSum[j] += dataSet[i].Amplitudes[j];
                        else
                            cumulativeSum.Add(j, dataSet[i].Amplitudes[j]);
                    }

            var maxElement = dataSet.Select(scan => scan.Amplitudes.Max()).Max();
            var rankData = new double[sampleCount];

            for (int i = 0; i < cumulativeSum.Keys.Count; i++)
            {
                var key = cumulativeSum.Keys.ElementAt(i);
                if (cumulativeSum[key] > maxElement * 1.5)
                {
                    rankData[key] = cumulativeSum[key];
                }
            }
            var peakIndices = new List<int>();

            for (int i = 0; i < rankData.Length; i++)
            {
                if(rankData[i] == 0)
                    continue;
                peakIndices.Add(i);
            }
            return peakIndices;
        }

        public static IReadOnlyList<List<int>> DividePeaksToSignalPeaks(IReadOnlyList<int> peaks)
        {
            var output = new List<List<int>>();
            if (peaks.Count == 0)
                return output;

            var index = 0;
            output.Add(new List<int>());
            for (int i = 0; i < peaks.Count; i++)
            {
                if (output[index].Count == 0)
                {
                    output[index].Add(peaks[i]);
                    continue;
                }

                var lastIndex = output[index].Last();
                if (Math.Abs(lastIndex - peaks[i]) < 2)
                {
                    output[index].Add(peaks[i]);
                }
                else
                {
                    index++;
                    output.Add(new List<int>());
                    output[index].Add(peaks[i]);
                }
            }
            return output;
        }
        public static ISignal ConvertPossibleSignalToSignal(PossibleSignal signal)
        {
            if (Math.Abs(signal.Direction) > 22.5f)
            {
                return null;
                // this check ensures that signal direction is between antennas
                // if the value is bigger than 22.5, it means that signal should be in another antenna pair
                // or it is an invalid signal
                // thus we delete such results
            }

            var signalDirection = AntennaHelper.ClarifyDirection((float)signal.Direction, signal.MasterAntennaIndex, signal.SlaveAntennaIndex);

            return new Signal(
                frequencyKhz: signal.PeakIndex.FrequencyKhz,
                amplitude: (byte)((signal.PeakIndex.MasterAmplitudeVolts.ToByteAmplitude() + signal.PeakIndex.SlaveAmplitudeVolts.ToByteAmplitude()) / 2),
                direction: signalDirection,
                standardDeviation: (float)signal.StandardDeviation,
                masterAntennaIndex: signal.MasterAntennaIndex,
                slaveAntennaIndex: signal.SlaveAntennaIndex);
        }
    }
}
