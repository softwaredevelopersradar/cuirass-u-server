﻿using System.Threading;
using DspDataModel;
using DspDataModel.Data;

namespace DataProcessorLibrary.Data
{
    public class Signal : ISignal
    {
        private static int idFactory = 0;

        public int Id { get; private set; }

        public float FrequencyKhz { get; private set; }

        public byte Amplitude { get; private set; }

        public double AmplitudeVolt => Amplitude.ToVoltAmplitude();

        public float Direction { get; private set; }

        public float BandwidthKhz { get; private set; }

        public float StandardDeviation { get; private set; }

        public float DurationMcs { get; private set; }

        public float RepeatPeriodMcs { get; private set; }

        public int MasterAntennaIndex { get; private set; }

        public int SlaveAntennaIndex { get; private set; }

        public string Note { get; set; }

        public Signal(float frequencyKhz, byte amplitude, float direction, float standardDeviation, 
            int masterAntennaIndex, int slaveAntennaIndex)
        {
            Id = Interlocked.Increment(ref idFactory);
            FrequencyKhz = frequencyKhz;
            Amplitude = amplitude;
            Direction = direction;
            StandardDeviation = standardDeviation;
            MasterAntennaIndex = masterAntennaIndex;
            SlaveAntennaIndex = slaveAntennaIndex;
        }

        public void Update(ISignal newSignal)
        {
            this.FrequencyKhz = newSignal.FrequencyKhz;
            this.Amplitude = newSignal.Amplitude;
            this.Direction = newSignal.Direction;
            this.StandardDeviation = newSignal.StandardDeviation;

            //this ifs to prevent measured update of measured signal with unmeasured values
            if (newSignal.BandwidthKhz != 0)
            {
                this.BandwidthKhz = newSignal.BandwidthKhz;
            }
            if (newSignal.DurationMcs != 0)
            {
                this.DurationMcs = newSignal.DurationMcs;
            }
            if (newSignal.RepeatPeriodMcs != 0)
            {
                this.RepeatPeriodMcs = newSignal.RepeatPeriodMcs;
            }
        }

        public void UpdateCalculatorValues(int bandWidthKhz, int durationNs, int repeatPeriodNs)
        {
            BandwidthKhz = bandWidthKhz;
            DurationMcs = durationNs / 1000.0f;
            RepeatPeriodMcs = repeatPeriodNs / 1000.0f;            
            
            Note = string.Empty;// clear in case of being "failed to measure"
        }
    }
}
