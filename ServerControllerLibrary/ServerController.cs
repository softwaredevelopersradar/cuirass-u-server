﻿using DspDataModel;
using DspDataModel.Database;
using DspDataModel.Hardware;
using DspDataModel.Server;
using DspDataModel.Tasks;
using System;

namespace ServerControllerLibrary
{
    public class ServerController : IServerController
    {
        public IDatabaseController DatabaseController { get; private set; }
        public ITaskManager TaskManager { get; private set; }
        public IFtmDeviceManager DeviceManager { get; private set; }

        private readonly DspServerLibrary.DspServer _server;

        public ServerController() 
        {
            DeviceManager = new HardwareLibrary.Ftm.FtmDeviceManager();
            DatabaseController = new DatabaseLibrary.DatabaseController("Cuirass Server"); //todo : add config read
            TaskManager = new TasksLibrary.TaskManager(DeviceManager, DatabaseController);
            var config = Config.Instance.AppSettings;
            _server = new DspServerLibrary.DspServer(config.ServerHost, config.ServerPort, this);
            EstablishConnections();
        }

        private void EstablishConnections()
        {
            DeviceManager.Connect();
            DatabaseController.Connect();
            TaskManager.Initialize();
            _server.Start();
        }
    }
}
