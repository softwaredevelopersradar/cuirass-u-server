﻿using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Storages;

namespace DataStorages
{
    public class SecondarySpectrumStorage : ISecondarySpectrumStorage
    {
        public IScanStorage<FtmScan> NarrowSpectrumStorage { get; private set; }
        public IScanStorage<VideoScan> VideoSpectrumStorage { get; private set; }
        public IScanStorage<RfSignalScan> RfSpectrumStorage { get; private set; }

        public SecondarySpectrumStorage() 
        {
            NarrowSpectrumStorage = new ScanStorage<FtmScan>();
            VideoSpectrumStorage = new ScanStorage<VideoScan>();
            RfSpectrumStorage = new ScanStorage<RfSignalScan>();
        }

        public void PutNarrow(FtmScan scan, FtmRole role) =>
            NarrowSpectrumStorage.Put(scan, role);

        public void PutVideo(VideoScan scan, FtmRole role) =>
            VideoSpectrumStorage.Put(scan, role);

        public void PutRf(RfSignalScan scan, FtmRole role) =>
            RfSpectrumStorage.Put(scan, role);
    }
}
