﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DspDataModel.Storages;
using Settings;

namespace DataStorages
{
    public class HistoryStorage : IHistoryStorage
    {
        private readonly Queue<byte[]> _history;
        private readonly int capacity = 1000;
        //todo : to property
        private int _historyScanLength = Constants.WideBandFftSampleCount;

        private CancellationTokenSource cancellationTokenSource;

        public HistoryStorage() 
        {
            _history = new Queue<byte[]>(capacity * 2);
            Clear();
            Start();
        }

        public void AddScan(byte[] scan)
        {
            lock (_history)
            {
                if (scan.Length != _historyScanLength)
                {
                    _historyScanLength = scan.Length;
                    LockLessClear();
                }

                while (_history.Count > capacity)
                {
                    _history.Dequeue();
                }

                _history.Enqueue(scan);
            }
        }

        public byte[] GetHistory()
        {
            lock (_history)
            {
                var output = new byte[capacity * _historyScanLength];
                var offset = 0;
                var counter = 0;
                foreach (var scan in _history)
                {
                    if (counter == capacity)
                        break;
                    Array.Copy(scan, 0, output, offset, scan.Length);
                    offset += scan.Length;
                    counter++;
                }
                return output;
            }
        }

        public void Start()
        {
            if (cancellationTokenSource != null)
                return;

            cancellationTokenSource = new CancellationTokenSource();
            Task.Run(CleaningTask);
        }

        public void Stop()
        {
            cancellationTokenSource.Cancel();
        }

        public void Clear()
        {
            lock (_history)
            {
                LockLessClear();
            }
        }

        private void LockLessClear()
        {
            _history.Clear();
            for (int i = 0; i < capacity; i++)
                _history.Enqueue(new byte[_historyScanLength]);
        }

        private async Task CleaningTask()
        {
            if (cancellationTokenSource == null)
                return;

            while (!cancellationTokenSource.Token.IsCancellationRequested)
            {
                lock (_history)
                {
                    while (_history.Count > capacity)
                    {
                        _history.Dequeue();
                    }
                }

                await Task.Delay(100).ConfigureAwait(false);
            }
        }

        public int GetNumberOfPointsPerScan()
        {
            return _historyScanLength;
        }
    }
}
