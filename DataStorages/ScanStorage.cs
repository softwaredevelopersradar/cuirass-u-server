﻿using DspDataModel;
using DspDataModel.Storages;

namespace DataStorages
{
    /// <summary>
    /// Storage for master/slave scans of some type.
    /// </summary>
    public class ScanStorage<T> : IScanStorage<T> where T : struct
    {
        public T MasterStorage { get; private set; }
        public T SlaveStorage { get; private set; }

        public void Put(T scan, FtmRole role)
        {
            if (role == FtmRole.Master)
                MasterStorage = scan;
            else
                SlaveStorage = scan;
        }
        public void Clear()
        {
            MasterStorage = new T();
            SlaveStorage = new T();
        }

        public T GetStorage(FtmRole role) =>
            role == FtmRole.Master ? MasterStorage : SlaveStorage;

    }
}
