﻿using System;
using System.Linq;
using System.Collections.Generic;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Storages;

namespace DataStorages
{
    public class SignalStorage : ISignalStorage
    {
        private readonly List<ISignal> _signals;
        private readonly object _lock;

        public IReadOnlyList<ISignal> Signals => _signals;

        public SignalStorage()
        {
            _lock = new object();
            _signals = new List<ISignal>(100);
        }

        public void Put(IReadOnlyList<ISignal> signals)
        {
            try
            {
                lock (_lock)
                {
                    foreach (var signal in signals)
                    {
                        var oldSignal = _signals.FirstOrDefault(s => s.AreSameSignals(signal));
                        if (oldSignal != null)
                            oldSignal.Update(signal);
                        else
                            _signals.Add(signal);
                    }
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
            }
        }

        public bool Remove(int signalId)
        {
            lock (_lock)
            {
                var signal = _signals.FirstOrDefault(s => s.Id == signalId);
                if (signal == null)
                    return false;
                return _signals.Remove(signal);
            }
        }

        public ISignal GetSignal(int signalId)
        {
            lock (_lock)
            {
                return _signals.FirstOrDefault(s => s.Id == signalId);
            }
        }

        public void Clear()
        {
            lock (_lock)
            {
                _signals.Clear();
            }
        }
    }
}