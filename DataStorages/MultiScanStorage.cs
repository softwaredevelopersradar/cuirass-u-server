﻿using System;
using DspDataModel;
using DspDataModel.Storages;

namespace DataStorages
{ 
    public class MultiScanStorage<T> : IMultiScanStorage<T> where T : struct
    {
        private T[] _masterStorage;
        private T[] _slaveStorage;

        public int NumberOfScans { get; }

        public MultiScanStorage(int numberOfScans)
        {
            if (numberOfScans < 0)
                throw new ArgumentOutOfRangeException("numberOfScans", "Number of scans cannot be less than 0");

            NumberOfScans = numberOfScans;
            Clear();
        }

        public void Clear()
        {
            _masterStorage = new T[NumberOfScans];
            _slaveStorage = new T[NumberOfScans];
        }

        public void Put(T scan, int scanIndex, FtmRole role)
        {
            var storage = GetStorage(role);
            IsScanIndexValid(storage, scanIndex);
            lock (storage)
            {
                storage[scanIndex] = scan;
            }
        }

        public T GetScan(int scanIndex, FtmRole role)
        {
            var storage = GetStorage(role);
            IsScanIndexValid(storage, scanIndex);
            lock (storage)
            {
                return storage[scanIndex];
            }
        }

        private T[] GetStorage(FtmRole role) => role == FtmRole.Master ? _masterStorage : _slaveStorage;

        private void IsScanIndexValid(T[] storage, int scanIndex)
        {
            if (scanIndex >= storage.Length || scanIndex < 0)
                throw new ArgumentOutOfRangeException("scanIndex", $"Scan index cannot be less than 0 and more than {storage.Length - 1}");
        }
    }
}
