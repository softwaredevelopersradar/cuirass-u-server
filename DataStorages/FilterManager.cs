﻿using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Storages;
using System;
using System.Linq;
using System.Collections.Generic;
using DspDataModel;

namespace DataStorages
{
    public class FilterManager : IFilterManager
    {
        private FrequencyRange[] _intelligenceRanges;

        private FrequencyRange[] _forbiddenRanges;

        private DirectionSector[] _intelligenceSectors;

        private readonly object _lockObject = new object();

        public IReadOnlyList<FrequencyRange> IntelligenceRanges => _intelligenceRanges;
        public IReadOnlyList<FrequencyRange> ForbiddenRanges => _forbiddenRanges;
        public IReadOnlyList<DirectionSector> IntelligenceSectors => _intelligenceSectors;

        public AntennaSetting AntennaSettings { get; set; }
        public NarrowVideoClarificationSetting NarrowVideoSettings { get; set; }
        public NarrowVideoClarificationSetting VideoSettings { get; set; }
        public bool IsWaterfallEnabled { get; set; } = false;
        public bool IsNarrowMeasuringEnabled { get; set; } = true;
        public FrequencyRange WideMeasureRange { get; set; }

        public event EventHandler<IReadOnlyList<FrequencyRange>> IntelligenceRangesUpdatedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesUpdatedEvent;

        public FilterManager() 
        {
            AntennaSettings = new AntennaSetting(new[] { 0, 1, 2, 3 }, new[] { 0, 1, 2, 3 });
            //default rdf settings
        }

        public IEnumerable<(int MasterIndex, int SlaveIndex)> GetRdfSettings() 
        {
            var config = Config.Instance.DirectionFindingSettings;
            var pairs = config.PairsMode == RdfPairsMode.FullPairs
                ? AntennaSettings.Pairs
                : AntennaSettings.FastRdfPairs;
            return pairs;
        }

        public bool SetForbiddenRanges(IEnumerable<FrequencyRange> forbiddenRanges)
        {
            lock (_lockObject)
            {
                var ranges = forbiddenRanges.OrderBy(r => r.StartFrequencyKhz).ToArray();
                if (!AreListsEqual(ranges, _forbiddenRanges))
                {
                    _forbiddenRanges = ranges;
                    ForbiddenRangesUpdatedEvent?.Invoke(this, ForbiddenRanges);
                    return true;
                }
                return false;
            }
        }

        public bool SetIntelligenceRanges(IEnumerable<FrequencyRange> intelligenceRanges)
        {
            lock (_lockObject)
            {
                var ranges = intelligenceRanges.OrderBy(r => r.StartFrequencyKhz).ToArray();
                if (!AreListsEqual(ranges, _intelligenceRanges))
                {
                    _intelligenceRanges = ranges;
                    IntelligenceRangesUpdatedEvent?.Invoke(this, IntelligenceRanges);
                    return true;
                }
                return false;
            }
        }
        

        // hope lists are sorted in same way
        private static bool AreListsEqual<T>(IReadOnlyList<T> list1, IReadOnlyList<T> list2) where T : IEquatable<T>
        {
            if (list1 == null || list2 == null)
                return false;
            if (list1.Count != list2.Count)
            {
                return false;
            }
            for (var i = 0; i < list1.Count; ++i)
            {
                if (!list1[i].Equals(list2[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public bool SetIntelligenceSectors(IEnumerable<DirectionSector> intelligenceSectors)
        {
            lock (_lockObject)
            {
                // screw the checks?
                _intelligenceSectors = intelligenceSectors.ToArray();
                return true;
            }
        }
    }
}
