﻿using Grpc.Core;
using System;
using System.Linq;
using System.Threading.Tasks;
using TransmissionPackage;
using System.Collections.Generic;
using DspDataModel;
using DspDataModel.Server;
using TasksLibrary.Modes;
using Settings;
using System.Threading;
using TasksLibrary.Tasks;
using DspDataModel.Data;

namespace DspServerLibrary
{
    public class ProtoServer : Transmission.TransmissionBase
    {
        private readonly Dictionary<string, DateTime> _clients;
        private readonly IServerController _serverController;
        private readonly DspDataModel.Tasks.ITaskManager _taskManager;
        private readonly DspDataModel.Hardware.IFtmDeviceManager _deviceManager;
        private readonly DspDataModel.Storages.IFilterManager _filterManager;

        private readonly ManualResetEvent _manualResetEvent;
        private DspDataModel.Hardware.AntennaSwitchSource _latestSwitchSource;

        private DspDataModel.Tasks.IFtmTask _narrowSpectrumTask;
        private DspDataModel.Tasks.IFtmTask _videoSpectrumTask;

        public ProtoServer(IServerController serverController) 
        {
            _clients = new Dictionary<string, DateTime>();
            _serverController = serverController;
            _taskManager = _serverController.TaskManager;
            _deviceManager = _serverController.DeviceManager;
            _filterManager = _taskManager.FilterManager;
            _manualResetEvent = new ManualResetEvent(false);
            _taskManager.ReceiverManager.MasterReceiver.AntennaCommutator.WorkingAntennasChanged += AntennaCommutator_WorkingAntennasChanged;
            _taskManager.ReceiverManager.SlaveReceiver.AntennaCommutator.WorkingAntennasChanged += AntennaCommutator_WorkingAntennasChanged;
        }

        private void AntennaCommutator_WorkingAntennasChanged(object sender, DspDataModel.Hardware.AntennaSwitchSource switchSource)
        {
            if (switchSource == DspDataModel.Hardware.AntennaSwitchSource.None)
                return;
            _latestSwitchSource = switchSource;
            _manualResetEvent.Set();
        }

        private void ServerLog(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            var time = _commandReceiveTime;
            MessageLogger.Log($"{time.Hour:D2}:{time.Minute:D2}:{time.Second:D2}:{time.Millisecond:D3} ({watch.Elapsed.TotalMilliseconds:F3} ms) : {message}", color);
        }

        private DateTime _commandReceiveTime;
        private readonly System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

        private void CommandReceived() 
        {
            _commandReceiveTime = DateTime.Now;
            watch.Restart();
        }

        public override Task<PingResponse> Ping(PingRequest request, ServerCallContext context)
        {
            CommandReceived();
            var clientValuePair = _clients.FirstOrDefault(c => c.Key.Equals(request.ClientName));
            if (clientValuePair.Value.Year != 1)//Year 1 is the year of default datetime
            {
                _clients[clientValuePair.Key] = DateTime.Now;
            }
            else
                _clients.Add(request.ClientName, DateTime.Now);
            ServerLog("Ping response sended");
            return Task.FromResult(new PingResponse() { IsFirstPingRequest = true });
        }

        public override Task<SpectrumResponse> GetSpectrum(SpectrumRequest request, ServerCallContext context)
        {
            CommandReceived();
            var storage = _taskManager.WideBandSpectrumStorage;
            var gap = 2; //2 dots for the 880-900 gap
            var spectrum = new byte[Constants.NumberOfSamplesFrom100MhzTo18Ghz];
            var role = (DspDataModel.FtmRole)request.Role;
            var indexToCopy = 0;
            for (int i = 0; i < storage.NumberOfScans; i++)
            {
                var band = new Band(i);
                if (i == 28)
                {
                    Array.Copy(new byte[gap], 0, spectrum, indexToCopy, gap);
                }
                var afterGap = i > 27 ? gap : 0; //todo : to constants
                var scan = storage.GetScan(i, role).Scan ?? new byte[band.ScanSampleCount];
                Array.Copy(scan, 0, spectrum, indexToCopy + afterGap, scan.Length);
                indexToCopy += scan.Length;
            }
            var result = new SpectrumResponse()
            {
                Spectrum = Google.Protobuf.ByteString.CopyFrom(spectrum)
            };
            ServerLog("Get spectrum request");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetMode(SetModeRequest request, ServerCallContext context)
        {
            CommandReceived();
            switch (request.Mode)
            {
                case TransmissionPackage.DspServerMode.Stop:
                    _taskManager.CurrentMode?.CancelTasks();
                    break;
                case TransmissionPackage.DspServerMode.RadioIntelligence:
                    _taskManager.SetMode(new RdfMode(_taskManager));
                    break;
                case TransmissionPackage.DspServerMode.FastRadioIntelligence:
                    //todo : add
                    break;
            }
            //todo : await set mode
            ServerLog($"Set mode request : {request.Mode}");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<GetModeResponse> GetMode(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            //todo : get mode
            var result = new GetModeResponse() { Mode = TransmissionPackage.DspServerMode.Stop };
            ServerLog($"Get mode request");
            return Task.FromResult(result);
        }

        public override Task<SpectrumResponse> GetNarrowSpectrum(SpectrumRequest request, ServerCallContext context)
        {
            CommandReceived();
            var storage = _taskManager.SecondarySpectrumStorage
                .NarrowSpectrumStorage.GetStorage((DspDataModel.FtmRole)request.Role);
            var scan = storage.Scan ?? new byte[0];
            var result = new SpectrumResponse()
            {
                Spectrum = Google.Protobuf.ByteString.CopyFrom(scan)
            };
            ServerLog("Get narrow spectrum request");
            return Task.FromResult(result);
        }

        public override Task<VideoSpectrumResponse> GetVideoSpectrum(SpectrumRequest request, ServerCallContext context)
        {
            CommandReceived();
            var storage = _taskManager.SecondarySpectrumStorage
                .VideoSpectrumStorage.GetStorage((DspDataModel.FtmRole)request.Role);
            var scan = storage.Scan ?? new int[0];
            var result = new VideoSpectrumResponse();
            result.Spectrum.AddRange(scan);
            ServerLog("Get video spectrum request");
            return Task.FromResult(result);
        }

        public override Task<RfSignalSpectrumResponse> GetRfSignalSpectrum(SpectrumRequest request, ServerCallContext context)
        {
            CommandReceived();
            var storage = _taskManager.SecondarySpectrumStorage
                .RfSpectrumStorage.GetStorage((DspDataModel.FtmRole)request.Role);
            var result = new RfSignalSpectrumResponse();
            result.RawSpectrum.AddRange(storage.RawScan ?? new int[0]);
            result.WindowedSpectrum.AddRange(storage.WindowedScan ?? new int[0]);
            ServerLog("Get rf signals spectrum request");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetAntennaFrequencySettings(FrequencyAntennaSetting request, ServerCallContext context)
        {
            CommandReceived();
            var bandNumber = Utilities.GetBandNumber(request.FrequencyMhz);
            var ftmFrequencyKhz = (int) NarrowFrequencyCalculator.GetFtmNarrowFrequency(request.FrequencyMhz * 1000, bandNumber);
            var frequencyAntennaSettings = new DspDataModel.Hardware.NarrowVideoClarificationSetting(request.FrequencyMhz * 1000, ftmFrequencyKhz, bandNumber);
            var antennaSettings = new DspDataModel.Hardware.AntennaSetting(request.MasterAntennaIndices, request.SlaveAntennaIndices);
            _filterManager.AntennaSettings = antennaSettings;
            _filterManager.NarrowVideoSettings = frequencyAntennaSettings;
            _taskManager.HistoryStorage.Clear();
            ServerLog($"Set antenna frequency settings");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override async Task SubscribeForAntennaUpdates(DefaultRequest request, IServerStreamWriter<AntennasResponse> responseStream, ServerCallContext context)
        {
            while (_manualResetEvent.WaitOne())
            {
                var master = _taskManager.ReceiverManager.MasterReceiver.AntennaCommutator;
                var slave = _taskManager.ReceiverManager.SlaveReceiver.AntennaCommutator;

                var (MasterAntennaNumber, SlaveAntennaNumber) = DspDataModel.Hardware.AntennaHelper
                    .ConvertIndexPairToNumberPair(master.WorkingAntennaIndex, slave.WorkingAntennaIndex);
                var workingAntennas = new List<int>() { MasterAntennaNumber, SlaveAntennaNumber};
                var antennaMessage = new AntennasResponse
                {
                    SwitchSource = (AntennaSwitchSource)_latestSwitchSource
                };
                antennaMessage.WorkingAntennas.AddRange(workingAntennas);

                await responseStream.WriteAsync(antennaMessage);
                _manualResetEvent.Reset();
                await Task.Delay(50);
            }
        }

        public override Task<DefaultResponse> StartNarrowRdfTask(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            if (_filterManager.NarrowVideoSettings.IsDefault)
            {
                ServerLog("Start narrow band spectrum task failed, settings were not set", ConsoleColor.Yellow);
                return Task.FromResult(GenerateDefaultResponse(false));
            }
            _narrowSpectrumTask = new NarrowRdfTask(_taskManager, true, 1);
            _taskManager.AddTask(_narrowSpectrumTask);
            ServerLog("Start narrow band spectrum task");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> StopNarrowRdfTask(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            if (_narrowSpectrumTask != null)
                _narrowSpectrumTask.Cancel();
            ServerLog("Stop narrow band spectrum task");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> StartVideoSpectrumTask(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            if (_filterManager.NarrowVideoSettings.IsDefault)
            {
                ServerLog("Start video spectrum task failed, settings were not set", ConsoleColor.Yellow);
                return Task.FromResult(GenerateDefaultResponse(false));
            }
            _videoSpectrumTask = new VideoSpectrumTask(_taskManager, true, 1);
            _taskManager.AddTask(_videoSpectrumTask);
            ServerLog("Start video spectrum task");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> StopVideoSpectrumTask(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            if (_videoSpectrumTask != null)
                _videoSpectrumTask.Cancel();
            ServerLog("Stop video spectrum task");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        private DspDataModel.Hardware.IBandSettings GetBandSettings(bool isMaster, bool isWideBand)
        {
            var device = isMaster
                ? _deviceManager.Ftm2M
                : _deviceManager.Ftm2S;
            return isWideBand
                ? device.WideSettings as DspDataModel.Hardware.IBandSettings
                : device.NarrowSettings;
        }

        private DspDataModel.Hardware.IBandSettings GetBandSettings(ComparatorThresholdRequest request)
        {
            return GetBandSettings(request.IsMaster, request.IsWideBand);
        }

        public override Task<ComparatorThresholdMessage> GetAttackThreshold(ComparatorThresholdRequest request, ServerCallContext context)
        {
            CommandReceived();
            var threshold = GetBandSettings(request).AttackThreshold;
            var result = new ComparatorThresholdMessage() { IsMaster = true, IsWideBand = true, ThresholdParrots = threshold };
            ServerLog("Get attack threshold");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetAttackThreshold(ComparatorThresholdMessage request, ServerCallContext context)
        {
            CommandReceived();
            var role = request.IsMaster ? DspDataModel.FtmRole.Master : DspDataModel.FtmRole.Slave;
            var mode = request.IsWideBand ? FtmMode.Wideband : FtmMode.Narrowband;
            var settings = _deviceManager.GetBandSettings(role, mode);
            settings.SetAttackThreshold((short)request.ThresholdParrots);
            ServerLog("Set attack threshold");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<ComparatorThresholdMessage> GetPicThreshold(ComparatorThresholdRequest request, ServerCallContext context)
        {
            CommandReceived();
            var threshold = GetBandSettings(request).PicThreshold;
            var result = new ComparatorThresholdMessage() { IsMaster = true, IsWideBand = true, ThresholdParrots = threshold };
            ServerLog("Get pic threshold");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetPicThreshold(ComparatorThresholdMessage request, ServerCallContext context)
        {
            CommandReceived();
            var role = request.IsMaster ? DspDataModel.FtmRole.Master : DspDataModel.FtmRole.Slave;
            var mode = request.IsWideBand ? FtmMode.Wideband : FtmMode.Narrowband;
            var settings = _deviceManager.GetBandSettings(role, mode);
            settings.SetPicThreshold((short)request.ThresholdParrots);
            ServerLog("Set pic threshold");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<ThresholdMessage> GetRdfThreshold(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var threshold = _deviceManager.Threshold.ToByteAmplitude();
            var result = new ThresholdMessage() { ThresholdParrots = threshold };
            ServerLog("Get pic threshold");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetRdfThreshold(ThresholdMessage request, ServerCallContext context)
        {
            CommandReceived();
            _deviceManager.Threshold = ((byte)request.ThresholdParrots).ToVoltAmplitude();
            ServerLog($"Set rdf threshold");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<VideoSpectrumWorkingBandMessage> GetVideoWorkingBand(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var result = new VideoSpectrumWorkingBandMessage() { WorkingBand = (WorkingBand)_deviceManager.WorkingBand };
            ServerLog($"Get video working band");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetVideoWorkingBand(VideoSpectrumWorkingBandMessage request, ServerCallContext context)
        {
            CommandReceived();
            var videoWorkingBand = (DspDataModel.Hardware.VideoWorkingBand)request.WorkingBand;
            _deviceManager.SetVideoWorkingBand(videoWorkingBand);
            ServerLog($"Set video working band");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> ClearSignalStorage(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            _taskManager.SignalStorage.Clear();
            _serverController.DatabaseController.ClearFrsSignalsTable();
            ServerLog($"Clear signal storage");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> RemoveFromSignalStorage(SignalIdMessage request, ServerCallContext context)
        {
            CommandReceived();
            var isRemoved = _taskManager.SignalStorage.Remove(request.SignalId);
            _serverController.DatabaseController.ClearFrsSignalsTable();
            _taskManager.DatabaseController.AddFrsSignals(_taskManager.SignalStorage.Signals);
            ServerLog($"Remove from signal storage");
            return Task.FromResult(GenerateDefaultResponse(isRemoved));
        }

        public override Task<AttenuatorMessage> GetCommutatorAttenuator(SpectrumRequest request, ServerCallContext context)
        {
            CommandReceived();
            var commutator = request.Role == TransmissionPackage.FtmRole.Master 
                ?  _taskManager.ReceiverManager.MasterReceiver.AntennaCommutator
                : _taskManager.ReceiverManager.SlaveReceiver.AntennaCommutator;
            var result = new AttenuatorMessage()
            {
                Role = request.Role,
                Attenuator = commutator.DefaultAttenuator
            };
            ServerLog($"Get commutator attenuator");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetCommutatorAttenuator(AttenuatorMessage request, ServerCallContext context)
        {
            CommandReceived();
            var commutator = request.Role == TransmissionPackage.FtmRole.Master
                ? _taskManager.ReceiverManager.MasterReceiver.AntennaCommutator
                : _taskManager.ReceiverManager.SlaveReceiver.AntennaCommutator;
            commutator.DefaultAttenuator = request.Attenuator;
            ServerLog($"Set commutator attenuator");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        private DefaultResponse GenerateDefaultResponse(bool isSucceeded)
        {
            return new DefaultResponse() { IsSucceeded = isSucceeded };
        }

        public override Task<StrobeModeMessage> GetStrobeMode(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var mode = _deviceManager.Ftm2M.Settings.StrobeMode;
            var result = new StrobeModeMessage()
            {
                Mode = (StrobeMode) mode
            };
            ServerLog($"Get strobe mode");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetStrobeMode(StrobeModeMessage request, ServerCallContext context)
        {
            CommandReceived();
            var result = _deviceManager.SetStrobeMode((DspDataModel.Hardware.StrobeMode) request.Mode);
            ServerLog($"Set strobe mode");
            return Task.FromResult(GenerateDefaultResponse(result));
        }

        public override Task<PairsModeMessage> GetPairsMode(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var mode = Config.Instance.DirectionFindingSettings.PairsMode;
            var result = new PairsModeMessage()
            {
                Mode = (TransmissionPackage.RdfPairsMode)mode
            };
            ServerLog($"Get pairs mode");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetPairsMode(PairsModeMessage request, ServerCallContext context)
        {
            CommandReceived();
            Config.Instance.DirectionFindingSettings.PairsMode = (DspDataModel.RdfPairsMode)request.Mode;
            ServerLog($"Set pairs mode");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<ExternalSecondsMessage> GetSecondsMark(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var mark = _deviceManager.Ftm2M.Settings.SecondsMark;
            var result = new ExternalSecondsMessage()
            {
                Mark = (SecondsMark)mark
            };
            ServerLog($"Get seconds mark");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetSecondsMark(ExternalSecondsMessage request, ServerCallContext context)
        {
            CommandReceived();
            var result = _deviceManager.SetSecondsMark((DspDataModel.Hardware.SecondsMark)request.Mark);
            ServerLog($"Set seconds mark");
            return Task.FromResult(GenerateDefaultResponse(result));
        }

        public override Task<WindowFunctionMessage> GetWindowType(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var type = _deviceManager.Ftm2M.Settings.WindowType;
            var result = new WindowFunctionMessage()
            {
                WindowType = (WindowFunctionType)type
            };
            ServerLog($"Get window type");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetWindowType(WindowFunctionMessage request, ServerCallContext context)
        {
            CommandReceived();
            var result = _deviceManager.SetWindowType((DspDataModel.Hardware.WindowFunctionType)request.WindowType);
            ServerLog($"Set window type");
            return Task.FromResult(GenerateDefaultResponse(result));
        }

        public override Task<FftDelayMessage> GetFftDelay(RoleBandMessage request, ServerCallContext context)
        {
            CommandReceived();
            var device = request.Role == TransmissionPackage.FtmRole.Master ? _deviceManager.Ftm2M : _deviceManager.Ftm2S;
            var settings = request.Band == WorkingBand.Wide 
                ? device.WideSettings as DspDataModel.Hardware.IBandSettings 
                : device.NarrowSettings;
            var result = new FftDelayMessage()
            {
                Settings = request,
                Delay = settings.FftDelay
            };
            ServerLog($"Get fft delay");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetFftDelay(FftDelayMessage request, ServerCallContext context)
        {
            CommandReceived();
            var device = request.Settings.Role == TransmissionPackage.FtmRole.Master ? _deviceManager.Ftm2M : _deviceManager.Ftm2S;
            var settings = request.Settings.Band == WorkingBand.Wide
                ? device.WideSettings as DspDataModel.Hardware.IBandSettings
                : device.NarrowSettings;
            var result = settings.SetFftDelay((short)request.Delay);
            ServerLog($"Set fft delay");
            return Task.FromResult(GenerateDefaultResponse(result));
        }

        public override Task<IsActionEnabledMessage> GetNarrowMeasureStrategy(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var result = new IsActionEnabledMessage()
            {
                IsEnabled = _taskManager.FilterManager.IsNarrowMeasuringEnabled
            };
            ServerLog($"Get narrow measure strategy");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetNarrowMeasureStrategy(IsActionEnabledMessage request, ServerCallContext context)
        {
            CommandReceived();
            _taskManager.FilterManager.IsNarrowMeasuringEnabled = request.IsEnabled;
            ServerLog($"Set narrow measure strategy");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> SetWideMeasureBand(WideMeasureBandMessage request, ServerCallContext context)
        {
            CommandReceived();
            float startFrequencyKhz = (Constants.LowerBandMinFrequencyMhz - 50) * 1000;
            float endFrequencyKhz = (Constants.LowerBandMinFrequencyMhz - 30) * 1000;

            if (request.BandStartMhz != 0 && request.BandEndMhz != 0)
            {
                startFrequencyKhz = request.BandStartMhz > Constants.LowerBandMinFrequencyMhz
                    ? request.BandStartMhz * 1000
                    : Constants.LowerBandMinFrequencyMhz * 1000;
                endFrequencyKhz = request.BandEndMhz < Constants.HigherBandMaxFrequencyMhz
                    ? request.BandEndMhz * 1000
                    : Constants.HigherBandMaxFrequencyMhz * 1000;
            }
                
            _taskManager.FilterManager.WideMeasureRange = new FrequencyRange(startFrequencyKhz, endFrequencyKhz);
            ServerLog($"Set wide measure band");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> SetSignalForMeasuring(MeasureSignalMessage request, ServerCallContext context)
        {
            CommandReceived();
            var result = false;
            var signal = _taskManager.SignalStorage.GetSignal(request.SignalId);
            if (signal != null)
            {
                result = true;
                var measuringTask = new MeasuringTask(signal, _taskManager);
                _taskManager.AddTask(measuringTask);
            }
            ServerLog($"Set signal for measuring");
            return Task.FromResult(GenerateDefaultResponse(result));
        }

        public override Task<IntensitySpectrumResponse> GetIntensitySpectrum(SpectrumRequest request, ServerCallContext context)
        {
            CommandReceived();
            var history = _taskManager.HistoryStorage.GetHistory();
            var response = new IntensitySpectrumResponse
            {
                Spectrum = Google.Protobuf.ByteString.CopyFrom(history),
                NumberOfPoints = _taskManager.HistoryStorage.GetNumberOfPointsPerScan()
            };
            ServerLog($"Get intensity spectrum");
            return Task.FromResult(response);
        }

        public override Task<FrequencyRangeCoefficientResponse> GetFrequencyRangeCoefficients(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var response = new FrequencyRangeCoefficientResponse();
            var ranges = Config.Instance.CoefficientSettings.Records.Select(r => new FrequencyRangeCoefficient() 
            {
                StartFrequencyMhz = r.StartFrequencyMhz,
                EndFrequencyMhz = r.EndFrequencyMhz,
                Coefficient = r.Coefficient
            });
            response.Ranges.AddRange(ranges);
            ServerLog($"Get frequency range coefficients");
            return Task.FromResult(response);
        }

        public override Task<DefaultResponse> SetFrequencyRangeCoefficient(FrequencyRangeCoefficient request, ServerCallContext context)
        {
            CommandReceived();
            var result = false;
            if (request.Coefficient < 1)
            {
                ServerLog($"Set frequency range coefficient");
                return Task.FromResult(GenerateDefaultResponse(result));
            }

            var records = Config.Instance.CoefficientSettings.Records;
            for (int i = 0; i < records.Length; i++)
            {
                if (records[i].StartFrequencyMhz == request.StartFrequencyMhz 
                    && records[i].EndFrequencyMhz == request.EndFrequencyMhz)
                {
                    records[i].Coefficient = request.Coefficient;
                    result = true;
                    break;
                }
            }

            ServerLog($"Set frequency range coefficient");
            return Task.FromResult(GenerateDefaultResponse(result));
        }

        public override Task<IsActionEnabledMessage> GetWaterfallStrategy(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var result = new IsActionEnabledMessage()
            {
                IsEnabled = _taskManager.FilterManager.IsWaterfallEnabled
            };
            ServerLog($"Get waterfall strategy");
            return Task.FromResult(result);
        }

        public override Task<DefaultResponse> SetWaterfallStrategy(IsActionEnabledMessage request, ServerCallContext context)
        {
            CommandReceived();
            _taskManager.FilterManager.IsWaterfallEnabled = request.IsEnabled;
            ServerLog($"Set waterfall strategy");
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> StartSeriesMeasuringTask(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            ServerLog($"Starting measuring series task");
            var measuringTask = new SeriesCalculationTask(_taskManager);
            _taskManager.AddTask(measuringTask);
            return Task.FromResult(GenerateDefaultResponse(true));
        }

        public override Task<DefaultResponse> PerformThresholdCalibration(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            var result = _taskManager.DeviceManager.StartNoiseCalibration();
            ServerLog($"Threshold calibration finished");
            return Task.FromResult(GenerateDefaultResponse(result));
        }

        public override Task<DefaultResponse> PerformIqAggregation(DefaultRequest request, ServerCallContext context)
        {
            CommandReceived();
            if (_taskManager.Tasks.Any(t => t is IqAggregationTask))
            {
                MessageLogger.Warning("Another iq gathering task is in process");
                return Task.FromResult(GenerateDefaultResponse(false));
            }

            ServerLog($"Starting iq gathering task");
            var iqTask = new IqAggregationTask(_taskManager);
            _taskManager.AddTask(iqTask);
            return Task.FromResult(GenerateDefaultResponse(true));
        }
    }
}
