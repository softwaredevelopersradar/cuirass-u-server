﻿using DspDataModel.Server;
using TransmissionPackage;

namespace DspServerLibrary
{
    public class DspServer
    {
        private Grpc.Core.Server _server;
        private readonly ProtoServer _service;

        public bool IsWorking { get; private set; }

        public string ServerAddress { get; private set; }

        public int ServerPort { get; private set; }

        public DspServer(string host, int port, IServerController serverController)
        {
            _service = new ProtoServer(serverController);
            ServerAddress = host;
            ServerPort = port;
        }

        public void Start()
        {
            if (IsWorking)
                return;
            _server = new Grpc.Core.Server
            {
                Services = { Transmission.BindService(_service) },
                Ports = { new Grpc.Core.ServerPort(ServerAddress, ServerPort, Grpc.Core.ServerCredentials.Insecure) }
            };
            
            IsWorking = true;
            _server.Start();
        }

        public void Stop()
        {
            if (!IsWorking)
                return;
            IsWorking = false;
            _server.ShutdownAsync().Wait();
        }
    }
}
