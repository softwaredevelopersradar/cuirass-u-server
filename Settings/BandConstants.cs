﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace Settings
{
    /*
     * Every constant here relies on a documents/ConverterRangeSettings.doc
     */
    public static class BandConstants
    {
        private static readonly List<BandRangeSettings> _bandRangeSettings = new List<BandRangeSettings>()
        {
            new BandRangeSettings(
                startBandNumber: 0,
                endBandNumber: 1,
                startFrequencyMhz: 100,
                endFrequencyMhz: 133,
                converterFrequencyMhz: 88,
                bandWidthMhz: 30,
                firstBandFrequencyRangeStartMhz: 100,
                firstBandFrequencyRangeEndMhz: 103,
                firstBandNumberOfSamples: 3),

            new BandRangeSettings(
                startBandNumber: 2,
                endBandNumber: 5,
                startFrequencyMhz: 133,
                endFrequencyMhz: 248,
                converterFrequencyMhz: 143,
                bandWidthMhz: 30,
                firstBandFrequencyRangeStartMhz: 133,
                firstBandFrequencyRangeEndMhz: 158,
                firstBandNumberOfSamples: 24),

            new BandRangeSettings(
                startBandNumber: 6,
                endBandNumber: 7,
                startFrequencyMhz: 248,
                endFrequencyMhz: 293,
                converterFrequencyMhz: 249,
                bandWidthMhz: 30,
                firstBandFrequencyRangeStartMhz: 248,
                firstBandFrequencyRangeEndMhz: 263,
                firstBandNumberOfSamples: 15),

            new BandRangeSettings(
                startBandNumber: 8,
                endBandNumber: 14,
                startFrequencyMhz: 293,
                endFrequencyMhz: 496,
                converterFrequencyMhz: 301,
                bandWidthMhz: 30,
                firstBandFrequencyRangeStartMhz: 293,
                firstBandFrequencyRangeEndMhz: 316,
                firstBandNumberOfSamples: 21),

            new BandRangeSettings(
                startBandNumber: 15,
                endBandNumber: 27,
                startFrequencyMhz: 496,
                endFrequencyMhz: 880,
                converterFrequencyMhz: 505,
                bandWidthMhz: 30,
                firstBandFrequencyRangeStartMhz: 496,
                firstBandFrequencyRangeEndMhz: 520,
                firstBandNumberOfSamples: 22),

            new BandRangeSettings(
                startBandNumber: 28,
                endBandNumber: 28,
                startFrequencyMhz: 900,
                endFrequencyMhz: 1000,
                converterFrequencyMhz: 1000,
                bandWidthMhz: 500,
                firstBandFrequencyRangeStartMhz: 900,
                firstBandFrequencyRangeEndMhz: 1000,
                firstBandNumberOfSamples: 95),

            new BandRangeSettings(
                startBandNumber: 29,
                endBandNumber: 62,
                startFrequencyMhz: 1000,
                endFrequencyMhz: 18_000,
                converterFrequencyMhz: 1250,
                bandWidthMhz: 500,
                firstBandFrequencyRangeStartMhz: 1000,
                firstBandFrequencyRangeEndMhz: 1500,
                firstBandNumberOfSamples: 466),
        };

        //42 - number of inversed bands for now
        private static readonly List<int> _inversedBandNumbers = new List<int>(42);

        static BandConstants() 
        {
            var lowerBands = Enumerable.Range(0, 28);
            _inversedBandNumbers.AddRange(lowerBands);
            _inversedBandNumbers.Add(32);

            _inversedBandNumbers.Add(35);
            _inversedBandNumbers.Add(36);
            _inversedBandNumbers.Add(37);
            _inversedBandNumbers.Add(38);

            _inversedBandNumbers.Add(44);
            _inversedBandNumbers.Add(45);
            _inversedBandNumbers.Add(46);

            _inversedBandNumbers.Add(50);
            _inversedBandNumbers.Add(51);
            _inversedBandNumbers.Add(52);

            _inversedBandNumbers.Add(56);
            _inversedBandNumbers.Add(57);
            _inversedBandNumbers.Add(58);
        }

        public static BandRangeSettings GetBandRangeSettings(int bandNumber) 
        {
            return _bandRangeSettings.First(r => r.StartBandNumber <= bandNumber && bandNumber <= r.EndBandNumber);
        }

        public static BandRangeSettings GetFrequencyRangeSettings(float frequencyMhz)
        {
            return _bandRangeSettings.First(r => r.StartFrequencyMhz <= frequencyMhz && frequencyMhz <= r.EndFrequencyMhz);
        }

        public static bool IsBandInversed(int bandNumber) 
        {
            return _inversedBandNumbers.Contains(bandNumber);
        }

        public static bool IsFrequencyLiesInLowerBand(int frequencyMhz) 
        {
            return frequencyMhz >= Constants.LowerBandMinFrequencyMhz 
                && frequencyMhz <= Constants.LowerBandMaxFrequencyMhz;
        }
    }
}
