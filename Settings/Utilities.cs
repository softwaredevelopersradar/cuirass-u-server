﻿using System;
using System.Diagnostics.Contracts;

namespace Settings
{
    public static class Utilities
    {
        public static int ThreeBytesToInt(byte low, byte middle, byte high)
        {
            var bytes = new byte[] { low, middle, high, 0 };
            return BitConverter.ToInt32(bytes, 0);
        }

        public static int ThreeBytesToInt(byte[] data, int index)
        {
            return ThreeBytesToInt(data[index], data[index + 1], data[index + 2]);
        }

        public static short TwoBytesToShort(byte low, byte high)
        {
            var bytes = new byte[] { low, high };
            return BitConverter.ToInt16(bytes, 0);
        }

        public static short TwoBytesToShort(byte[] data, int index)
        {
            return TwoBytesToShort(data[index], data[index + 1]);
        }

        /// <summary>
        /// Converts integer to three bytes, fourth byte is dropped
        /// </summary>
        public static byte[] IntToThreeBytes(int number)
        {
            var bytes = BitConverter.GetBytes(number);
            return new byte[] { bytes[0], bytes[1], bytes[2], 0 };//dropping last byte
        }


        public static float GetFrequencyKhz(int bandNumber, int sampleNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.ReceiverBandwidthKhz * bandNumber + Constants.SamplesGapKhz * sampleNumber;
        }

        public static int GetBandNumber(float frequencyMhz) 
        {
            Contract.Assert(frequencyMhz >= Constants.FirstBandMinMhz, $"Frequency is {frequencyMhz} MHz, but it can't be less than {Constants.FirstBandMinMhz}");
            var range = BandConstants.GetFrequencyRangeSettings(frequencyMhz);
            if (frequencyMhz <= range.FirstBandFrequencyRangeEndMhz)
                return range.StartBandNumber;

            if (frequencyMhz == range.EndFrequencyMhz)
                return range.EndBandNumber;

            var band = (int)(frequencyMhz - range.FirstBandFrequencyRangeEndMhz) / range.BandWidthMhz + 1;
            return band + range.StartBandNumber;
        }

        public static int GetBandSamplesCount(int receiverSamplesCount)
        {
            return (Constants.ReceiverBandwidthKhz * (receiverSamplesCount - 1) - receiverSamplesCount + 1) / Constants.ReceiverBandwidthKhz + 2;
        }

        public static int GetBandMinFrequencyKhz(int bandNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.ReceiverBandwidthKhz * bandNumber;
        }

        public static int GetBandMaxFrequencyKhz(int bandNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.ReceiverBandwidthKhz * (bandNumber + 1);
        }

        public static int GetSampleNumber(float frequencyKhz, int bandNumber)
        {
            return GetSamplesCount(GetBandMinFrequencyKhz(bandNumber), frequencyKhz);
        }

        /// <summary>
        /// calculates count of samples between two frequencies (for frequencies F and F+sampleGap result is 1)
        /// </summary>
        public static int GetSamplesCount(float startFrequencyKhz, float endFrequencyKhz, float samplesGapKhz = Constants.SamplesGapKhz)
        {
            return (int)((endFrequencyKhz - startFrequencyKhz + samplesGapKhz - 1) / samplesGapKhz);
        }
    }
}
