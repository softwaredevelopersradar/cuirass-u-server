﻿namespace Settings
{
    public struct BandRangeSettings
    {
        public int StartBandNumber { get; }
        public int EndBandNumber { get; }

        public int StartFrequencyMhz { get; }
        public int EndFrequencyMhz { get; }

        public int ConverterFrequencyMhz { get; }
        public int BandWidthMhz { get; }

        /// <summary>
        /// First band, due to an hardware situation always has intersection
        /// with the last band of previous band range (including the first one, since our antennas work range starts from 100 Mhz)
        /// So this values are used to define work ranges of those bands
        /// </summary>
        public int FirstBandFrequencyRangeStartMhz { get; }
        public int FirstBandFrequencyRangeEndMhz { get; }

        /*
         * Lower scans are not consistent with their bandwidth and 
         * can have different number of samples
         */
        public int FirstBandNumberOfSamples { get; }

        public BandRangeSettings(int startBandNumber, int endBandNumber, int startFrequencyMhz, int endFrequencyMhz,
            int converterFrequencyMhz, int bandWidthMhz, int firstBandFrequencyRangeStartMhz, int firstBandFrequencyRangeEndMhz,
            int firstBandNumberOfSamples)
        {
            StartBandNumber = startBandNumber;
            EndBandNumber = endBandNumber;
            StartFrequencyMhz = startFrequencyMhz;
            EndFrequencyMhz = endFrequencyMhz;
            ConverterFrequencyMhz = converterFrequencyMhz;
            BandWidthMhz = bandWidthMhz;

            FirstBandFrequencyRangeStartMhz = firstBandFrequencyRangeStartMhz;
            FirstBandFrequencyRangeEndMhz = firstBandFrequencyRangeEndMhz;

            FirstBandNumberOfSamples = firstBandNumberOfSamples;
        }
    }
}
