﻿namespace Settings
{
    public static class Constants
    {
        public const int ForbiddenRangesCount = 10;
        public const int NarrowFftSampleCount = 512;
        public const int WideBandFftSampleCount = 466;
        public const int MinFrequencyMhz = 1100;
        public const int MinFrequencyKhz = MinFrequencyMhz * 1000;
        public const int MaxFrequencyMhz = 1600;
        public const int MaxFrequencyKhz = MaxFrequencyMhz * 1000;
        public const float WideBandFrequencyStepMhz = (float)(MaxFrequencyMhz - MinFrequencyMhz) / (WideBandFftSampleCount - 1);
        public const float WideBandFrequencyStepKhz = WideBandFrequencyStepMhz * 1000;
        public const float WideBandFrequencyStepHz = WideBandFrequencyStepKhz * 1000;

        public const int ReceiverBandwidthKhz = 500_000;
        public const int FirstBandMinKhz = 100_000; //todo? ??
        public const int FirstBandMinMhz = FirstBandMinKhz / 1000;

        public const int NumberOfBands = 63;//todo : to config?>_>


        /// <summary>
        /// How much samples do receivers really "see" in one band
        /// </summary>
        public const int ReceiverSampleCount = 466;

        public const float SamplesGapKhz = 1f * ReceiverBandwidthKhz / (ReceiverSampleCount - 1);

        /// <summary>
        /// Count of samples in one band that server take from whole receiver's band
        /// </summary>
        public const int BandSampleCount = (ReceiverBandwidthKhz * (ReceiverSampleCount - 1) - ReceiverSampleCount + 1) / ReceiverBandwidthKhz + 2;

        public const int FirstAntennaNumber = 1;
        public const int LastAntennaNumber = 6;

        public const int MinAttenuatorValue = 0;
        public const int MaxAttenuatorValue = 15;

        public const int ConverterCommandPause = 40;

        /// <summary>
        /// After this, bands with numbers higher that this are considered higher (1-18 GHz).
        /// Bands with numbers lower or equal to this are considered lower (100 MHz - 1 GHz)
        /// </summary>
        public const int BandThresholdNumber = 27;
        public const int LowerBandMaxFrequencyMhz = 880;
        public const int LowerBandMinFrequencyMhz = 100;
        public const int HigherBandMaxFrequencyMhz = 18_000;
        public const int HigterBandMinFrequencyMhz = 900;

        public const int LowerBandWideFftStartIndex = 122;
        public const int LowerBandWideFftSampleCount = 29;

        public const int NumberOfSamplesFrom100MhzTo18Ghz= 16_693;
    }
}
