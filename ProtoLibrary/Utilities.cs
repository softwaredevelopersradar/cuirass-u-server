﻿namespace ProtoLibrary
{
    public static class Utilities
    {
        public static DspDataModel.Server.DspServerMode ConvertToDspModel(this TransmissionPackage.DspServerMode args) =>
            (DspDataModel.Server.DspServerMode)args;
        public static TransmissionPackage.DspServerMode ConvertToProto(this DspDataModel.Server.DspServerMode args) =>
            (TransmissionPackage.DspServerMode)args;
    }
}
