﻿using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using HardwareLibrary.Ftm;
using Settings;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TasksLibrary.Tasks
{
    public class MeasuringTask : IFtmTask
    {
        public bool IsEndless => false;

        public int Priority { get; set; }

        public bool IsReady { get; set; }

        public object Result { get; set; }

        public IReadOnlyList<Band> Objectives { get; set; }

        public bool AutoUpdateObjectives => false;

        public event EventHandler TaskEndEvent;

        private readonly ISignal _signalToMeasure;
        private readonly ITaskManager _taskManager;
        private readonly IReceiverManager _receiverManager;

        private readonly Band signalBand;

        public MeasuringTask(ISignal signal, ITaskManager taskManager)
        {
            _signalToMeasure = signal;
            _taskManager = taskManager;
            _receiverManager = taskManager.ReceiverManager;
            Priority = 1;

            var bandNumber = Utilities.GetBandNumber(_signalToMeasure.FrequencyKhz / 1000);
            signalBand = new Band(bandNumber);
        }

        public void Cancel()
        {
            IsReady = true;
        }

        public async Task ProcessTask()
        {
            await _receiverManager.SetConverterBandAsync(signalBand);
            MeasureSignalWithCalculator();
            _taskManager.DatabaseController.AddFrsSignals(_taskManager.SignalStorage.Signals);

            Cancel();
        }

        private void MeasureSignalWithCalculator()
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                var god = new Random();
                var bandwidthMhz = god.Next(1, 4);
                var durationNs = god.Next(100, 100_000);
                var periodNs = god.Next(100, 100_000);
                _signalToMeasure.UpdateCalculatorValues(bandwidthMhz, durationNs, periodNs);
                return;
            }

            _taskManager.ReceiverManager.SetCommutatorAntennas(_signalToMeasure.MasterAntennaIndex, _signalToMeasure.SlaveAntennaIndex, AntennaSwitchSource.None);

            var measuringDevice = _taskManager.DeviceManager.Ftm2M;
            var calculatorSignals = measuringDevice.GetCalculatorSignals(FtmDeviceOutputMode.WideCalculator);
            var signalFrequencyToFtmRangeKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(_signalToMeasure.FrequencyKhz, signalBand);
            _signalToMeasure.TryUpdateCalculatorValues(signalFrequencyToFtmRangeKhz, calculatorSignals);
        }

        public void UpdateObjectives(IEnumerable<Band> newObjectives)
        {
            //not implemented and should not be
        }
    }
}
