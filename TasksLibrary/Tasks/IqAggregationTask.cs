﻿using DspDataModel.Data;
using DspDataModel.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using DspDataModel;
using System.IO;
using DspDataModel.Hardware;

namespace TasksLibrary.Tasks
{
    public class IqAggregationTask : IFtmTask
    {
        private static int FileCounter = 0;

        public bool IsEndless { get; private set; }

        public int Priority => 10_000;

        public bool IsReady { get; private set; }

        public object Result { get; private set; }

        public IReadOnlyList<Band> Objectives { get; private set; }

        public bool AutoUpdateObjectives { get; private set; }

        public event EventHandler TaskEndEvent;

        private readonly IFtmDeviceManager _deviceManager;
        private readonly ITaskManager _taskManager;

        public IqAggregationTask(ITaskManager taskManager) 
        {
            _taskManager = taskManager;
            _deviceManager = taskManager.DeviceManager;
        }

        public void Cancel()
        {}

        public async Task ProcessTask()
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                IsReady = true;
                MessageLogger.Log("I cannot give you iq data in simulation mode, I'm sorry", ConsoleColor.Green);
                return;
            }
            if (Config.Instance.DirectionFindingSettings.IqFilePath.Equals(""))
            {
                IsReady = true;
                MessageLogger.Warning($"Iq file path : \"{Config.Instance.DirectionFindingSettings.IqFilePath}\" is not valid");
                return;
            }

            //setting frequency and antennas 
            var _settings = _taskManager.FilterManager.NarrowVideoSettings;
            if (_settings.IsDefault || _settings.FtmFrequencyKhz <= 0)
            {
                //settings not set, nothing to look at either
                //narrow frequency not set -> no point in performing narrow rdf
                //cancel current task
                IsReady = true;
                return;
            }
            _deviceManager.NarrowSetFrequency(_settings.FtmFrequencyKhz);
            await _taskManager.ReceiverManager.SetConverterBandAsync(new Band(_settings.BandNumber));

            var device = _deviceManager.Ftm2M;
            var measurementSeconds = Config.Instance.DirectionFindingSettings.IqAggregationDurationSec;
            var list = new byte[35_000 * measurementSeconds][];
            var i = 0;
            var watch = Stopwatch.StartNew();
            device.Client.SendReceive(new byte[] { 0x34, 2, 6, 6, 1 });
            device.SetOutputMode(FtmDeviceOutputMode.IqComponents);
            while (watch.Elapsed.TotalSeconds < measurementSeconds)
            {
                var packet = device.DataClient.Receive();
                list[i] = packet;
                i++;
            }
            var numberOfPackets = i;
            watch.Stop();
            MessageLogger.Log("Finished gathering iq scans", ConsoleColor.Green);
            device.SetOutputMode(FtmDeviceOutputMode.NoOutput);
            i = 0;
            try
            {
                var iqArray = new byte[1024];
                for (int j = 0; j < list.Length; j++, i++)
                {
                    if (list[j] == null)
                        continue;
                    if (list[j].Length == 0)
                        continue;

                    if (list[j].Length - 2 != 1024)
                    {
                        MessageLogger.Log($"Scan {j} length is inconsistent");
                        continue;
                    }
                    Array.Copy(list[j], 2, iqArray, 0, 1024);
                    if (list[j].Length >= 1)
                        MessageLogger.Log($"index {j} : length {list[j].Length}");
                    var scan = new IqScan(iqArray);
                    var realPath = Path.Combine(Config.Instance.DirectionFindingSettings.IqFilePath, $"Real{FileCounter}.txt");
                    var imgPath = Path.Combine(Config.Instance.DirectionFindingSettings.IqFilePath, $"Imaginary{FileCounter}.txt");
                    File.AppendAllText(realPath, scan.Real.ShortArrayToString());
                    File.AppendAllText(imgPath, scan.Imaginary.ShortArrayToString());
                }
            }
            catch(Exception e)
            {
                MessageLogger.Log($"Error occured during iq scan task {e}");
            }
            MessageLogger.Log($"Received {numberOfPackets} packets, {watch.Elapsed.TotalMilliseconds} ms spent for them", ConsoleColor.Green);
            FileCounter++;
            IsReady = true;
        }

        public void UpdateObjectives(IEnumerable<Band> newObjectives)
        {}
    }
}
