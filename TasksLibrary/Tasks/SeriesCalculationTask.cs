﻿using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TasksLibrary.Tasks
{
    public class SeriesCalculationTask : IFtmTask
    {
        public bool IsEndless { get; private set; }

        public int Priority { get; private set; }

        public bool IsReady { get; private set; }

        public object Result { get; private set; }

        public IReadOnlyList<Band> Objectives { get; private set; }

        public bool AutoUpdateObjectives { get; private set; }

        public event EventHandler TaskEndEvent;

        private readonly ITaskManager _taskManager;
        private readonly IReceiverManager _receiverManager;
        private readonly IFtmDeviceManager _deviceManager;
        private const int _numberOfScansToMeasure = 5000; // todo : to constants?
        private const int _threshold = 60;
        private const float _averageScanReceiveTimeMs = 1.2f; 
        private Band _narrowRdfBand;
        private NarrowVideoClarificationSetting _settings;
        private int _frequencyIndex = 256;
        private const int _frequencyIndexDelta = 2;

        public SeriesCalculationTask(ITaskManager taskManager) 
        {
            _taskManager = taskManager;
            _receiverManager = _taskManager.ReceiverManager;
            _deviceManager = _taskManager.DeviceManager;

            Priority = 1_000;
        }

        public void Cancel()
        {
            //atomic task cannot be canceled
        }

        public async Task ProcessTask()
        {
            IsReady = true; // this task is atomic, cannot be canceled
            try 
            {
                var setupResult = await Setup();
                if (!setupResult) return;
                var seriesData = AggregateScans();
                CalculateSeriesPeriodAndDuration(seriesData);
            }
            catch (Exception e)
            {
                MessageLogger.Warning($"Failed to measure series data for {_settings.FrequencyKhz} KHz\r\n" +
                    $"Exception {e}, stack {e.StackTrace}");
            }
        }

        private async Task<bool> Setup() 
        {
            _settings = _taskManager.FilterManager.NarrowVideoSettings;
            _narrowRdfBand = new Band(_settings.BandNumber);
            _deviceManager.NarrowSetFrequency(_settings.FtmFrequencyKhz);
            var (MasterIndex, SlaveIndex) = _taskManager.FilterManager.AntennaSettings.Pairs.First();
            _receiverManager.SetCommutatorAntennas(MasterIndex, SlaveIndex, AntennaSwitchSource.None);
            return await _receiverManager.SetConverterBandAsync(_narrowRdfBand);
        }

        private byte[] AggregateScans() 
        {
            var output = new byte[_numberOfScansToMeasure];
            var start = _frequencyIndex - _frequencyIndexDelta;
            var end = _frequencyIndex + _frequencyIndexDelta;
            for (int i = 0; i < _numberOfScansToMeasure; i++)
            {
                var scan = _deviceManager.Ftm2M.NarrowReadFft();
                var scanPeak = GetScanPeak(scan, start, end);
                output[i] = scanPeak;
            }

            return output;
        }

        private byte GetScanPeak(byte[] scan, int signalIndexRangeStart, int signalIndexRangeEnd) 
        {
            var possibleMaxPeaks = new List<byte>();
            for (int i = signalIndexRangeStart; i <= signalIndexRangeEnd; i++)
            {
                possibleMaxPeaks.Add(scan[i]);
            }
            return possibleMaxPeaks.Max();
        }

        private void CalculateSeriesPeriodAndDuration(byte[] data) 
        {
            var peaks = new List<Peak>(100);
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] < _threshold) continue;
                var start = i;
                while (i < data.Length && data[i] >= _threshold)
                {
                    i++;
                }
                var end = i;
                if (i == data.Length)
                    end -= 1;
                var peak = new Peak
                {
                    Start = start,
                    End = end
                };
                peaks.Add(peak);
            }
            if (peaks.Count == 0)
            {
                MessageLogger.Warning("0 peaks were found");
                return;
            }
            var periods = new List<int>(peaks.Count);
            for (int i = 0; i < peaks.Count - 1; i++)
            {
                periods.Add(peaks[i + 1].Start - peaks[i].Start);
            }
            var (MasterIndex, SlaveIndex) = _taskManager.FilterManager.AntennaSettings.Pairs.First();
            MessageLogger.Log($"Finished calculation of period and duration of series at pairs {MasterIndex}-{SlaveIndex}, {_taskManager.FilterManager.NarrowVideoSettings.FrequencyKhz} KHz, peaks :");
            foreach (var peak in peaks)
            {
                MessageLogger.Log($"Indices {peak.Start} - {peak.End}, number of peaks - {peak.NumberOfPeaks}, duration - {peak.DurationMs}");
            }
            if(periods.Count > 0)
                MessageLogger.Log($"Average period is {periods.Average()}");
            for (int i = 0; i < periods.Count; i++)
            {
                MessageLogger.Log($"Period {i} is {periods[i]}");
            }
        }

        public void UpdateObjectives(IEnumerable<Band> newObjectives)
        {
            //atomic task has no objectives
        }

        private struct Peak 
        {
            public int Start { get; set; }
            public int End { get; set; }
            public int NumberOfPeaks => End - Start + 1;
            public float DurationMs => (NumberOfPeaks - 1) * _averageScanReceiveTimeMs;
        }
    }
}
