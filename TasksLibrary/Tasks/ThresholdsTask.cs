﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataProcessorLibrary;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using Settings;

namespace TasksLibrary.Tasks
{
    public class ThresholdsTask : IFtmTask
    {
        public bool IsReady { get; private set; }
        public int Priority => 200;

        public bool IsEndless => throw new NotImplementedException();

        public object Result => throw new NotImplementedException();

        public IReadOnlyList<Band> Objectives => throw new NotImplementedException();

        public bool AutoUpdateObjectives => throw new NotImplementedException();

        private readonly IFtmDeviceManager _manager;
        private readonly int _scansCount;

        public event EventHandler TaskEndEvent;

        public ThresholdsTask(IFtmDeviceManager manager, int scansCount)
        {
            _manager = manager;
            _scansCount = scansCount;
        }

        public async Task ProcessTask()
        {
            var watch = new System.Diagnostics.Stopwatch();//todo : remove
            watch.Start();
            var masterDeviation = DataProcessor.GetStandardDeviation(
                    DataAggregator.GetFftDataVolts(_manager.Ftm2M.WideReadFft, _scansCount).ToList());
            var slaveDeviation = DataProcessor.GetStandardDeviation(
                DataAggregator.GetFftDataVolts(_manager.Ftm2S.WideReadFft, _scansCount).ToList());
            
            var deviceProcessTime = 0.001 * 2 * _scansCount;//1000ns = 1ms = 0.001s
            var threshold = Math.Sqrt(2 * Math.Log(Constants.WideBandFrequencyStepHz * deviceProcessTime)) * Math.Max(masterDeviation, slaveDeviation) * 3;
            watch.Stop();
            MessageLogger.Warning($"Our computed threshold : {threshold}, byte {threshold.ToByteAmplitude():F3} for time {watch.ElapsedMilliseconds}ms");
            
            double m, s;//lift m or s levels to the other one
            if (masterDeviation > slaveDeviation)
            {
                m = 1;
                s = masterDeviation / slaveDeviation;
            }
            else
            {
                m = slaveDeviation / masterDeviation;
                s = 1;
            }

            var masterScans = DataAggregator.GetFftDataVolts(_manager.Ftm2M.WideReadFft, _scansCount);
            var slaveScans = DataAggregator.GetFftDataVolts(_manager.Ftm2S.WideReadFft, _scansCount);

            
            foreach (var scan in masterScans)
            {
                for (int i = 0; i < scan.Amplitudes.Length; i++)
                    scan.Amplitudes[i] *= m;
            }
            foreach (var scan in slaveScans)
            {
                for (int i = 0; i < scan.Amplitudes.Length; i++)
                    scan.Amplitudes[i] *= s;
            }

            if (masterScans.Any(
                scan => scan.Amplitudes.Any(
                    element => element > threshold)) ||
                slaveScans.Any(
                    scan => scan.Amplitudes.Any(
                        element => element > threshold)))
                MessageLogger.Warning("threshold check failed");
            else
            {
                MessageLogger.Log("threshold check succeeded");
            }
            _manager.Threshold = threshold;
            _manager.SlaveAntennaCoefficient = s;
            _manager.MasterAntennaCoefficient = m;
            MessageLogger.Log($"m : {m}, s :{s}");
            IsReady = true;
        }

        public void Cancel()
        {
            throw new NotImplementedException();
        }

        public void UpdateObjectives(IEnumerable<Band> newObjectives)
        {
            throw new NotImplementedException();
        }
    }
}
