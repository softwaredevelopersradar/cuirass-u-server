﻿using DataProcessorLibrary;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using HardwareLibrary.Ftm;
using Settings;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TasksLibrary.Tasks
{
    public class WideRdfTask : RdfTask
    {
        public BandRangeSettings BandSettings { get; private set; }

        private bool IsHistoryGathered = false;
        private int WaterfallBandNumber;

        public WideRdfTask(ITaskManager taskManager, IEnumerable<Band> objectives, bool isEndless = false,
               bool autoUpdateObjectives = true, int priority = 1)
            : base(taskManager, objectives, isEndless, autoUpdateObjectives, priority)
        {
            BandSettings = BandConstants.GetBandRangeSettings(Objectives[BandIndex].Number);
            _antennaSwitchSource = AntennaSwitchSource.RdfSpectrumTask;

            _taskManager.WideBandSpectrumStorage.Clear();
            _taskManager.HistoryStorage.Clear();
        }

        protected override Task<bool> SetupTaskIteration()
        {
            WaterfallBandNumber = _taskManager.FilterManager.NarrowVideoSettings.BandNumber;
            return _receiverManager.SetConverterBandAsync(Objectives[BandIndex]);
        }

        protected override IEnumerable<(int MasterIndex, int SlaveIndex)> GetPairs()
        {
            return _taskManager.FilterManager.GetRdfSettings();
        }

        protected override IEnumerable<ISignal> PerformRdfCycle()
        {
            byte[] getMasterData() => GetWidebandProcessedScan(_deviceManager.Ftm2M.WideReadFft);
            byte[] getSlaveData() => GetWidebandProcessedScan(_deviceManager.Ftm2S.WideReadFft);

            var masterScanTask = Task.Run(() => GetScans(getMasterData, FtmRole.Master));
            var slaveScanTask = Task.Run(() => GetScans(getSlaveData, FtmRole.Slave));
            Task.WaitAll(masterScanTask, slaveScanTask);
            var mData = masterScanTask.Result;
            var sData = slaveScanTask.Result;

            var band = Objectives[BandIndex];
            var startFrequencyKhz = band.FrequencyFromMhz * 1000;
            var stepKhz = Constants.WideBandFrequencyStepKhz;

            return DataProcessor.GetSignalsFromScans(mData, sData, startFrequencyKhz, stepKhz, _deviceManager.Threshold,
                _receiverManager.MasterReceiver.AntennaCommutator.WorkingAntennaIndex, _receiverManager.SlaveReceiver.AntennaCommutator.WorkingAntennaIndex);
        }
        private List<FftVoltScan> GetScans(Func<byte[]> getFtmData, FtmRole role)
        {
            var output = new List<FftVoltScan>(_scansCount);
            for (int i = 0; i < _scansCount; i++)
            {
                if (i % 2 == 0)
                {
                    IsHistoryGathered = false;
                }
                var byteScan = getFtmData();
                if (Objectives[BandIndex].Inversed)
                    Array.Reverse(byteScan);
                PutInWidebandStorage(byteScan, role);
                output.Add(new FftVoltScan(byteScan));
            }
            return output;
        }

        private byte[] GetWidebandProcessedScan(Func<byte[]> getFtmData)
        {
            var byteScan = getFtmData();
            return ProcessWidebandFirstBand(byteScan);
        }

        private byte[] ProcessWidebandFirstBand(byte[] byteScan)
        {
            var band = Objectives[BandIndex];
            if (band.Number == BandSettings.StartBandNumber)
            {
                var output = new byte[BandSettings.FirstBandNumberOfSamples];
                var startIndex = band.IsLowerBand ? Constants.LowerBandWideFftStartIndex : 0;
                Array.Copy(byteScan, startIndex, output, 0, BandSettings.FirstBandNumberOfSamples);
                return output;
            }
            if (band.IsLowerBand)
            {
                var output = new byte[Constants.LowerBandWideFftSampleCount];
                Array.Copy(byteScan, Constants.LowerBandWideFftStartIndex, output, 0, Constants.LowerBandWideFftSampleCount);
                return output;
            }
            return byteScan;
        }

        private void PutInWidebandStorage(byte[] scan, FtmRole role)
        {
            var bandNumber = Objectives[BandIndex].Number;
            var ftmScan = new FtmScan(scan, bandNumber);
            _taskManager.WideBandSpectrumStorage.Put(ftmScan, bandNumber, role);
            if (role == FtmRole.Master 
                && _taskManager.FilterManager.IsWaterfallEnabled 
                && bandNumber == WaterfallBandNumber && !IsHistoryGathered)
            {
                _taskManager.HistoryStorage.AddScan(scan);
                IsHistoryGathered = true;
            }
        }

        protected override void PerformMeasuring(IReadOnlyList<ISignal> signals)
        {
            var measureRange = _taskManager.FilterManager.WideMeasureRange;
            foreach (var signal in signals)
            {
                if (measureRange.Contains(signal.FrequencyKhz))
                    MeasureSignalWithCalculator(signal);
            }
        }

        //todo : refactor with wide one

        private void MeasureSignalWithCalculator(ISignal signal)
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                var god = new Random();
                var bandwidthMhz = god.Next(1, 4);
                var durationNs = god.Next(100, 100_000);
                var periodNs = god.Next(100, 100_000);
                signal.UpdateCalculatorValues(bandwidthMhz, durationNs, periodNs);
                return;
            }

            _taskManager.ReceiverManager.SetCommutatorAntennas(signal.MasterAntennaIndex, signal.SlaveAntennaIndex, _antennaSwitchSource);

            var measuringDevice = _deviceManager.Ftm2M;
            var calculatorSignals = measuringDevice.GetCalculatorSignals(FtmDeviceOutputMode.WideCalculator);
            var signalFrequencyToFtmRangeKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(signal.FrequencyKhz, Objectives[BandIndex]);
            signal.TryUpdateCalculatorValues(signalFrequencyToFtmRangeKhz, calculatorSignals);
        }

        protected override void FinishTaskIteration()
        {
            // We do not update band number when waterfall is enabled to exterminate band changes during waterfall
            // Thats because we implemented only one band waterfall
            if (_taskManager.FilterManager.IsWaterfallEnabled && Objectives[BandIndex].Number == WaterfallBandNumber)
            {
                return;
            }

            BandIndex++;
            if (BandIndex == Objectives.Count)
            {
                BandIndex = 0;
                if (!IsEndless)
                    Cancel();
            }
            BandSettings = BandConstants.GetBandRangeSettings(Objectives[BandIndex].Number);
        }
    }
}
