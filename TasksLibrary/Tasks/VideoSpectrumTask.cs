﻿using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TasksLibrary.Tasks
{
    public class VideoSpectrumTask : IFtmTask
    {
        public bool IsEndless { get; private set; }

        public int Priority { get; private set; }

        public bool IsReady { get; private set; }

        public object Result { get; private set; }

        public IReadOnlyList<Band> Objectives { get; private set; }

        public bool AutoUpdateObjectives { get; private set; }

        public event EventHandler TaskEndEvent;

        private readonly ITaskManager _taskManager;
        private readonly IFtmDeviceManager _deviceManager;
        private readonly IReceiverManager _receiverManager;

        public VideoSpectrumTask(ITaskManager taskManager, bool isEndless = false, int priority = 1)
        {
            _taskManager = taskManager;
            _deviceManager = _taskManager.DeviceManager;
            _receiverManager = _taskManager.ReceiverManager;
            IsEndless = isEndless;
            Priority = priority;
        }

        public async Task ProcessTask()
        {
            if (_deviceManager.WorkingBand == VideoWorkingBand.Off)
            {
                _deviceManager.SetVideoWorkingBand(VideoWorkingBand.Wide);
            }

            var videoSettings = _taskManager.FilterManager.NarrowVideoSettings;
            if (videoSettings.IsDefault)
            {
                //settings not set, nothing to look at
                return;
            }
            if (videoSettings.FtmFrequencyKhz == 0 && _deviceManager.WorkingBand == VideoWorkingBand.Narrow)
            {
                //Narrow band frequency not set, no point in getting narrow video spectrum
                await Task.Delay(10);
                return;
            }

            var band = new Band(videoSettings.BandNumber);
            _deviceManager.NarrowSetFrequency(videoSettings.FtmFrequencyKhz);
            _receiverManager.SetConverterBand(band);
            foreach (var (MasterIndex, SlaveIndex) in _taskManager.FilterManager.AntennaSettings.Pairs)
            {
                _receiverManager.SetCommutatorAntennas(MasterIndex, SlaveIndex, AntennaSwitchSource.VideoSpectrumTask);

                GetInfo(_deviceManager.Ftm2M, FtmRole.Master);
                GetInfo(_deviceManager.Ftm2S, FtmRole.Slave);
            }

            if (IsEndless)
                return;
            else
                IsReady = true;

            void GetInfo(IFtmDevice device, FtmRole role)
            {
                for (int i = 0; i < 5; i++)
                {
                    var videoScan = device.VideoSpectrumRead(role);
                    _taskManager.SecondarySpectrumStorage.PutVideo(new VideoScan(videoScan), role);
                }
            }
        }

        public void Cancel()
        {
            IsReady = true;
        }

        public void UpdateObjectives(IEnumerable<Band> newObjectives)
        {}
    }
}
