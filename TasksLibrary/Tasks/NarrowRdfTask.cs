﻿using DataProcessorLibrary;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using HardwareLibrary.Ftm;
using Settings;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TasksLibrary.Tasks
{
    public class NarrowRdfTask : RdfTask
    {
        private Band _narrowRdfBand;
        private NarrowVideoClarificationSetting _settings;

        public NarrowRdfTask(ITaskManager taskManager, bool isEndless = false, int priority = 1)
            : base(taskManager, new List<Band> { new Band(taskManager.FilterManager.NarrowVideoSettings.BandNumber) }, isEndless, false, priority)
        {
            _antennaSwitchSource = AntennaSwitchSource.NarrowSpectrumTask;
            _taskManager.SecondarySpectrumStorage.NarrowSpectrumStorage.Clear();

            InitializeSettings();
        }

        private void InitializeSettings()
        {
            _settings = _taskManager.FilterManager.NarrowVideoSettings;
            _narrowRdfBand = Objectives[BandIndex];
            ValidateNarrowSettings();
        }

        private void ValidateNarrowSettings()
        {
            if (_settings.IsDefault || _settings.FtmFrequencyKhz <= 0)
            {
                //settings not set, nothing to look at either
                //narrow frequency not set -> no point in performing narrow rdf
                //cancel current task
                Cancel();
            }
        }

        protected override async Task<bool> SetupTaskIteration()
        {
            InitializeSettings(); // because between iterations settings can change
            _deviceManager.NarrowSetFrequency(_settings.FtmFrequencyKhz);
            return await _receiverManager.SetConverterBandAsync(_narrowRdfBand);
        }

        protected override IEnumerable<(int MasterIndex, int SlaveIndex)> GetPairs()
        {
            return _taskManager.FilterManager.AntennaSettings.Pairs;
        }

        protected override IEnumerable<ISignal> PerformRdfCycle()
        {
            var masterScanTask = Task.Run(() => GetScans(_deviceManager.Ftm2M.NarrowReadFft, FtmRole.Master));
            var slaveScanTask = Task.Run(() => GetScans(_deviceManager.Ftm2S.NarrowReadFft, FtmRole.Slave));
            Task.WaitAll(masterScanTask, slaveScanTask);
            var mData = masterScanTask.Result;
            var sData = slaveScanTask.Result;

            var startFrequencyKhz = _settings.FrequencyKhz - _taskManager.DeviceManager.NarrowBandWidthKhz.NarrowFilterWidthToMhz() * 1_000 / 2;
            var stepKhz = _taskManager.DeviceManager.NarrowBandWidthKhz.NarrowFilterWidthToMhz() * 1_000 / (Constants.NarrowFftSampleCount - 1);

            return DataProcessor.GetSignalsFromScans(mData, sData, startFrequencyKhz, stepKhz, _deviceManager.Threshold,
                _receiverManager.MasterReceiver.AntennaCommutator.WorkingAntennaIndex, _receiverManager.SlaveReceiver.AntennaCommutator.WorkingAntennaIndex);
        }

        private List<FftVoltScan> GetScans(Func<byte[]> getFtmData, FtmRole role)
        {
            var output = new List<FftVoltScan>(_scansCount);
            for (int i = 0; i < _scansCount; i++)
            {
                var byteScan = getFtmData();
                if (_narrowRdfBand.Inversed)
                    Array.Reverse(byteScan);
                PutInStorage(byteScan, role);
                output.Add(new FftVoltScan(byteScan));
            }
            return output;
        }

        private void PutInStorage(byte[] scan, FtmRole role)
        {
            var ftmScan = new FtmScan(scan, _narrowRdfBand.Number);
            _taskManager.SecondarySpectrumStorage.NarrowSpectrumStorage.Put(ftmScan, role);
        }

        protected override void PerformMeasuring(IReadOnlyList<ISignal> signals)
        {
            if (!_taskManager.FilterManager.IsNarrowMeasuringEnabled)
                return;

            foreach (var signal in signals)
            {
                MeasureSignalWithCalculator(signal);
            }
        }

        //todo : refactor with wide one
        private void MeasureSignalWithCalculator(ISignal signal)
        {
            if (Config.Instance.HardwareSettings.IsSimulation)
            {
                var god = new Random();
                var bandwidthMhz = god.Next(1, 4);
                var durationNs = god.Next(100, 100_000);
                var periodNs = god.Next(100, 100_000);
                signal.UpdateCalculatorValues(bandwidthMhz, durationNs, periodNs);
                return;
            }

            _taskManager.ReceiverManager.SetCommutatorAntennas(signal.MasterAntennaIndex, signal.SlaveAntennaIndex, _antennaSwitchSource);

            var measuringDevice = _deviceManager.Ftm2M;
            var calculatorSignals = measuringDevice.GetCalculatorSignals(FtmDeviceOutputMode.NarrowCalculator);
            var signalFrequencyToFtmRangeKhz = NarrowFrequencyCalculator.GetFtmNarrowFrequency(signal.FrequencyKhz, _narrowRdfBand);
            signal.TryUpdateCalculatorValues(signalFrequencyToFtmRangeKhz, calculatorSignals);
        }

        protected override void FinishTaskIteration()
        {
            //nothing happens here, we are just a narrow rdf task with only one objective
        }
    }
}
