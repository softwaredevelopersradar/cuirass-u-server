﻿using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Tasks;
using DspDataModel.Hardware;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TasksLibrary.Tasks
{
    public abstract class RdfTask : IFtmTask
    {
        public bool IsEndless { get; private set; }

        public int Priority { get; private set; }

        public bool IsReady { get; private set; }

        public object Result { get; private set; }

        public IReadOnlyList<Band> Objectives { get; private set; }

        public int BandIndex { get; protected set; }

        public bool AutoUpdateObjectives { get; private set; }

        protected AntennaSwitchSource _antennaSwitchSource;

        public event EventHandler TaskEndEvent;

        protected readonly ITaskManager _taskManager;
        protected readonly IReceiverManager _receiverManager;
        protected readonly IFtmDeviceManager _deviceManager;
        protected readonly int _scansCount;

        public RdfTask(ITaskManager taskManager, IEnumerable<Band> objectives, bool isEndless = false,
            bool autoUpdateObjectives = true, int priority = 1)
        {
            _taskManager = taskManager;
            _receiverManager = _taskManager.ReceiverManager;
            _deviceManager = _taskManager.DeviceManager;
            _scansCount = Config.Instance.DirectionFindingSettings.NumberOfRdfScans;

            if (objectives.Count() == 0 || objectives == null)
            {
                IsReady = true;
                return;
            }
            Objectives = objectives.ToList();

            IsEndless = isEndless;
            Priority = priority;
            AutoUpdateObjectives = autoUpdateObjectives;
            _antennaSwitchSource = AntennaSwitchSource.None;
        }

        public async Task ProcessTask()
        {
            await SetupTaskIteration();
            var signals = await PerformAntennaCycle();
            PerformMeasuring(signals);
            PutSignalsInStorages(signals);
            FinishTaskIteration();
        }

        protected abstract Task<bool> SetupTaskIteration();

        protected async Task<List<ISignal>> PerformAntennaCycle()
        {
            var cycleSignals = new List<ISignal>();
            var pairs = GetPairs();
            foreach (var (MasterIndex, SlaveIndex) in pairs)
            {
                _receiverManager.SetCommutatorAntennas(MasterIndex, SlaveIndex, _antennaSwitchSource);
                var newSignals = PerformRdfCycle();
                ProcessNewSignals(newSignals);
            }
            //we wait this pause for correct converter behavior
            await Task.Delay(Config.Instance.DirectionFindingSettings.PreConverterPauseTimeMs);
            return cycleSignals;

            void ProcessNewSignals(IEnumerable<ISignal> newSignals)
            {
                foreach (var signal in newSignals)
                {
                    if (IsSignalInForbiddenRange(signal)) continue;
                    if (!IsSignalInIntelligenceSectors(signal)) continue;

                    var previousSignal = cycleSignals.Where(s => s.AreSameSignals(signal));
                    //signal is unique, therefore it is added to the cycle list as it is
                    if (previousSignal.Count() == 0)
                    {
                        cycleSignals.Add(signal);
                        continue;
                    }

                    //a similar signal already exists in the cycle list, check whether we should update it 
                    var previous = previousSignal.First();
                    if (previous.Amplitude < signal.Amplitude)
                    {
                        cycleSignals.Remove(previous);
                        cycleSignals.Add(signal);
                    }
                }
            }

            bool IsSignalInForbiddenRange(ISignal signal) 
            {
                return _taskManager.FilterManager.ForbiddenRanges.Any(r => r.Contains(signal.FrequencyKhz));
            }

            bool IsSignalInIntelligenceSectors(ISignal signal) 
            {
                return _taskManager.FilterManager.IntelligenceSectors.Count == 0
                    || _taskManager.FilterManager.IntelligenceSectors.Any(r => r.Contains(signal.Direction));
            }
        }

        protected abstract IEnumerable<(int MasterIndex, int SlaveIndex)> GetPairs();

        protected abstract IEnumerable<ISignal> PerformRdfCycle();

        protected abstract void PerformMeasuring(IReadOnlyList<ISignal> signals);
        protected virtual void PutSignalsInStorages(List<ISignal> signals)
        {
            if (signals.Count == 0)
                return;
            _taskManager.SignalStorage.Put(signals);
            _taskManager.DatabaseController.AddFrsSignals(_taskManager.SignalStorage.Signals);
        }

        protected abstract void FinishTaskIteration();

        public void Cancel()
        {
            IsReady = true;
        }

        //todo :!
        public void UpdateObjectives(IEnumerable<Band> newObjectives)
        {
            Objectives = newObjectives.ToList();
            BandIndex = 0;
        }
    }
}
