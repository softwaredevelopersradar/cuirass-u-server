﻿using DspDataModel.Data;
using DspDataModel.Tasks;
using System.Threading.Tasks;
using System.Collections.Generic;
using TasksLibrary.Tasks;

namespace TasksLibrary.Modes
{
    public class RdfMode : IModeController
    {
        private IEnumerable<IFtmTask> _modeTasks;
        public ITaskManager TaskManager { get; }

        public RdfMode(ITaskManager taskManager) 
        {
            TaskManager = taskManager;
        }

        public IEnumerable<IFtmTask> CreateTasks()
        {
            var bands = FrequencyRange.SplitInBands(TaskManager.FilterManager.IntelligenceRanges);
            _modeTasks = new[] { new WideRdfTask(TaskManager, bands, true, true, 10) };
            return _modeTasks;
        }

        public async Task Initialize()
        {
            await Task.Delay(1);
        }

        public void CancelTasks()
        {
            if (_modeTasks == null)
                return;

            foreach (var task in _modeTasks)
                task.Cancel();
        }
    }
}
