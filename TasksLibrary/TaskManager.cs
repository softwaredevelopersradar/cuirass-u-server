﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Database;
using DspDataModel.Hardware;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using DspDataModel.Data;
using Nito.AsyncEx;
using HardwareLibrary.Receivers;
using Settings;

namespace TasksLibrary
{
    public class TaskManager : ITaskManager
    {
        public IFtmDeviceManager DeviceManager { get; private set; }
        public IReceiverManager ReceiverManager { get; private set; }
        public ISignalStorage SignalStorage { get; private set; }
        public IModeController CurrentMode { get; private set; }
        public IFilterManager FilterManager { get; private set; }
        public IDatabaseController DatabaseController { get; private set; }
        public IMultiScanStorage<FtmScan> WideBandSpectrumStorage { get; private set; }
        public ISecondarySpectrumStorage SecondarySpectrumStorage { get; private set; }
        public IHistoryStorage HistoryStorage { get; private set; }

        public IReadOnlyList<IFtmTask> Tasks => _tasks;

        private readonly List<IFtmTask> _tasks;
        private readonly AsyncAutoResetEvent _taskAddEvent;

        private readonly object _lockObject = new object();

        public TaskManager(IFtmDeviceManager manager, IDatabaseController databaseController)
        {
            _taskAddEvent = new AsyncAutoResetEvent();

            DeviceManager = manager;
            DatabaseController = databaseController;
            SignalStorage = new DataStorages.SignalStorage();
            FilterManager = new DataStorages.FilterManager();
            WideBandSpectrumStorage = new DataStorages.MultiScanStorage<FtmScan>(Constants.NumberOfBands);
            SecondarySpectrumStorage = new DataStorages.SecondarySpectrumStorage();
            HistoryStorage = new DataStorages.HistoryStorage();
            ReceiverManager = new ReceiverManager();
            _tasks = new List<IFtmTask>();

            SubscribeUtilityToEvents();
            Task.Run(WorkCircuit);
        }

        private void SubscribeUtilityToEvents()
        {
            DatabaseController.OnReconFrequenciesUpdated += DatabaseController_OnReconFrequenciesUpdated;
            DatabaseController.OnForbiddenFrequenciesUpdated += DatabaseController_OnForbiddenFrequenciesUpdated;
            DatabaseController.OnSectorsUpdated += DatabaseController_OnSectorsUpdated;
            FilterManager.IntelligenceRangesUpdatedEvent += FilterManager_IntelligenceRangesUpdatedEvent;
        }

        private void DatabaseController_OnSectorsUpdated(object sender, IEnumerable<DirectionSector> e)
        {
            FilterManager.SetIntelligenceSectors(e);
        }

        private void FilterManager_IntelligenceRangesUpdatedEvent(object sender, IReadOnlyList<FrequencyRange> e)
        {
            var newObjectives = FrequencyRange.SplitInBands(FilterManager.IntelligenceRanges);
            lock (_tasks) 
            {
                foreach (var task in _tasks)
                {
                    if (task.AutoUpdateObjectives)
                        task.UpdateObjectives(newObjectives);
                }
            }
        }

        private void DatabaseController_OnForbiddenFrequenciesUpdated(object sender, IEnumerable<FrequencyRange> e)
        {
            FilterManager.SetForbiddenRanges(e);
        }

        private void DatabaseController_OnReconFrequenciesUpdated(object sender, IEnumerable<FrequencyRange> e)
        {
            FilterManager.SetIntelligenceRanges(e);
        }

        public bool Initialize()
        {
            return DeviceManager.Initialize() && ReceiverManager.Initialize();
        }

        public void SetMode(IModeController modeController)
        {
            lock (_lockObject)
            {
                //todo : REFACTOR TASKSSKSKSKS
                //CurrentMode.CancelTasks();
                CurrentMode = modeController;
                var newTasks = modeController.CreateTasks();
                foreach (var task in newTasks)
                {
                    AddTask(task);
                }
            }
        }

        public void AddTask(IFtmTask task)
        {
            if (task.IsReady)
            {
                return;
            }
            lock (_tasks)
            {
                _tasks.Add(task);
                _tasks.Sort((t1, t2) => t2.Priority.CompareTo(t1.Priority));
                _taskAddEvent.Set();
            }
        }

        public void AddTasks(IEnumerable<IFtmTask> tasks)
        {
            lock (_tasks)
            {
                _tasks.AddRange(tasks.Where(t => !t.IsReady));
                _tasks.Sort((t1, t2) => t2.Priority.CompareTo(t1.Priority));
                _taskAddEvent.Set();
            }
        }

        public void RemoveTask(IFtmTask task)
        {
            lock (_tasks)
            {
                if (!_tasks.Contains(task))
                {
                    return;
                }
                _tasks.Remove(task);
            }
        }

        public void ClearTasks()
        {
            lock (_tasks)
            {
                foreach (var task in Tasks)
                {
                    task.Cancel();
                }
                _tasks.Clear();
            }
        }

        private async Task WorkCircuit()
        {
            try
            {
                while (true)
                {
                    IFtmTask[] localTasks;
                    lock (_tasks)
                    {
                        localTasks = Tasks.ToArray();
                    }
                    if (localTasks.Length == 0)
                    {
                        await _taskAddEvent.WaitAsync();
                        continue;
                    }
                    foreach (var task in localTasks)
                    {
                        await task.ProcessTask();
                    }
                    RemoveCompletedTasks(localTasks);
                }
            }
            catch(Exception e)
            {
                MessageLogger.Error($"Work cicruit exception : {e}");
            }
        }

        private void RemoveCompletedTasks(IFtmTask[] localTasks)
        {
            foreach (var task in localTasks)
            {
                if (task.IsReady)
                {
                    RemoveTask(task);
                }
            }
        }
    }
}
