﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using FtmDeviceMessages;
using HardwareLibrary.Ftm;
using Settings;
using TasksLibrary.Modes;
using TasksLibrary.Tasks;
using TasksLibrary;
using DspDataModel.Data;
using DatabaseLibrary;

namespace CuirassTestApp
{
    class Program
    {
        private static bool RespondToAnalyzer = false;
        private static bool IsRdfRunning = false;
        private static bool IsIqRunning = false;

        private static readonly object _lockObject = new object();

        private static ITaskManager _taskManager;
        private static FtmDeviceManager _deviceManager;

        private static Dictionary<int, CalculatorResponseMessage> _calculatorResponseMessages;

        private static UdpClient _analyzerServer;

        static void Main(string[] args)
        {
            MessageLogger.SetLogger(new NLogLogger());

            _deviceManager = new FtmDeviceManager();
            _taskManager = new TaskManager(_deviceManager, new DatabaseMock());

            StartSimulatorIfNecessary();
            InitializeAnalyzerClient();
            _calculatorResponseMessages = new Dictionary<int, CalculatorResponseMessage>();
            
            //first forced connection
            try
            {
                var result = _deviceManager.Connect();
                if(result)  
                    _taskManager.Initialize();
            }
            catch
            {
            }

            while (true)
            {
                var line = Console.ReadLine();
                if (line.Equals("save"))
                {
                    Console.WriteLine("Input number of scans to be saved");
                    var numberOfScans = Int32.Parse(Console.ReadLine());
                    var masterScans = new byte[numberOfScans * Constants.WideBandFftSampleCount];
                    var slaveScans = new byte[numberOfScans * Constants.WideBandFftSampleCount];
                    for (int i = 0; i < numberOfScans; i++)
                    {
                        var masterScan = _deviceManager.Ftm2M.WideReadFft();
                        Array.Copy(masterScan, 0, masterScans, 466 * i, masterScan.Length);
                        var slaveScan = _deviceManager.Ftm2S.WideReadFft();
                        Array.Copy(slaveScan, 0, slaveScans, 466 * i, slaveScan.Length);
                    }
                    File.WriteAllBytes($"{numberOfScans}MasterScans.bin", masterScans);
                    File.WriteAllBytes($"{numberOfScans}SlaveScans.bin", slaveScans);
                    Console.WriteLine("Scans saved");
                }
                //if (line.Equals("antenna"))
                //{
                //    polinaReceiver.SetCommutatorAntenna(2);
                //}
                //if (line.Equals("super cool test"))
                //{
                //    var argz = Int32.Parse(Console.ReadLine());
                //    var testCount = 100;
                //    var successfulTests = 0;
                //    for (int i = 0; i < testCount; i++)
                //    {
                //        //init
                //        polinaReceiver.SetCommutatorAntenna(2);
                //        Thread.Sleep(50);
                //        for(int j = 0; j < 10; j++)
                //            _deviceManager.Ftm2M.WbReadFft();
                //        var scan = _deviceManager.Ftm2M.WbReadFft();

                //        if (scan.Any(v => v > _deviceManager.Threshold.ToByteAmplitude()))
                //            Console.WriteLine("waaaa");

                //        polinaReceiver.SetCommutatorAntenna(1);
                //        Thread.Sleep(argz);
                //        scan = _deviceManager.Ftm2M.WbReadFft();

                //        if (scan.Any(v => v > _deviceManager.Threshold.ToByteAmplitude()))
                //            successfulTests++;
                //    }
                //    Console.WriteLine($"{successfulTests} out of {testCount}, sleep time {argz} ms");

                //}
                if (line.Equals("scan test"))
                {
                    var w = new Stopwatch();
                    w.Start();
                    var scan = _deviceManager.Ftm2M.WideReadFft();
                    w.Stop();
                    Console.WriteLine($"{w.Elapsed.TotalMilliseconds} ms for scan");
                }
                //if (line.Equals("cool test"))
                //{
                //    var argz = Int32.Parse(Console.ReadLine());
                //    polinaReceiver.SetCommutatorAntenna(1);
                //    Thread.Sleep(50);
                //    var scan = _deviceManager.Ftm2M.WbReadFft();
                //    var w = new Stopwatch();
                //    w.Start();
                //    polinaReceiver.SetCommutatorAntenna(2);
                //    Thread.Sleep(argz);
                //    scan = _deviceManager.Ftm2M.WbReadFft();
                //    w.Stop();
                //    Console.WriteLine($"{w.Elapsed.TotalMilliseconds} ms for switch + 1 scan");

                //}
                if (line.Equals("iq start"))
                {
                    var device = _deviceManager.Ftm2M;
                    device.SetOutputMode(FtmDeviceOutputMode.IqComponents); //trying iq
                }
                if (line.Equals("iq stop"))
                {
                    var device = _deviceManager.Ftm2M;
                    device.SetOutputMode(FtmDeviceOutputMode.NoOutput);
                }
                if (line.Equals("test"))
                {
                    try
                    {
                        var watch = new Stopwatch();
                        var device = _deviceManager.Ftm2M;
                        Console.WriteLine("Input args");
                        var iqArgs = Console.ReadLine().Split(' ').Select(e => Byte.Parse(e)).ToArray();

                        //device.SetOutputMode(FtmDeviceOutputMode.IqComponents); //trying iq
                        device.DataClient.Client.ReceiveTimeout = iqArgs[2];
                        device.DataClient.Client.ReceiveTimeout = 5000;
                        var measurementSeconds = iqArgs[3];
                        var list = new byte[35_000 * measurementSeconds][];
                        var i = 0;
                        watch.Start();
                        var numberOfPackets = 0;
                        device.Client.SendReceive(new byte[] { 0x34, 2, iqArgs[0], iqArgs[1], 1 });
                        while (watch.Elapsed.TotalSeconds < measurementSeconds)
                        {
                            var length = list.Length;
                            var packet = device.DataClient.Receive();
                            numberOfPackets++;
                            list[i] = packet;
                            i++;
                        }
                        watch.Stop();
                        MessageLogger.Log("hey yo finished");
                        //device.Client.SendReceive(new byte[] { 0x35, 1 });
                        //device.SetOutputMode(FtmDeviceOutputMode.NoOutput);
                        //var iqData = new IqScan[numberOfPackets];
                        i = 0;
                        try
                        {
                            for (int j = 0; j < list.Length; j++)
                            {
                                if (list[j] == null)
                                    continue;
                                if (list[j].Length == 0)
                                    continue;
                                var iqArray = new byte[1024];
                                Array.Copy(list[j], 2, iqArray, 0, 1024);
                                if (list[j].Length >= 1)
                                    MessageLogger.Log($"index {j} : n {list[j][1]}, length {list[j].Length}");
                                else
                                    MessageLogger.Log($"index {j} : length {list[j].Length}");
                                var scan = new IqScan(iqArray);
                                File.AppendAllText($"Real{measurementSeconds}Sec.txt", scan.Real.ShortArrayToString());
                                File.AppendAllText($"Imaginary{measurementSeconds}Sec.txt", scan.Imaginary.ShortArrayToString());
                                i++;

                                if (list[j].Length - 2 != 1024)
                                    MessageLogger.Log("hey yo watafaka");
                            }
                        }
                        catch
                        {
                            var e = "";
                        }
                        MessageLogger.Log($"Received {numberOfPackets} packets\r\n" +
                                          $"{watch.Elapsed.TotalMilliseconds} ms spent for them");
                        continue;
                    }
                    catch (Exception e)
                    {
                        MessageLogger.Error($"Input params error\r\n" +
                                            $"{e}\r\n" +
                                            $"{e.StackTrace}");
                    }
                }
                if (line.Equals("binrf"))
                {
                    Console.WriteLine("input args");

                    var rfArgs = Console.ReadLine().Split(' ').Select(v => Int32.Parse(v)).ToList();
                    var watch = new Stopwatch();
                    var device = _taskManager.DeviceManager.Ftm2M;
                    var Client = device.Client;
                    watch.Start();
                    var scans = new byte[10000 * 1024];
                    var index = 0;
                    var count = 0;
                    while (watch.Elapsed.TotalMilliseconds < rfArgs[0] * 1000)
                    {
                        var result1 = Client.SendReceive(new byte[] { 0x8c, 0 });
                        var result2 = Client.SendReceive(new byte[] { 0x8c, 1 });
                        var result3 = Client.SendReceive(new byte[] { 0x8c, 2 });
                        var result4 = Client.SendReceive(new byte[] { 0x8c, 3 });
                        Array.Copy(result1, 0, scans, index, result1.Length);
                        index += 1024;
                        Array.Copy(result2, 0, scans, index, result2.Length);
                        index += 1024;
                        Array.Copy(result3, 0, scans, index, result3.Length);
                        index += 1024;
                        Array.Copy(result4, 0, scans, index, result4.Length);
                        index += 1024;
                        count++;
                    }
                    watch.Stop();
                    var result = new byte[1024 * count * 4];
                    Array.Copy(scans, 0, result, 0 , result.Length);
                    File.WriteAllBytes("rf.bin", result);
                    MessageLogger.Log($"Received {count}, time {watch.Elapsed.TotalMilliseconds} ms");
                }

                if (line.Equals("rf"))
                {
                    Console.WriteLine("input args");

                    var rfArgs = Console.ReadLine().Split(' ').Select(v => Int32.Parse(v)).ToList(); 
                    var watch = new Stopwatch();
                    var device = _taskManager.DeviceManager.Ftm2M;
                    watch.Start();
                    var windowed = new int[10000][];
                    var raw = new int[10000][];
                    var index = 0;
                    while (watch.Elapsed.TotalMilliseconds < rfArgs[0] * 1000) 
                    {
                        var rf = device.RfSignalSpectrumRead(FtmRole.Master);
                        raw[index] = rf.Item1;
                        windowed[index] = rf.Item2;
                        index++;
                    }
                    watch.Stop();
                    for (int i = 0; i < index; i++)
                    {
                        File.AppendAllText("Raw10Sec.txt", raw[i].IntArrayToString());
                        File.AppendAllText("Windowed10Sec.txt", windowed[i].IntArrayToString());
                    }
                    MessageLogger.Log($"Received {index}, {watch.Elapsed.TotalMilliseconds}");
                    
                }

                //switches respondToAnalyzer flag
                if (line.Equals("flag"))
                {
                    RespondToAnalyzer = !RespondToAnalyzer;
                    MessageLogger.Log($"respond to analyzer flag is {RespondToAnalyzer}");
                }

                if (line.Equals("1"))
                {
                    _taskManager.SetMode(new RdfMode(_taskManager));
                }

                //tries to get iq data
                if (line.Equals("iq"))
                {
                    IsIqRunning = !IsIqRunning;
                    MessageLogger.Log($"iq flag is {IsIqRunning}");
                }

                //allows to get current threshold and change it
                if (line.Split(' ')[0].Equals("p"))
                {
                    try
                    {
                        if (line.Split(' ').Length == 1)
                        {
                            MessageLogger.Log($"threshold now : {_deviceManager.Threshold}, in bytes {_deviceManager.Threshold.ToByteAmplitude()}");
                        }

                        var value = Int32.Parse(line.Split(' ')[1]);
                        _deviceManager.Threshold = ((byte)(_deviceManager.Threshold.ToByteAmplitude() + value)).ToVoltAmplitude();
                        MessageLogger.Log($"threshold now : {_deviceManager.Threshold}, in bytes {_deviceManager.Threshold.ToByteAmplitude()}");
                    }
                    catch
                    {
                    }
                }

                if (line.Equals("start"))
                {
                    if (!_deviceManager.Connect())
                    {
                        MessageLogger.Error("Connection failed");
                        continue;
                    }
                    if (!_taskManager.Initialize())
                    {
                        MessageLogger.Error("Initialization failed");
                        continue;
                    }
                }

                //performs full calibration of blocks
                if (line.Equals("calibration"))
                {
                    _deviceManager.StartNoiseCalibration();
                    continue;
                }

                //performs calibration of master
                if (line.Equals("m calibration"))
                {
                    _deviceManager.Ftm2M.StartNoiseCalibration();
                    continue;
                }

                //performs calibration of slave
                if (line.Equals("s calibration"))
                {
                    _deviceManager.Ftm2S.StartNoiseCalibration();
                    continue;
                }

                //stops rdf mode
                if (line.Equals("stop"))
                {
                    IsRdfRunning = false;
                }

                //clears signal storage
                if (line.Equals("clear"))
                {
                    _taskManager.SignalStorage.Clear();
                }

                //calculates threshold via noise
                if (line.Equals("threshold"))
                {
                    var task = new ThresholdsTask(_deviceManager, 100);
                    _taskManager.AddTask(task);
                }

                //receiver calculator data
                if ((line.Split(' ')[0].Equals("m") && line.Split(' ')[1].Equals("calc")) ||
                    (line.Split(' ')[0].Equals("s") && line.Split(' ')[1].Equals("calc")))
                {
                    try
                    {
                        var parts = line.Split(' ').ToList();
                        var device = parts[0].Equals("m") ? _deviceManager.Ftm2M : _deviceManager.Ftm2S;
                        device.SetOutputMode(parts[2].Equals("wb") ? FtmDeviceOutputMode.WideCalculator : FtmDeviceOutputMode.NarrowCalculator);
                        var data = device.DataClient.Receive();
                        device.SetOutputMode(FtmDeviceOutputMode.NoOutput);
                        for (int i = 2; i < data.Length; i += 16)
                        {
                            try
                            {
                                var msg = CalculatorResponseMessage.Parse(data, i);
                                var bitFreqAndAmp = Convert.ToString(msg.FrequencyAndAmplitude, 2);
                                var bitFreq = bitFreqAndAmp.Remove(9);
                                var bitAmp = bitFreqAndAmp.Remove(0, 9);

                                var FrequencyMHz = Convert.ToInt32(bitFreq, 2) + 1100;
                                var Amplitude = Convert.ToInt32(bitAmp, 2);

                                var bitApp = Convert.ToString(msg.ImpulseAppearanceStamp, 2);
                                var bitSec = bitApp.Remove(6);
                                var bitTime = bitApp.Remove(0, 6);
                                var Seconds = Convert.ToInt32(bitSec, 2);
                                var Ticks = Convert.ToInt32(bitTime, 2);
                                var Duration = Utilities.ThreeBytesToInt(msg.ImpulseDurationLow, msg.ImpulseDurationMiddle,
                                                   msg.ImpulseDurationHigh) * 5;

                                MessageLogger.Log($"{(i - 2) / 16}: {msg.BandwidthMhz}, freq {FrequencyMHz}, amp {Amplitude}, {msg.Hours}:{msg.Minutes}:{Seconds}:{Ticks}, " +
                                                  $"repeat {msg.ImpulseRepeatCount * 5}, duration {Duration}");
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        continue;
                    }
                    catch (Exception e)
                    {
                        MessageLogger.Error($"Input params error\r\n{e}\r\n{e.StackTrace}");
                    }
                }

                //sends any command to respective block
                if (line.Split(' ')[0].Equals("m") || line.Split(' ')[0].Equals("s"))
                {
                    try
                    {
                        var parts = line.Split(' ').ToList();
                        var device = parts[0].Equals("m") ? _deviceManager.Ftm2M : _deviceManager.Ftm2S;
                        parts.RemoveAt(0);
                        var first = parts[0];
                        byte commandCode;
                        try
                        {
                            commandCode = Byte.Parse(first);
                        }
                        catch
                        {
                            commandCode = Convert.ToByte(first, 16);
                        }
                        parts.RemoveAt(0);
                        var arguments = parts.Select(Byte.Parse).ToList();
                        var commandCodeList = new List<byte>
                        {
                            commandCode
                        };
                        var command = commandCodeList.Concat(arguments);
                        device.Client.SendReceive(command.ToArray());
                        continue;
                    }
                    catch
                    {
                        MessageLogger.Error("Input params error");
                    }
                }

                if (line.Equals("exit"))
                    break;
            }
        }

        private static void StartSimulatorIfNecessary()
        {
            Process simulatorProcess;
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            var simTask = Task.Run(() =>
            {
                if (Config.Instance.HardwareSettings.IsSimulation)
                {
                    if (Process.GetProcessesByName("Simulator").Any())
                    {
                        return;
                    }
                    simulatorProcess = new Process
                    {
                        StartInfo = { FileName = "Simulator.exe" }
                    };
                    simulatorProcess.Start();
                }
            });
            simTask.Wait();
            Thread.Sleep(2000);//time for simulator to start
        }

        private static void InitializeAnalyzerClient()
        {
            var ipEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 14880);
            _analyzerServer = new UdpClient(ipEndPoint);
            _analyzerServer.Connect("127.0.0.1", 14881);
            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        _analyzerServer.Client.ReceiveTimeout = 5000;
                        var request = _analyzerServer.Receive();
                        if (!RespondToAnalyzer)
                            continue;
                        switch (request[0])
                        {
                            case 0:
                                {
                                    RespondToAnalyzer = false;
                                    break;
                                }
                            case 1:
                                {
                                    RespondToAnalyzer = true;
                                    break;
                                }
                            case 2:
                                {
                                    //var response =
                                    //    _taskManager.SpectrumStorage.lastMasterScan.Amplitudes
                                    //        .Select(b => (byte)(b * _taskManager.DeviceManager.MasterAntennaCoefficient)).ToArray();
                                    //_analyzerServer.Send(response, response.Length);
                                    break;
                                }
                            case 3:
                                {
                                    //var response =
                                    //    _taskManager.SpectrumStorage.lastSlaveScan.Amplitudes
                                    //        .Select(b => (byte)(b * _taskManager.DeviceManager.SlaveAntennaCoefficient)).ToArray();
                                    //_analyzerServer.Send(response, response.Length);
                                    break;
                                }
                            case 4:
                                {
                                    var response = new[] { _deviceManager.Ftm2M.WideSettings.Threshold };
                                    _analyzerServer.Send(response, response.Length);
                                    break;
                                }
                            case 5:
                                {
                                    var count = _taskManager.SignalStorage.Signals.Count;
                                    var response = new byte[count * (4 + CalculatorResponseMessage.BinarySize)];
                                    var startIndex = 0;
                                    if (_taskManager.SignalStorage.Signals.Count == 0)
                                    {
                                        _analyzerServer.Send(response, response.Length);
                                        break;
                                    }

                                    foreach (var signal in _taskManager.SignalStorage.Signals)
                                    {
                                        var directionBytes = BitConverter.GetBytes(signal.Direction);
                                        for (int i = 0; i < 4; i++)
                                        {
                                            response[i + startIndex] = directionBytes[i];
                                        }

                                        var msg = new CalculatorResponseMessage();
                                        var keyOrDefaultKey = _calculatorResponseMessages.Keys.FirstOrDefault(k =>
                                            Math.Abs(k - signal.FrequencyKhz / 1000) < 5); //MHz gap
                                        if (keyOrDefaultKey != 0)
                                        {
                                            msg = _calculatorResponseMessages[keyOrDefaultKey];
                                        }

                                        var msgBytes = msg.GetBytes();
                                        for (int i = 0; i < CalculatorResponseMessage.BinarySize; i++)
                                            response[i + startIndex + 4] = msgBytes[i];
                                        startIndex += 4 + CalculatorResponseMessage.BinarySize;
                                    }

                                    _analyzerServer.Send(response, response.Length);
                                    break;
                                }
                            case 6:
                                {
                                    if (IsIqRunning)
                                    {
                                        var device = _deviceManager.Ftm2M;

                                        device.SetOutputMode(FtmDeviceOutputMode.IqComponents); //trying iq

                                        device.DataClient.Client.ReceiveTimeout = 50;
                                        device.Client.SendReceive(new byte[] { 0x34, 2, 9, 9, 1 });
                                        var data = device.DataClient.Receive();
                                        var iqData = new byte[data.Length - 2];
                                        Array.Copy(data, 2, iqData, 0, data.Length - 2);
                                        device.SetOutputMode(FtmDeviceOutputMode.NoOutput);
                                        var removed = 0;
                                        while (true)
                                        {
                                            var b = device.DataClient.Receive();
                                            removed++;
                                            if (b.Length == 0)
                                                break;
                                        }

                                        MessageLogger.Log($"removed {removed} packets");
                                        _analyzerServer.Send(iqData, iqData.Length);
                                    }
                                    else
                                    {
                                        var response = new byte[1] { 0 };
                                        _analyzerServer.Send(response, response.Length);
                                    }

                                    break;
                                }
                        }
                    }
                    catch (Exception e)
                    {
                        //ignored
                    }
                }
            });
        }
    }
}
