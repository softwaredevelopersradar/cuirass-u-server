﻿using System;

namespace CuirassTestApp
{
    public struct IqScan
    {
        public short[] Real { get; private set; }
        public short[] Imaginary { get; private set; }

        private const int ScanSize = 256;

        public IqScan(byte[] iqBytes)
        {
            Real = new short[ScanSize];
            Imaginary = new short[ScanSize];

            if (iqBytes.Length != 1)
            {
                for (int i = 0; i < ScanSize; i++)
                {
                    var realBytes = new byte[] { iqBytes[4 * i + 1], iqBytes[4 * i] };
                    var imgBytes = new byte[] { iqBytes[4 * i + 3], iqBytes[4 * i + 2] };
                    var real = BitConverter.ToInt16(realBytes, 0);
                    var imaginary = BitConverter.ToInt16(imgBytes, 0);
                    Real[i] = real;
                    Imaginary[i] = imaginary;
                }
            }
        }
    }

    public static class Extensions
    {
        public static string DoubleArrayToString(this double[] array)
        {
            var output = "";
            for (int i = 0; i < array.Length; i++)
            {
                output += $"{array[i]} ";
            }

            return output;
        }

        public static string ShortArrayToString(this short[] array)
        {
            var output = "";
            for (int i = 0; i < array.Length; i++)
            {
                output += $"{array[i]} ";
            }

            return output;
        }

        public static string IntArrayToString(this int[] array)
        {
            var output = "";
            for (int i = 0; i < array.Length; i++)
            {
                output += $"{array[i]} ";
            }

            return output;
        }
    }
}
