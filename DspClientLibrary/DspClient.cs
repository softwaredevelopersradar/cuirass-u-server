﻿using System;
using DspDataModel.Server;
using Grpc.Core;
using TransmissionPackage;
using ProtoLibrary;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DspClientLibrary
{
    public class DspClient : IDspClient
    {
        public bool IsConnected
        {
            get => _isConnected;
            private set
            {
                if (_isConnected == value)
                    return;

                _isConnected = value;
                if (_isConnected)
                    OnConnect?.Invoke(this, EventArgs.Empty);
                else
                    OnDisconnect?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;
        public static event EventHandler<string> OnDeadlineExceeded;
        public event EventHandler<DspDataModel.Server.DspServerMode> ModeReceived;
        public event EventHandler<AntennasState> AntennasStateReceived;

        private Channel _channel;
        private Transmission.TransmissionClient _client;
        private bool _isInitialized;
        private bool _isConnected;

        private bool IsAntennaStreamUp = false;
        /// <summary>
        /// Used for antenna state stream
        /// </summary>
        private System.Threading.CancellationTokenSource _cancellationTokenSource;
        private DateTime Deadline => DateTime.UtcNow.AddSeconds(1);

        public string ClientAddress { get; private set; }
        public int ClientPort { get; private set; }

        public DspClient(string host = "127.0.0.1", int port = 10001)
        {
            ClientAddress = host;
            ClientPort = port;
            InitializeChannels();
        }

        private void InitializeChannels()
        {
            _channel = new Channel(ClientAddress, ClientPort, ChannelCredentials.Insecure);
            _client = new Transmission.TransmissionClient(_channel);
            _isInitialized = true;
        }

        private void ShutDown()
        {
            if (!_isInitialized)
                return;
            _isInitialized = false;
            _channel.ShutdownAsync().Wait();
        }

        public bool Connect()
        {
            if (_isInitialized == false)
                return false; //todo : add log
            return Ping();
        }

        public async Task<bool> ConnectAsync()
        {
            try
            {
                if (_isInitialized == false)
                    return false;
                await _client.PingAsync(new PingRequest(), deadline: Deadline);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private delegate TResponse CommandDelegate<TRequest, TResponse>(TRequest request, Metadata headers = null,
            DateTime? deadline = null, System.Threading.CancellationToken cancellationToken = default);

        private TResponse SendGprcCommandWithDeadline<TRequest, TResponse>(CommandDelegate<TRequest, TResponse> command, TRequest request)
            where TResponse : new()
        {
            try
            {
                var commandResponse = command(request: request, deadline: Deadline);
                IsConnected = true;
                return commandResponse;
            }
            catch (RpcException)
            {
                IsConnected = false;
                OnDeadlineExceeded?.Invoke(this, $"Deadline exceeded for command {command.Method.Name}");
                return new TResponse();
            }
        }

        private bool Ping()
        {
            var pingResponse = SendGprcCommandWithDeadline(_client.Ping, new PingRequest());
            if (pingResponse.IsFirstPingRequest)
                ;//todo : perform GET CURRENT MODE AND STUFF
            return true;
        }

        public bool Connect(string host, int port)
        {
            ShutDown();
            ClientAddress = host;
            ClientPort = port;
            InitializeChannels();
            return Ping();
        }

        public void Disconnect()
        {
            ShutDown();
            IsConnected = false;
        }

        public byte[] GetSpectrum(DspDataModel.FtmRole role)
        {
            var sup = SendGprcCommandWithDeadline(
                command: _client.GetSpectrum,
                request: new SpectrumRequest() { Role = (FtmRole)role });
            return sup.Spectrum.ToByteArray();
        }

        public bool SetMode(DspDataModel.Server.DspServerMode mode)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetMode,
                request: new SetModeRequest() { Mode = mode.ConvertToProto() });
            return response.IsSucceeded;
        }

        public DspDataModel.Server.DspServerMode GetMode()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetMode,
                request: new DefaultRequest());
            return response.Mode.ConvertToDspModel();
        }

        public byte[] GetNarrowSpectrum(DspDataModel.FtmRole role)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetNarrowSpectrum,
                request: new SpectrumRequest() { Role = (FtmRole)role });
            return response.Spectrum.ToByteArray();
        }

        public int[] GetVideoSpectrum(DspDataModel.FtmRole role)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetVideoSpectrum,
                request: new SpectrumRequest() { Role = (FtmRole)role });
            return response.Spectrum.ToArray();
        }

        public (int[], int[]) GetRfSignalSpectrum(DspDataModel.FtmRole role)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetRfSignalSpectrum,
                request: new SpectrumRequest() { Role = (FtmRole)role });
            return (response.RawSpectrum.ToArray(), response.WindowedSpectrum.ToArray());
        }

        public void EstablishAntennaStateStream()
        {
            if (IsAntennaStreamUp)
                return;
            try
            {
                var stream = _client.SubscribeForAntennaUpdates(new DefaultRequest());
                InitializeAntennaStreamArguments();
                AntennaStateStreamTask(stream);
            }
            catch (RpcException)
            {
                OnDeadlineExceeded?.Invoke(this, $"Deadline exceeded during antenna stream establishment");
            }
        }

        private void InitializeAntennaStreamArguments()
        {
            IsAntennaStreamUp = true;
            _cancellationTokenSource = new System.Threading.CancellationTokenSource();
        }

        private async Task AntennaStateStreamTask(AsyncServerStreamingCall<AntennasResponse> stream)
        {
            while (await stream.ResponseStream.MoveNext(_cancellationTokenSource.Token) && IsAntennaStreamUp)
            {
                var current = stream.ResponseStream.Current;
                var antennaState = new AntennasState((DspDataModel.Hardware.AntennaSwitchSource)current.SwitchSource, current.WorkingAntennas);
                AntennasStateReceived?.Invoke(this, antennaState);
            }
        }

        public void AbortAntennaStateStream()
        {
            if (!IsAntennaStreamUp)
                return;
            _cancellationTokenSource.Cancel();
            IsAntennaStreamUp = false;
        }

        public bool StartNarrowRdfTask()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.StartNarrowRdfTask,
                request: new DefaultRequest());
            return response.IsSucceeded;
        }
        public bool StopNarrowRdfTask()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.StopNarrowRdfTask,
                request: new DefaultRequest());
            return response.IsSucceeded;
        }

        public bool StartVideoSpectrumTask()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.StartVideoSpectrumTask,
                request: new DefaultRequest());
            return response.IsSucceeded;
        }
        public bool StopVideoSpectrumTask()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.StopVideoSpectrumTask,
                request: new DefaultRequest());
            return response.IsSucceeded;
        }

        public int GetRdfThreshold()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetRdfThreshold,
                request: new DefaultRequest());
            return response.ThresholdParrots;
        }

        public bool SetRdfThreshold(int threshold)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetRdfThreshold,
                request: new ThresholdMessage() { ThresholdParrots = threshold });
            return response.IsSucceeded;
        }

        public int GetPicThreshold(bool isMaster, bool isWideBand)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetPicThreshold,
                request: new ComparatorThresholdRequest()
                {
                    IsMaster = isMaster,
                    IsWideBand = isWideBand
                });
            return response.ThresholdParrots;
        }
        public bool SetPicThreshold(int picThreshold, bool isMaster, bool isWideBand)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetPicThreshold,
                request: new ComparatorThresholdMessage()
                {
                    ThresholdParrots = picThreshold,
                    IsMaster = isMaster,
                    IsWideBand = isWideBand
                });
            return response.IsSucceeded;
        }

        public int GetAttackThreshold(bool isMaster, bool isWideBand)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetAttackThreshold,
                request: new ComparatorThresholdRequest()
                {
                    IsMaster = isMaster,
                    IsWideBand = isWideBand
                });
            return response.ThresholdParrots;
        }

        public bool SetAttackThreshold(int attackThreshold, bool isMaster, bool isWideBand)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetAttackThreshold,
                request: new ComparatorThresholdMessage()
                {
                    ThresholdParrots = attackThreshold,
                    IsMaster = isMaster,
                    IsWideBand = isWideBand
                });
            return response.IsSucceeded;
        }

        public DspDataModel.Hardware.VideoWorkingBand GetVideoWorkingBand()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetVideoWorkingBand,
                request: new DefaultRequest());
            return (DspDataModel.Hardware.VideoWorkingBand)response.WorkingBand;
        }

        public bool SetVideoWorkingBand(DspDataModel.Hardware.VideoWorkingBand workingBand)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetVideoWorkingBand,
                request: new VideoSpectrumWorkingBandMessage() { WorkingBand = (WorkingBand)workingBand });
            return response.IsSucceeded;
        }

        /// <summary>
        /// Removes frs signals from the signal storage
        /// </summary>
        public bool RemoveFrsSignal(int signalId)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.RemoveFromSignalStorage,
                request: new SignalIdMessage() { SignalId = signalId });
            return response.IsSucceeded;
        }

        public bool ClearFrsSignalStorage()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.ClearSignalStorage,
                request: new DefaultRequest());
            return response.IsSucceeded;
        }

        public int GetCommutatorAttenuator(DspDataModel.FtmRole role)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetCommutatorAttenuator,
                request: new SpectrumRequest() { Role = (FtmRole)role });
            return response.Attenuator;
        }

        public bool SetCommutatorAttenuator(DspDataModel.FtmRole role, int attenuator)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetCommutatorAttenuator,
                request: new AttenuatorMessage() { Role = (FtmRole)role, Attenuator = attenuator });
            return response.IsSucceeded;
        }

        public bool SetAntennaFrequencySettings(int frequencyMhz, IEnumerable<int> antennaNumbers)
        {
            var (masterAntennaIndices, slaveAntennaIndices) = DspDataModel.Hardware.AntennaHelper.ConvertNumbersToIndices(antennaNumbers);
            var request = new FrequencyAntennaSetting()
            {
                FrequencyMhz = frequencyMhz
            };
            request.MasterAntennaIndices.AddRange(masterAntennaIndices);
            request.SlaveAntennaIndices.AddRange(slaveAntennaIndices);
            var response = SendGprcCommandWithDeadline(
                command: _client.SetAntennaFrequencySettings,
                request: request);
            return response.IsSucceeded;
        }

        public DspDataModel.Hardware.StrobeMode GetStrobeMode()
        {
            var response = SendGprcCommandWithDeadline(
                            command: _client.GetStrobeMode,
                            request: new DefaultRequest());
            return (DspDataModel.Hardware.StrobeMode)response.Mode;
        }

        public bool SetStrobeMode(DspDataModel.Hardware.StrobeMode mode)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetStrobeMode,
                request: new StrobeModeMessage()
                {
                    Mode = (StrobeMode)mode
                });
            return response.IsSucceeded;
        }

        public DspDataModel.Hardware.WindowFunctionType GetWindowType()
        {
            var response = SendGprcCommandWithDeadline(
                               command: _client.GetWindowType,
                               request: new DefaultRequest());
            return (DspDataModel.Hardware.WindowFunctionType)response.WindowType;
        }

        public bool SetWindowType(DspDataModel.Hardware.WindowFunctionType type)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetWindowType,
                request: new WindowFunctionMessage()
                {
                    WindowType = (WindowFunctionType)type
                });
            return response.IsSucceeded;
        }

        public DspDataModel.Hardware.SecondsMark GetSecondsMark()
        {
            var response = SendGprcCommandWithDeadline(
                               command: _client.GetSecondsMark,
                               request: new DefaultRequest());
            return (DspDataModel.Hardware.SecondsMark)response.Mark;
        }
        public bool SetSecondsMark(DspDataModel.Hardware.SecondsMark mark)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetSecondsMark,
                request: new ExternalSecondsMessage()
                {
                    Mark = (SecondsMark)mark
                });
            return response.IsSucceeded;
        }

        public DspDataModel.RdfPairsMode GetPairsMode()
        {
            var response = SendGprcCommandWithDeadline(
                               command: _client.GetPairsMode,
                               request: new DefaultRequest());
            return (DspDataModel.RdfPairsMode)response.Mode;
        }
        public bool SetPairsMode(DspDataModel.RdfPairsMode pairsMode)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetPairsMode,
                request: new PairsModeMessage()
                {
                    Mode = (RdfPairsMode)pairsMode
                });
            return response.IsSucceeded;
        }

        public int GetFftDelay(DspDataModel.FtmRole role, DspDataModel.Hardware.VideoWorkingBand band)
        {
            var response = SendGprcCommandWithDeadline(
                   command: _client.GetFftDelay,
                   request: new RoleBandMessage()
                   {
                       Role = (FtmRole)role,
                       Band = (WorkingBand)band
                   });
            return response.Delay;
        }

        public bool SetFftDelay(int delay, DspDataModel.FtmRole role, DspDataModel.Hardware.VideoWorkingBand band)
        {
            var response = SendGprcCommandWithDeadline(
                   command: _client.SetFftDelay,
                   request: new FftDelayMessage()
                   {
                       Delay = delay,
                       Settings = new RoleBandMessage()
                       {
                           Role = (FtmRole)role,
                           Band = (WorkingBand)band
                       }
                   });
            return response.IsSucceeded;
        }

        public bool GetNarrowMeasureStrategy()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetNarrowMeasureStrategy,
                request: new DefaultRequest());
            return response.IsEnabled;
        }

        public bool SetNarrowMeasureStrategy(bool isMeasuringAllowed)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetNarrowMeasureStrategy,
                request: new IsActionEnabledMessage()
                {
                    IsEnabled = isMeasuringAllowed
                });
            return response.IsSucceeded;
        }

        public bool SetWideMeasureBand(float startFrequencyMhz, float endFrequencyMhz)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetWideMeasureBand,
                request: new WideMeasureBandMessage()
                {
                    BandStartMhz = startFrequencyMhz,
                    BandEndMhz = endFrequencyMhz
                });
            return response.IsSucceeded;
        }

        public bool SetSignalForMeasuring(int signalId) 
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetSignalForMeasuring,
                request: new MeasureSignalMessage()
                {
                    SignalId = signalId
                });
            return response.IsSucceeded;
        }

        public (byte[] spectrum, int numberOfPointsPerScan) GetIntensitySpectrum()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetIntensitySpectrum,
                request: new SpectrumRequest()
                {
                    Role = FtmRole.Master
                });
            return (response.Spectrum.ToByteArray(), response.NumberOfPoints);
        }

        public DspDataModel.FrequencyCoefficientRange[] GetFrequencyCoefficientRanges()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetFrequencyRangeCoefficients,
                request: new DefaultRequest());
            var ranges = response.Ranges.Select(r => new DspDataModel.FrequencyCoefficientRange() 
            {
                StartFrequencyMhz = r.StartFrequencyMhz,
                EndFrequencyMhz = r.EndFrequencyMhz,
                Coefficient = r.Coefficient
            });
            return ranges.ToArray();
        }

        public bool SetFrequencyCoefficientRange(int startFrequencyMhz, int endFrequencyMhz, float coefficient)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetFrequencyRangeCoefficient,
                request: new FrequencyRangeCoefficient()
                {
                    StartFrequencyMhz = startFrequencyMhz,
                    EndFrequencyMhz = endFrequencyMhz,
                    Coefficient = coefficient
                });
            return response.IsSucceeded;
        }

        public bool GetWaterfallStrategy()
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.GetWaterfallStrategy,
                request: new DefaultRequest());
            return response.IsEnabled;
        }

        public bool SetWaterfallStrategy(bool isWaterfallEnabled)
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.SetWaterfallStrategy,
                request: new IsActionEnabledMessage()
                {
                    IsEnabled = isWaterfallEnabled
                });
            return response.IsSucceeded;
        }

        public bool StartSeriesMeasuringTask() 
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.StartSeriesMeasuringTask,
                request: new DefaultRequest());
            return response.IsSucceeded;
        }

        public bool StartNoiseCalibration() 
        {
            try
            {
                var longDeadline = Deadline.AddSeconds(5); //noise calibration takes some time to complete
                var commandResponse = _client.PerformThresholdCalibration(request: new DefaultRequest(), deadline: longDeadline);
                IsConnected = true;
                return commandResponse.IsSucceeded;
            }
            catch (RpcException)
            {
                IsConnected = false;
                OnDeadlineExceeded?.Invoke(this, $"Deadline exceeded for command {nameof(this.StartNoiseCalibration)}");
                return false;
            }
        }

        public bool StartIqGathering() 
        {
            var response = SendGprcCommandWithDeadline(
                command: _client.PerformIqAggregation,
                request: new DefaultRequest());
            return response.IsSucceeded;
        }
    }
}
