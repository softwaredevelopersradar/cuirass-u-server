﻿using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using DspDataModel.Hardware;
using DspDataModel;
using Settings;

namespace Simulator
{
    public class DeviceSimulator
    {
        private readonly UdpClient _server;
        private readonly SignalSimulator _signalSimulator;
        private float _narrowFrequencyKhz;
        private float _narrowBandwidthKhz;
        private bool _isWorking;
        private CancellationTokenSource _cancellationTokenSource;

        private FtmRole Role { get; }

        public bool IsWorking
        {
            get => _isWorking;
            set
            {
                _isWorking = value;
                if (_isWorking)
                {
                    _cancellationTokenSource = new CancellationTokenSource();
                    StartWorkCircuit(_cancellationTokenSource.Token);
                }
                else
                    _cancellationTokenSource.Cancel();
            }
        }
        public float NarrowFrequencyKhz {
            get => _narrowFrequencyKhz;
            set
            {
                if (value >= 1_100_000 && value <= 1_600_000)
                    _narrowFrequencyKhz = value;
            }
        }
        public float NarrowBandwidthKhz
        {
            get => _narrowBandwidthKhz;
            set
            {
                if (value >= 5_000 && value <= 50_000)
                    _narrowBandwidthKhz = value;
            }
        }

        public DeviceSimulator(string ipAddress, int port, FtmRole role, SignalSimulator signalSimulator, SimulatorSave simulatorSave)
        {
            var ipEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);
            _server = new UdpClient(ipEndPoint);
            Role = role;
            _signalSimulator = signalSimulator;
            _narrowFrequencyKhz = role == FtmRole.Master ? simulatorSave.NarrowMasterFrequency : simulatorSave.NarrowSlaveFrequency;
            _narrowBandwidthKhz = role == FtmRole.Master ? simulatorSave.NarrowMasterBandwidth : simulatorSave.NarrowSlaveBandwidth;
            IsWorking = true;
        }

        /// <summary>
        /// Start reading port endlessly and respond to particular requests
        /// </summary>
        private async Task StartWorkCircuit(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    var request = await _server.ReceiveAsync();
                    byte[] response;
                    var commandCode = (CommandCodes) request.Buffer[0];
                    switch (commandCode)
                    {
                        case CommandCodes.WideReadFft:
                        {
                            response = _signalSimulator.GenerateWideBandData(Role);
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break;
                        }
                        case CommandCodes.NarrowReadFft:
                        {
                            response = _signalSimulator.GenerateNarrowData(NarrowFrequencyKhz,
                                NarrowBandwidthKhz, Role);
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break;
                        }
                        case CommandCodes.NarrowSetFrequency:
                        {
                            NarrowFrequencyKhz = Utilities.ThreeBytesToInt(request.Buffer[1], request.Buffer[2], request.Buffer[3]);
                            response = new[] { (byte)commandCode };
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break;
                        }
                        case CommandCodes.NarrowGetFrequency:
                        {
                            var bytes = Utilities.IntToThreeBytes((int)NarrowFrequencyKhz);
                            response = new[] { (byte)commandCode };
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break;
                        }
                        case CommandCodes.ManualStrobe:
                        {
                            response = new[] { (byte)commandCode };
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break;
                        }
                        case CommandCodes.WideGetThreshold:
                        {
                            response = new byte[] { (byte)commandCode, 0 };
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break; 
                        }
                        case CommandCodes.NarrowGetThreshold: 
                        {
                            response = new byte[] { (byte)commandCode, 0 };
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break;  
                        }
                        case CommandCodes.WideGetUpperThreshold: 
                        {
                            response = new byte[] { (byte)commandCode, 0, 0 };
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break; 
                        }
                        case CommandCodes.NarrowGetUpperThreshold: 
                        {
                            response = new byte[] { (byte)commandCode, 0, 0 };
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break; 
                        }
                        default:
                        {
                            response = new[] {(byte) commandCode};
                            _server.Send(response, response.Length, request.RemoteEndPoint);
                            break;
                        }
                    }
                }
                catch { }
            }
        }
    }
}
