﻿using DspDataModel;

namespace Simulator
{
    public class SimulatorModel
    {
        public readonly SignalSimulator SignalGenerator;
        public readonly SimulatorSave SimulatorSave;
        public DeviceSimulator MasterDevice;
        public DeviceSimulator SlaveDevice;

        public SimulatorModel()
        {
            SimulatorSave = SimulatorSave.Load();
            SimulatorSave.Model = this;
            SignalGenerator = new SignalSimulator(SimulatorSave);
        }

        public void Initialize()
        {
            MasterDevice = new DeviceSimulator(SimulatorSave.IpAddress, SimulatorSave.SimulatorPort, FtmRole.Master,
                SignalGenerator, SimulatorSave);
            SlaveDevice = new DeviceSimulator(SimulatorSave.IpAddress, SimulatorSave.SimulatorPort + 1,
                FtmRole.Slave, SignalGenerator, SimulatorSave);
        }
    }
}