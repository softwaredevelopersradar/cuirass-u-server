﻿using System.Windows;
using System.Windows.Input;

namespace Simulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel _viewModel;
        public MainWindow()
        {
            InitializeComponent();
            
            _viewModel = DataContext as MainWindowViewModel ?? new MainWindowViewModel();
            _viewModel.Initialize();
            StartStopButton.Content = _viewModel.IsWorking ? "Stop" : "Start";
            IsWorkingLabel.Content = _viewModel.IsWorking ? "Simulator is working" : "Simulator is stopped";

            NoiseLevelTextBox.KeyDown += TextBoxKeyDown;
            NoiseDeviationTextBox.KeyDown += TextBoxKeyDown;
            MasterFrequencyTextBox.KeyDown += TextBoxKeyDown;
            MasterBandwidthTextBox.KeyDown += TextBoxKeyDown;
            SlaveFrequencyTextBox.KeyDown += TextBoxKeyDown;
            SlaveBandwidthTextBox.KeyDown += TextBoxKeyDown;
            IpTextBox.KeyDown += TextBoxKeyDown;
            PortTextBox.KeyDown += TextBoxKeyDown;
        }

        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                StartStopButton.Focus();
            }
        }

        private void StartStopButton_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ChangeIsWorkingState();
            StartStopButton.Content = _viewModel.IsWorking ? "Stop" : "Start";
            IsWorkingLabel.Content = _viewModel.IsWorking ? "Simulator is working" : "Simulator is stopped";
        }
    }
}
