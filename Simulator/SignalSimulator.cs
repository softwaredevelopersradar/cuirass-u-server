﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DspDataModel;
using DspDataModel.Hardware;
using Settings;

namespace Simulator
{
    public class SignalSimulator
    {
        private byte _noiseDeviation;
        private readonly Random _noiseGenerator;

        public ObservableCollection<Signal> Signals;
        public byte NoiseLevel { get; set; }

        public byte NoiseDeviation
        {
            get => _noiseDeviation;
            set
            {
                if (value < 15)
                    _noiseDeviation = value;
            }
        }
        
        public SignalSimulator(SimulatorSave simulatorSave)
        {
            Signals = new ObservableCollection<Signal>(simulatorSave.Signals);
            NoiseLevel = simulatorSave.NoiseLevel;
            _noiseDeviation = simulatorSave.NoiseDeviation;
            _noiseGenerator = new Random();
        }

        public byte[] GenerateWideBandData(FtmRole role)
        {
            var fftResult = new byte[Constants.WideBandFftSampleCount + 1];
            fftResult[0] = (byte) CommandCodes.WideReadFft;
            for (int i = 1; i < fftResult.Length; i++)
            {
                fftResult[i] = (byte)_noiseGenerator.Next(NoiseLevel - NoiseDeviation, NoiseLevel + NoiseDeviation + 1);
            }

            foreach (var signal in Signals)
            {
                var directionShift = signal.Direction  * 0.8f/ 2;
                var indexRange = (int)(signal.BandwidthMhz / 2);//hack, here step is considered 1Mhz, for simplicity
                var frequencyIndex = (int) ((signal.FrequencyMhz - 1100) / Constants.WideBandFrequencyStepMhz) + 1;
                var startIndex = frequencyIndex - indexRange;
                var endIndex = frequencyIndex + indexRange + 1;

                if (endIndex < 1 || startIndex > Constants.WideBandFftSampleCount)
                    continue;//bad signal, don't do this

                for (int i = startIndex; i < endIndex; i++)
                {
                    var noise = _noiseGenerator.Next(-1,2);
                    if (i < 1 || i > Constants.WideBandFftSampleCount)
                        continue;
                    var amplitudeCoefficient = 1 - (double)Math.Abs(i - startIndex - (endIndex - startIndex) / 2) / (endIndex - startIndex);//magic to get not a full amplitude

                    if (role == FtmRole.Master)
                        fftResult[i] = Math.Max(fftResult[i], (byte)(signal.Amplitude * amplitudeCoefficient + directionShift));
                    else
                        fftResult[i] = Math.Max(fftResult[i], (byte)(signal.Amplitude * amplitudeCoefficient - directionShift));
                    fftResult[i] = (byte)(fftResult[i] + noise);
                }
            }
            return fftResult;
        }

        public byte[] GenerateNarrowData(float frequencyKhz, float bandWidthKhz, FtmRole role)
        {
            var fftResult = new byte[Constants.NarrowFftSampleCount + 1];
            fftResult[0] = (byte)CommandCodes.NarrowReadFft;
            var stepKhz = bandWidthKhz / (Constants.NarrowFftSampleCount - 1);
            for (int i = 1; i < fftResult.Length; i++)
            {
                fftResult[i] = (byte)_noiseGenerator.Next(NoiseLevel - _noiseDeviation, NoiseLevel + _noiseDeviation + 1);
            }

            foreach (var signal in Signals)
            {
                if (signal.FrequencyMhz * 1000 > frequencyKhz + bandWidthKhz / 2 - signal.BandwidthMhz * 1000 / 2 ||
                   signal.FrequencyMhz * 1000 < frequencyKhz - bandWidthKhz / 2 + signal.BandwidthMhz * 1000 / 2)
                    continue; //skip signals out of sight

                var directionShift = signal.Direction * 0.8f / 2;
                var indexRange = (int)(signal.BandwidthMhz * 1000 / (2 * stepKhz));
                var frequencyIndex = (int)((signal.FrequencyMhz * 1000 - (frequencyKhz - bandWidthKhz / 2))/ stepKhz) + 1;
                var startIndex = frequencyIndex - indexRange;
                var endIndex = frequencyIndex + indexRange + 1;

                if (signal.BandwidthMhz * 1000 <= stepKhz)
                    endIndex = startIndex++;
                if (startIndex <= 0 || startIndex > Constants.NarrowFftSampleCount)
                    continue;
                if (endIndex <= 0 || endIndex > Constants.NarrowFftSampleCount)
                    continue;

                for (int i = startIndex; i < endIndex; i++)
                {
                    var noise = _noiseGenerator.Next(-1, 2);
                    if (i < 1 || i > Constants.NarrowFftSampleCount)
                        continue;
                    var amplitudeCoefficient = 1 - (double)Math.Abs(i - startIndex - (endIndex - startIndex) / 2) / (endIndex - startIndex);//magic to get not a full amplitude

                    if (role == FtmRole.Master)
                        fftResult[i] = Math.Max(fftResult[i], (byte)(signal.Amplitude * amplitudeCoefficient + directionShift));
                    else
                        fftResult[i] = Math.Max(fftResult[i], (byte)(signal.Amplitude * amplitudeCoefficient - directionShift));
                    fftResult[i] = (byte)(fftResult[i] + noise);
                }
            }

            return fftResult;
        }

        public double[] SimulateVideoSignal(int pulseWidthMs, int pulseMs)
        {
            var numberOfPoints = 8192;
            var output = new double[numberOfPoints];

            var numberOfPeaks = 1;
            if (pulseMs <= 100)
                numberOfPeaks = 2;
            if (pulseMs <= 50)
                numberOfPeaks = 3;
            if (pulseMs <= 20)
                numberOfPeaks = 6;

            var peakWidthPoints = pulseWidthMs * 100;
            var peakZeroIndex = 50;
            var peakStartIndecies = new List<int>() { peakZeroIndex };
            if (numberOfPeaks != 1)
            {
                var delta = pulseMs * 70;
                for (int i = 1; i < numberOfPeaks; i++)
                {
                    peakStartIndecies.Add(peakZeroIndex + delta * i);
                }
            }

            //fill

            var noiseLevel = 160;
            var noiseDelta = 20;
            var peakLevel = 580;
            var peakDelta = 5;

            for (int i = 0; i < output.Length; i++)
            {
                if (peakStartIndecies.Contains(i))
                {
                    for (int j = 0; j < peakWidthPoints; j++, i++)
                    {
                        output[i] = _noiseGenerator.Next(peakLevel - peakDelta, peakLevel + peakDelta + 1);
                    }
                    i--;
                    continue;
                }
                else
                    output[i] = _noiseGenerator.Next(noiseLevel - noiseDelta, noiseLevel + noiseDelta + 1);
            }

            return output;
        }

        public double[] SimulateRfSignalsWithoutWindow(int deltaFreqMhz)
        {
            var numberOfPoints = 1024;
            var output = new double[numberOfPoints];

            var maxLevel = 6;
            if (deltaFreqMhz <= 5)
                maxLevel = 20;
            if (deltaFreqMhz <= 3)
                maxLevel = 80;
            if (deltaFreqMhz <= 1)
                maxLevel = 100;

            for (int i = 0; i < output.Length; i++)
            {
                output[i] = _noiseGenerator.Next(-maxLevel, maxLevel + 1);
            }

            return output;
        }

        public double[] SimulateRfSignalsWithWindow(int deltaFreqMhz)
        {
            var numberOfPoints = 1024;
            var output = new double[numberOfPoints];

            var maxLevel = 6;
            double multiplier = 10;
            if (deltaFreqMhz <= 5)
            {
                maxLevel = 20;
                multiplier = 3;
            }
            if (deltaFreqMhz <= 3)
            {
                maxLevel = 80;
                multiplier = 1.5;
            }
            if (deltaFreqMhz <= 1)
            {
                maxLevel = 100;
                multiplier = 1.5;
            }
            var delta = Math.PI / (numberOfPoints - numberOfPoints * 2 / 10);
            var superDelta = 50;
            var sideLevel = 2;
            for (int i = 0; i < output.Length; i++)
            {
                if (i < numberOfPoints / 10 || i >= numberOfPoints - numberOfPoints / 10)
                {
                    output[i] = _noiseGenerator.Next(-sideLevel, sideLevel + 1);
                    continue;
                }
                var sign = i % 2 == 0 ? 1 : -1;
                output[i] = Math.Sin(delta * (i - numberOfPoints / 10)) * _noiseGenerator.Next(maxLevel - superDelta, maxLevel + superDelta + 1) * sign / multiplier;
            }

            return output;
        }
    }
}
