﻿using System;

namespace Simulator
{
    public class Signal
    {
        private float _frequencyMhz;
        private byte _amplitude;
        private float _direction;
        private float _bandwidthMhz;
        private int _widthMs;
        private int _pulseMs;

        public float FrequencyMhz
        {
            get => _frequencyMhz;
            set
            {
                _frequencyMhz = value;
                SignalChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public byte Amplitude
        {
            get => _amplitude;
            set
            {
                _amplitude = value;
                SignalChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public float Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                SignalChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public float BandwidthMhz
        {
            get => _bandwidthMhz;
            set
            {
                _bandwidthMhz = value;
                SignalChanged?.Invoke(this, EventArgs.Empty);
            }
        }        

        public int WidthMs
        {
            get => _widthMs;
            set
            {
                _widthMs = value;
                SignalChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public int PulseMs
        {
            get => _pulseMs;
            set
            {
                _pulseMs = value;
                SignalChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public static event EventHandler SignalChanged;

        public Signal()
        {
            FrequencyMhz = 1_400;
            Amplitude = 90;
            Direction = 0;
            BandwidthMhz = 2;
            PulseMs = 50;
            WidthMs = 7;
        }

        public Signal(float frequencyMhz, byte amplitude, float direction, float bandwidthMhz, int pulseMs, int widthMs)
        {
            FrequencyMhz = frequencyMhz;
            Amplitude = amplitude;
            Direction = direction;
            BandwidthMhz = bandwidthMhz;
            PulseMs = pulseMs;
            WidthMs = widthMs;
        }
    }
}
