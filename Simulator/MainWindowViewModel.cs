﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Simulator
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private SimulatorModel _model;

        public ObservableCollection<Signal> Signals { get; set; }
        public bool IsWorking => _model.MasterDevice.IsWorking && _model.SlaveDevice.IsWorking;

        public byte NoiseLevel
        {
            get => _model.SignalGenerator.NoiseLevel;
            set
            {
                _model.SignalGenerator.NoiseLevel = value;
                OnPropertyChanged(nameof(NoiseLevel));
            }
        }

        public byte NoiseDeviation
        {
            get => _model.SignalGenerator.NoiseDeviation;
            set
            {
                _model.SignalGenerator.NoiseDeviation = value;
                OnPropertyChanged(nameof(NoiseDeviation));
            }
        }

        public float NarrowMasterFrequency
        {
            get => _model.MasterDevice.NarrowFrequencyKhz;
            set
            {
                _model.MasterDevice.NarrowFrequencyKhz = value;
                OnPropertyChanged(nameof(NarrowMasterFrequency));
            }
        }

        public float NarrowMasterBandwidth
        {
            get => _model.MasterDevice.NarrowBandwidthKhz;
            set
            {
                _model.MasterDevice.NarrowBandwidthKhz = value;
                OnPropertyChanged(nameof(NarrowMasterBandwidth));
            }
        }

        public float NarrowSlaveFrequency
        {
            get => _model.SlaveDevice.NarrowFrequencyKhz;
            set
            {
                _model.SlaveDevice.NarrowFrequencyKhz = value;
                OnPropertyChanged(nameof(NarrowSlaveFrequency));
            }
        }

        public float NarrowSlaveBandwidth
        {
            get => _model.SlaveDevice.NarrowBandwidthKhz;
            set
            {
                _model.SlaveDevice.NarrowBandwidthKhz = value;
                OnPropertyChanged(nameof(NarrowSlaveBandwidth));
            }
        }

        public string IpString
        {
            get => _model.SimulatorSave.IpAddress;
            set
            {
                _model.SimulatorSave.IpAddress = value;
                OnPropertyChanged(nameof(IpString));
            }
        }

        public int SimulatorPort
        {
            get => _model.SimulatorSave.SimulatorPort;
            set
            {
                _model.SimulatorSave.SimulatorPort = value;
                OnPropertyChanged(nameof(SimulatorPort));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Initialize()
        {
            _model = new SimulatorModel();
            _model.Initialize();
            Signals = _model.SignalGenerator.Signals;
            InitializeFirstBinding();
            Signals.CollectionChanged += Signals_CollectionChanged;
            Signal.SignalChanged += Signal_SignalChanged;
        }

        private void Signals_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        { 
            _model.SimulatorSave.Save(); 
        }

        private void Signal_SignalChanged(object sender, System.EventArgs e)
        {
            _model.SimulatorSave.Save();
        }

        public void ChangeIsWorkingState()
        {
            _model.MasterDevice.IsWorking = !_model.MasterDevice.IsWorking;
            _model.SlaveDevice.IsWorking = !_model.SlaveDevice.IsWorking;
        }

        private void InitializeFirstBinding()
        {//todo : find out why do we need this in the first place
            OnPropertyChanged(nameof(Signals));
            OnPropertyChanged(nameof(NoiseLevel));
            OnPropertyChanged(nameof(NoiseDeviation));
            OnPropertyChanged(nameof(NarrowMasterFrequency));
            OnPropertyChanged(nameof(NarrowMasterBandwidth));
            OnPropertyChanged(nameof(NarrowSlaveFrequency));
            OnPropertyChanged(nameof(NarrowSlaveBandwidth));
            OnPropertyChanged(nameof(IpString));
            OnPropertyChanged(nameof(SimulatorPort));
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            _model.SimulatorSave.Save();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}