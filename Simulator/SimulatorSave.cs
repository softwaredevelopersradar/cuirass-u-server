﻿using System.IO;
using System.Linq;
using System.Text;
using YamlDotNet.Serialization;

namespace Simulator
{
    public class SimulatorSave
    {
        public const string DefaultFileName = "SimulatorSave.yaml";
        public Signal[] Signals { get; set; } = new Signal[0];
        public byte NoiseLevel { get; set; } = 42;
        public byte NoiseDeviation { get; set; } = 5;
        public float NarrowMasterFrequency { get; set; } = 1_400_000;
        public float NarrowMasterBandwidth { get; set; } = 10_000;
        public float NarrowSlaveFrequency { get; set; } = 1_400_000;
        public float NarrowSlaveBandwidth { get; set; } = 10_000;
        public string IpAddress { get; set; } = "127.0.0.1";
        public int SimulatorPort { get; set; } = 9001;

        [YamlIgnore]
        public SimulatorModel Model;

        public void Save(string fileName = DefaultFileName)
        {
            try
            {
                if(Model != null)
                    Update();
                var serializer = new SerializerBuilder()
                    .Build();
                using (var fs = File.Open(fileName, FileMode.Create))
                using (var sw = new StreamWriter(fs, Encoding.UTF8))
                {
                    serializer.Serialize(sw, this);
                }
            }
            catch
            {
                // ignored
            }
        }

        private void Update()
        {
            Signals = Model.SignalGenerator.Signals.ToArray();
            NoiseLevel = Model.SignalGenerator.NoiseLevel;
            NoiseDeviation = Model.SignalGenerator.NoiseDeviation;
            NarrowMasterFrequency = Model.MasterDevice.NarrowFrequencyKhz;
            NarrowMasterBandwidth = Model.MasterDevice.NarrowBandwidthKhz;
            NarrowSlaveFrequency = Model.SlaveDevice.NarrowFrequencyKhz;
            NarrowSlaveBandwidth = Model.SlaveDevice.NarrowBandwidthKhz;
        }

        /// <returns> returns true if load is successful </returns>
        public static SimulatorSave Load(string filename = DefaultFileName)
        {
            try
            {
                return LoadInner(filename);
            }
            catch
            {
                return new SimulatorSave();
            }
        }

        private static SimulatorSave LoadInner(string filename)
        {
            var deserializer = new DeserializerBuilder()
                .Build();
            return deserializer.Deserialize<SimulatorSave>(File.ReadAllText(filename));
        }
    }
}
