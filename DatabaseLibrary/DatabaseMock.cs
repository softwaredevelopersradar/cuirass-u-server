﻿using DspDataModel.Data;
using DspDataModel.Database;
using KirasaUModelsDBLib;
using System;
using System.Collections.Generic;

namespace DatabaseLibrary
{
    public class DatabaseMock : IDatabaseController
    {
        public bool IsConnected { get; private set; }

        public event EventHandler OnConnectToDatabase;
        public event EventHandler OnDisconnectFromDatabase;
        public event EventHandler<string> OnDatabaseError;
        public event EventHandler<GlobalProperties> OnGlobalPropertiesUpdated;
        public event EventHandler<IEnumerable<FrequencyRange>> OnForbiddenFrequenciesUpdated;
        public event EventHandler<IEnumerable<FrequencyRange>> OnReconFrequenciesUpdated;
        public event EventHandler<IEnumerable<DirectionSector>> OnSectorsUpdated;

        public void AddFrsSignals(IEnumerable<ISignal> signals)
        { }

        public void ClearFrsSignalsTable()
        { }

        public void Connect()
        { }

        public void Disconnect()
        { }
    }
}
