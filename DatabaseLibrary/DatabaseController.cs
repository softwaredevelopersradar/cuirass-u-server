﻿using ClientDataBase;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Database;
using KirasaUModelsDBLib;
using System;
using System.Linq;
using System.Collections.Generic;
using DspDataModel.Hardware;

namespace DatabaseLibrary
{
    public class DatabaseController : IDatabaseController
    {
        private readonly ClientDB _databaseClient;
        public bool IsConnected => _databaseClient.IsConnected();

        public event EventHandler OnConnectToDatabase;
        public event EventHandler OnDisconnectFromDatabase;
        public event EventHandler<string> OnDatabaseError;
        public event EventHandler<GlobalProperties> OnGlobalPropertiesUpdated;
        public event EventHandler<IEnumerable<FrequencyRange>> OnForbiddenFrequenciesUpdated;
        public event EventHandler<IEnumerable<FrequencyRange>> OnReconFrequenciesUpdated;
        public event EventHandler<IEnumerable<DirectionSector>> OnSectorsUpdated;

        public DatabaseController(string clientName = "Cuirass dsp server", string databaseAddress = "127.0.0.1:8302")
        {
            _databaseClient = new ClientDB(clientName, databaseAddress);
            SubscribeToUtilityEvents();
            watch.Start();
        }

        private void SubscribeToUtilityEvents()
        {
            _databaseClient.OnConnect += ConnectedToDatabase;
            _databaseClient.OnDisconnect += DisconnectedFromDatabase;
            _databaseClient.OnErrorDataBase += DatabaseErrorOccured;
        }

        public void Connect() => _databaseClient.ConnectAsync();

        public void Disconnect() => _databaseClient.DisconnectAsync();
        
        //todo : move to storage
        private readonly System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        public async void AddFrsSignals(IEnumerable<ISignal> signals)
        {
            if (watch.Elapsed.TotalSeconds < 1)
            {
                return;//we do not update to quickly
            }
            var databaseSignals = signals.Select(
                signal => new TableRes(
                    frequencyKhz: signal.FrequencyKhz, 
                    bearing: !float.IsNaN(signal.Direction) ? signal.Direction : -1,
                standardDeviation: signal.StandardDeviation, 
                pulse: new ParamDP(signal.DurationMcs, signal.RepeatPeriodMcs),
                series: new ParamDP(3,4), 
                antennaMaster: (byte)AntennaHelper.MasterIndexToNumber(signal.MasterAntennaIndex),
                antennaSlave: (byte)AntennaHelper.SlaveIndexToNumber(signal.SlaveAntennaIndex), 
                note: "",
                id: signal.Id)).ToList();
            await _databaseClient.Tables[NameTable.TableRes].ClearAsync();
            await _databaseClient.Tables[NameTable.TableRes].AddRangeAsync(databaseSignals);
            watch.Restart();
        }

        private void DatabaseErrorOccured(object sender, InheritorsEventArgs.OperationTableEventArgs e) =>
            OnDatabaseError?.Invoke(this, $"Error in table {e.TableName}, operation {e.Operation}, type error {e.TypeError}\r\n{e.GetMessage}");

        private void DisconnectedFromDatabase(object sender, InheritorsEventArgs.ClientEventArgs e)
        {
            MessageLogger.Warning("Disconnected from database");
            OnDisconnectFromDatabase?.Invoke(this, EventArgs.Empty);
            UnsubscribeFromTableEvents();
        }

        private void ConnectedToDatabase(object sender, InheritorsEventArgs.ClientEventArgs e)
        {
            MessageLogger.Warning("Connected to database");
            OnConnectToDatabase?.Invoke(this, EventArgs.Empty);
            SubscribeToTableEvents(); 
            LoadAllTables();
            ClearFrsSignalsTable();
        }

        private void SubscribeToTableEvents()
        {
            (_databaseClient.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += IntelligenceRangesChanged;
            (_databaseClient.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += ForbiddenFrequenciesChanged;
            (_databaseClient.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable += IntelligenceSectorsChanged;
            (_databaseClient.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += GlobalPropertiesChanged;
        }

        private void UnsubscribeFromTableEvents()
        {
            (_databaseClient.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable -= IntelligenceRangesChanged;
            (_databaseClient.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable -= ForbiddenFrequenciesChanged;
            (_databaseClient.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable -= IntelligenceSectorsChanged;
            (_databaseClient.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable -= GlobalPropertiesChanged;
        }

        private void IntelligenceSectorsChanged(object sender, InheritorsEventArgs.TableEventArgs<TableSectorsRecon> sectors)
        {
            var intelligenceSectors = sectors.Table.Where(r => r.IsActive)
                .Select(r => new DirectionSector(r.AngleMin, r.AngleMax))
                .Where(r => r.SectorStart != -1 || r.SectorEnd != -1);
            OnSectorsUpdated?.Invoke(this, intelligenceSectors);
        }

        private void ForbiddenFrequenciesChanged(object sender, InheritorsEventArgs.TableEventArgs<TableFreqForbidden> ranges)
        {
            OnForbiddenFrequenciesUpdated?.Invoke(this, ranges.Table.Where(r => r.IsActive).Select(r => new FrequencyRange((float)r.FreqMinKHz, (float)r.FreqMaxKHz)));
        }

        private void IntelligenceRangesChanged(object sender, InheritorsEventArgs.TableEventArgs<TableFreqRangesRecon> ranges)
        {
            OnReconFrequenciesUpdated?.Invoke(this, ranges.Table.Where(r => r.IsActive).Select(r => new FrequencyRange((float)r.FreqMinKHz, (float)r.FreqMaxKHz)));
        }

        private void GlobalPropertiesChanged(object sender, InheritorsEventArgs.TableEventArgs<GlobalProperties> properties)
        {
            if (properties.Table.Count == 0)
                return;
            OnGlobalPropertiesUpdated?.Invoke(this, properties.Table.First());
            SetGlobalPropertiesToConfig(properties.Table.First());
        }

        private async void LoadAllTables() 
        {
            var intelligenceRanges = await _databaseClient.Tables[NameTable.TableFreqRangesRecon].LoadAsync<TableFreqRangesRecon>();
            IntelligenceRangesChanged(this, new InheritorsEventArgs.TableEventArgs<TableFreqRangesRecon>(intelligenceRanges));

            var intelligenceSectors = await _databaseClient.Tables[NameTable.TableSectorsRecon].LoadAsync<TableSectorsRecon>();
            IntelligenceSectorsChanged(this, new InheritorsEventArgs.TableEventArgs<TableSectorsRecon>(intelligenceSectors));

            var forbiddenRanges = await _databaseClient.Tables[NameTable.TableFreqForbidden].LoadAsync<TableFreqForbidden>();
            ForbiddenFrequenciesChanged(this, new InheritorsEventArgs.TableEventArgs<TableFreqForbidden>(forbiddenRanges));
            
            var globalProperties = await _databaseClient.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>();

            if (globalProperties.Count != 0)
            {
                OnGlobalPropertiesUpdated?.Invoke(this, globalProperties.First()); // todo : remove
                SetGlobalPropertiesToConfig(globalProperties.First());
            }
        }

        public async void ClearFrsSignalsTable()
        {
            await _databaseClient.Tables[NameTable.TableRes].ClearAsync();
        }

        private void SetGlobalPropertiesToConfig(GlobalProperties globalProperties) 
        {
            var config = Config.Instance.DirectionFindingSettings;
            config.NumberOfRdfScans = globalProperties.RadioIntelegence.NumberOfReviews;
            config.DirectionCorrection = globalProperties.RadioIntelegence.CourseAngle;
            config.IqAggregationDurationSec = (int)globalProperties.RadioIntelegence.TimeRecord;
            config.IqFilePath = globalProperties.RadioIntelegence.PathFolderNbFiles;
        }
    }
}
